
var doc4js = {
    functions: {},
    onLoad: [],
    filterMenu : function(filterString){
        window.clearTimeout(doc4js.filterTimeout);
        doc4js.filterTimeout = window.setTimeout(function(){
            var menu = document.getElementById("doc4js-menu");
            var listItems = menu.getElementsByTagName("li");
            for(var i = listItems.length; i-- > 0;){
                var listItem = listItems[i];
                var link = listItem.childNodes[1].href;
                link = link.substring(link.indexOf("#")+1);
                listItem.style.display = ((filterString.indexOf(link) >= 0) || (link.indexOf(filterString) >= 0)) ? "" : "none";
            }
        }, 200);
    }
};

new function(){
    var onload = function(){
        
        //Set up layout - a split pane will do by default
        doc4js.layout = new tl.SplitPane({ 
            paneAAttrs:{size:280,units:"px"},
            divisorAttrs:tl.copyAttributes(tl.SplitPaneAttrs.divisorAttrs,{border:0})});
        
        var menu = document.getElementById("doc4js-menu");
        var filter = document.createElement("div");
        filter.innerHTML = '<input type="search" placeholder="Filter Elements" onkeyup="doc4js.filterMenu(this.value);" style="width:90%;" />';
        menu.insertBefore(filter, menu.childNodes[0]);
        
        doc4js.menu = new tl.Pane().adopt(menu);
        doc4js.layout.getPaneA({scrolling:true}).insertChild(doc4js.menu);
        doc4js.content = new tl.Pane({scrolling:true}).adopt("doc4js-header","doc4js-content","doc4js-footer");
        doc4js.layout.getPaneB().insertChild(doc4js.content);

        for(var f = 0; f < doc4js.onLoad.length; f++){ // Run onload functions
            doc4js.functions[doc4js.onLoad[f]]();
        }
        
        doc4js.menu.getEventChannel().addListener("updated", function(pane, outerRect){
            document.getElementById("doc4js-menu").style.width = (outerRect[2]-outerRect[0])+"px";
        });
        
        //Set the layout - layout is public giving onload functions the opportunity to modify it
        tl.BodyPane.setContentPane(doc4js.layout);
        
        window.setTimeout(function(){
            if(location.hash){
                location.replace(window.location.href); // Force refocus on hash after the initial layout
            }
        },1000);
    }
    if (window.addEventListener) {
        window.addEventListener('load', onload, false);
    } else if (window.attachEvent) {
        window.attachEvent('onload', onload);
    }
}