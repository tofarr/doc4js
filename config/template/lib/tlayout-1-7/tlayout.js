
/**
 * TLayout - Rich Layout Manager for Web Applications
 * @project tlayout
 * @author tofarr
 * @version 1.6
 * @description Rich Layout Manager for Web Applications
 */

/**
 * @namespace tl
 * Below is a collection of objects for laying out web pages. This was born out of Frustration with Browsers, HTML, and
 * especially the W3C, who by dogmatically sticking to their version of the box model have made it virtually impossible
 * to lay out a web page that will scale with the user's browser window without resorting to javascript hacks.
 *
 * Unlike the traditional model, the content of nodes will not cause them to expand beyond their alloted size - so all
 * calculations can be done using pixel values from the top down.
 *
 *
 */
var tl = {

    /** @class {static} tl.Sizing sizing methods for layout panes. */
    Sizing : {
        /** @property {static final string} ? Occupy the number of pixels specified */
        PIXEL : "px",
        /** @property {static final string} ? Occupy a Percentage of the space */
        PERCENT : "%",
        /** @property {static final string} ? Occupy the remaining space - only works if parent element is document.body or an instance of Panel */
        AUTO : "auto"
    },
    /** @end */

    /** @class {static} tl.Event event definitions for layout panes. */
    Event : {
        /** @property {static final string} ? Indicates the pane is being updated {params (pane, outerRect)} */
        UPDATING : "updating",
        /** @property {static final string} ? Indicates the pane was updated {params (pane, outerRect)} */
        UPDATED : "updated",
        /** @property {static final string} ? Indicates the pane was disposed of {params (pane)} */
        DISPOSED : "disposed",
        /** @property {static final string} ? Indicates the pane was removed {params (pane, parent)} */
        REMOVED : "removed",
        /** @property {static final string} ? Indicates the pane was inserted {params (pane, parent)} */
        INSERTED : "inserted",
        /** @property {static final string} ? Indicates the pane started loading data {params (pane, url)} */
        LOAD_START : "loadStart",
        /** @property {static final string} ? Indicates the pane finished loading data {params (pane, url, xhr)} */
        LOAD_END : "loadEnd"
    },
    /** @end */

    /** @function {static} tl.nextId Get the next unique id for a pane from the sequence */
    nextId : function(){
        if(!this.idIndex){
            this.idIndex = 1;
        }else{
            this.idIndex++;
        }
        return this.idIndex;
    },

    /**
     * @function {static} tl.copyAttributes Copy attributes from one object to another, without overriding any existing attributes
     * @param from the object from which attributes should be copied
     * @param to the object to which attributes should be copied
     * @param override if true, override parameters
     */
    copyAttributes : function(from, to, override){
        if(!to){
            to = {};
        }
        for(var i in from){
            if(override || (typeof to[i] == "undefined")){
                to[i] = from[i];
            }
        }
        return to;
    },

    /** @function {static} tl.toRect Derive a rect (array of 4 numbers) from the values given - rect may not represent an actual rectangle, but rather 4 connected values, x1,y1,x2,y2 (left,top,right,bottom) */
    toRect : function(x1, y1, x2, y2){
        if(arguments.length == 1){
            if(arguments[0] && (arguments[0].length == 4)){
                y2 = x1[3];
                x2 = x1[2];
                y1 = x1[1];
                x1 = x1[0];
            }else{
                y1 = x2 = y2 = x1;
            }
        }else if(arguments.length == 2){
            x2 = x1;
            y2 = y1;
        }
        x1 = ((!x1) || (x1 < 0)) ? 0 : x1;
        y1 = ((!y1) || (y1 < 0)) ? 0 : y1;
        x2 = ((!x2) || (x2 < 0)) ? 0 : x2;
        y2 = ((!y2) || (y2 < 0)) ? 0 : y2;
        return [x1,y1,x2,y2];
    },

    /** @function {static} tl.domElementToTabs
     * Convert the contents of the dom element given to tabs, and place them in the tab/accordion pane given
     * @param tabPane a tl.TabPane/tl.AccordionPane instance
     * @param domElement the DOM element from which to extract nodes
     */
    domElementToTabs : function(tabPane, domElement){
        var childNodes = domElement.childNodes;
        var length = childNodes.length;
        var nodes = [];
        var i;
        for(i = 0; i < length; i++){
            if(childNodes[i].nodeType == 1){
                nodes.push(childNodes[i]);
            }
        }
        for(i = 0; i < nodes.length; i++){
            var title = nodes[i].innerHTML;
            i++;
            var pane = new tl.Pane().adopt(nodes[i]);
            tabPane.insertTab(title, pane);
        }
        domElement.parentNode.removeChild(domElement); // Remove the broken husk from the dom!
    },

    // Mappings of aliases to panes
    aliases : {},

    /**
     * @function {static} tl.panes find and return any panes matching the alias given
     * @param alias for panes
     * @return array of panes matching the alias given (empty array if no matching panes)
     */
    panes : function(alias){
        var aliases = tl.aliases;
        var panes = aliases[alias];
        if(!panes){
            panes = aliases[alias] = [];
        }
        return panes;
    },

    /**
     * @function {static} tl.elements find and return the elements for any panes matching the alias given.
     * One use is to wrap the results using JQuery for easy DOM manipulation...
     */
    elements : function(alias){
        var panes = tl.panes(alias);
        var elements = [];
        for(var i = 0; i < panes.length; i++){
            elements[i] = panes[i].getElement();
        }
        return elements;
    },

    /**
     * @function {static} tl.addAlias add the alias given for the pane given. Should be invoked internally
     *  by panes to add references to them
     * @param alias for pane
     * @param pane the pane to alias
     */
    addAlias : function(alias, pane){
        var aliases = tl.aliases;
        var panes = aliases[alias];
        if(panes){
            for(var i = panes.length-1; i >= 0; i--){
                if(panes[i] == pane){
                    return; // alias/pane mapping already exists
                }
            }           
        }else{
            panes = aliases[alias] = []; // new alias
        }
        panes.push(pane); // add alias for pane
    },

    /**
     * @function {static} tl.removeAlias remove the alias given for the pane given. Should be invoked internally
     * by panes to add references to them
     * @param alias for pane
     * @param pane the pane to alias
     */
    removeAlias : function(alias, pane){
        var aliases = tl.aliases;
        var panes = aliases[alias];
        if(panes){
            for(var i = panes.length-1; i >= 0; i--){
                if(panes[i] == pane){
                    panes.splice(i, 1);
                    return;
                }
            }
        }
    }
}/**
 * @class {static} tl.EventChannel
 * Channel for events, allowing listeners to be added and events to be fired.
 */
tl.EventChannel = function(){
    var _this = this;
    var listeners = {};
    var firing = false;
    var updateQueue = [];

    /**
     * @function ? Setup a listener
     * @param eventType the type of event on which to listen
     * @param callback callback function
     * @param {optional} invocations max number of times the callback will be invoked
     * @return this
     */
    this.setListener = function(eventType, callback, invocations){
        setListener(eventType, callback, invocations);
        return _this;
    }

    /**
     * @function ? Add a listener
     * @param eventType the type of event on which to listen
     * @param callback callback function
     * @return this
     */
    this.addListener = function(eventType, callback){
        setListener(eventType, callback, -1);
        return _this;
    }

    /**
     * @function ? Remove a listener
     * @param eventType the type of event on which to listen
     * @param callback callback function
     * @return this
     */
    this.removeListener = function(eventType, callback){
        if(_this){
            setListener(eventType, callback, 0);
        }
        return _this;
    }

    /**
     * @function ? Add a listener for a single invocation
     * @param eventType the type of event on which to listen
     * @param callback callback function
     * @return this
     */
    this.addOnce = function(eventType, callback){
        setListener(eventType, callback, 1);
        return _this;
    }

    /**
     * @function ? Fire an event - takes extra arguments and passes them on to the callback function.
     * If a callback results in changes to the queue (callbacks added / removed),
     * this will not be manifested until after the callbacks have all been invoked (So new
     * callbacks will not be invoked, and removed callbacks will still be invoked that last time.)
     * @param eventType the type of event to fire
     * @param {optional} arg1 first argument to pass to callback function - accepts 0-n args
     * @param {optional} arg2 second argument to pass to callback function - accepts 0-n args
     * @return this
     */
    this.fireEvent = function(eventType, arg1, arg2){
        var eventTypeListeners = listeners[eventType];
        if(eventTypeListeners){
            firing = true;
            try{
                var callbacks = eventTypeListeners.callbacks; // clone queue, so that changes as a result of the event will not be visible until after the event has fired
                var invocations = eventTypeListeners.invocations;
                var numArgs = arguments.length;
                var error = null;
                for(var i = callbacks.length-1; i >= 0; i--){
                    try{
                        switch(numArgs){ // Hack for IE stack trace compliance - older versions of IE lose the stack trace when apply or call is used to call a method.
                            case 1:
                                callbacks[i]();
                                break;
                            case 2:
                                callbacks[i](arguments[1]);
                                break;
                            case 3:
                                callbacks[i](arguments[1], arguments[2]);
                                break;
                            case 4:
                                callbacks[i](arguments[1], arguments[2], arguments[3]);
                                break;
                            default:
                                var args = [];
                                args.push.apply(args, arguments);
                                args.splice(0,1);
                                callbacks[i].apply(this, args);
                                break;
                        }
                    }catch(e){
                        if(!error){
                            error = e; // catch the error - we will rethrow the first error later after any other listeners have been invoked
                        }
                    }
                    invocations[i]--;
                    if(!invocations[i]){
                        callbacks.splice(i, 1);
                        invocations.splice(i, 1);
                    }
                }
                if(error){
                    throw error; // rethrow thr first error error
                }
            }finally{
                firing = false; // no longer firing an event
                // Make any pending changes...
                for(var j = 0; j < updateQueue.length; j+=3){
                    setListener(updateQueue[j],updateQueue[j+1],updateQueue[j+2]);
                }
                updateQueue.length = 0;
            }
        }
        return _this;
    }

    /**
     * @function ? Dispose of this channel. After this method has been called,
     * the channel will no longer be usable
     */
    this.dispose = function(){
        listeners = updateQueue = _this = null;
    }

    // Set the number of invocations for the callback function given on the event type given
    // (undefined or less than 0 implies infinite; 0 implies remove)
    var setListener = function(eventType, callback, invocations) {
        if(!eventType){
            throw new Error("NoEventType");
        }
        if(!callback){
            throw new Error("NoCallback");
        }
        if(firing){
            updateQueue.push(eventType, callback, invocations);
        }else{
            if(typeof invocations == undefined){
                invocations = -1;
            }
            var eventTypeListeners = listeners[eventType];
            if(eventTypeListeners){
                var queue = eventTypeListeners.callbacks;
                for(var i = queue.length-1; i >= 0; i--){
                    if(queue[i] == callback){ // callback found...
                        if(invocations){
                            eventTypeListeners.invocations[i] = invocations; // listener already existed - update number of invocations
                        }else{
                            queue.splice(i, 1);
                            eventTypeListeners.invocations.splice(i, 1);
                        }
                        return; // done...
                    }
                }
                //Callback was not found, add it if there are invocations...
                if(invocations){
                    queue.splice(0, 0, callback);
                    eventTypeListeners.invocations.splice(0, 0, invocations);
                }
            }else if(invocations){ // If there are invocations, add callback
                eventTypeListeners = listeners[eventType] = {
                    callbacks : [callback],
                    invocations : []
                };
                eventTypeListeners.invocations[0] = invocations;
            }
        }
    }
}
tl.dom = {

    /**
    * @class {static} tl.dom.Animation
    * Create a new animation object. Animations are time based, with the interval determining
    * how often (in milliseconds) that updates should occur.
    * @param updateFn the function used to update. Has the signature update(value), where value is a floating point number between 0 and 1 indicating progress
    * @param callback (Optional) callback for when animation finishes.
    * @param time (Optional - defaults to 300 milliseconds) the time for the animation
    * @param interval (Optional - defaults to 20 milliseconds) - interval between frames.
    */
    Animation : function(updateFn, callback, time, interval){
        if(!time){
            time = 300;
        }
        if(!interval){
            interval = 20;
        }
        var startTime = new Date().getTime();
        var animate = window.setInterval(function(){
            var currentTime = new Date().getTime() - startTime;
            if(currentTime >= time){
                window.clearInterval(animate);
                animate = null;
                updateFn(1);
                if(callback){
                    callback();
                }
                updateFn = callback = null; // clean up potential memory leaks
            }
            else{
                updateFn(currentTime/time);
            }
        }, interval);

        /**
            * Terminate the animation mid stream - no callback is called
            */
        this.cancel = function(){
            if(animate){
                window.clearInterval(animate);
                animate = updateFn = callback = null; // clean up potential memory leaks
            }
        }
    },
    
    
    /** @function ? Convenience cross-browser function for setting the opacity of a DOM element
     * @param element the element for which to set opacity
     * @param opacity the opacity (number between 0 and 1)
     */
    setOpacity : function(element, opacity) {
        if(opacity > 1)
            opacity=1;
        if(opacity < 0.01)
            opacity=0.01;
        var style=element.style;
        style.filter="alpha(opacity="+Math.round(opacity*100)+")";
        style.zoom = "1"; // IE Opacity hack
        style.opacity=opacity;
    },

    /** @class {static} tl.dom.basic Set of Basic Dom functions for node manipulation - updates are immediate */
    basic : {
        /** @function ? Function to be used for removing dom elements from the page
        * @param domElement the element to remove
        * @param {optional} callback callback for when this operation completes
        */
        remove : function(domElement, callback){
            domElement.parentNode.removeChild(domElement);
            if(callback){
                callback();
            }
        },
        /** @function ? Function to be used for inserting dom elements into the page
        * @param parentDomElement the parent DOM node at which to insert the element
        * @param childDomElement the element to insert
        * @param {optional} beforeDomElement element before which to insert the element
        * @param {optional} callback callback for when this operation completes
        */
        insert : function(parentDomElement, childDomElement, beforeDomElement, callback){
            tl.dom.setOpacity(childDomElement, 1);
            if(beforeDomElement){
                parentDomElement.insertBefore(childDomElement, beforeDomElement);
            }else{
                parentDomElement.appendChild(childDomElement);
            }
            if(callback){
                callback();
            }
        },
        /** @function ? Function to be used for setting bounds of DOM elements
         * @param domElement element for which bounds should be set
         * @param left position left to move to
         * @param top position top to move to
         * @param width new width
         * @param height new height
         * @param {optional} callback function
         */
        setBounds : function(domElement, left, top, width, height, callback){
            var style = domElement.style;
            style.left = left+"px";
            style.top = top+"px";
            style.width = width+"px";
            style.height = height+"px";
            if(callback){
                callback();
            }
            
        }
    }
}
    
/** @class {static} tl.dom.animated Set of Dom functions for node manipulation, where animation is involved and dom
 * elements slide to the correct size and position */
tl.dom.animated = {
    /** @function ? Function to be used for removing dom elements from the page
     * @param domElement the element to remove
     * @param {optional} callback callback for when this operation completes
     */
    remove : tl.dom.basic.remove,
    /** @function ? Function to be used for inserting dom elements into the page
     * @param parentDomElement the parent DOM node at which to insert the element
     * @param childDomElement the element to insert
     * @param {optional} beforeDomElement element before which to insert the element
     * @param {optional} callback callback for when this operation completes
     */
    insert : function(parentDomElement, childDomElement, beforeDomElement, callback){
        var display = childDomElement.style.display;
        childDomElement.style.display = "none";
        tl.dom.basic.insert(parentDomElement,childDomElement,beforeDomElement);
        window.setTimeout(function(){
            childDomElement.style.display = display; // Doing this later means that the accordion animation works properly
            if(callback){
                callback();
            }
            parentDomElement = childDomElement = beforeDomElement = calback = null; // clean up memory leaks
        }, 300);
    },
    /** @function ? Function to be used for setting bounds of DOM elements
        * @param domElement element for which bounds should be set
        * @param left position left to move to
        * @param top position top to move to
        * @param width new width
        * @param height new height
        * @param {optional} callback function
        */
    setBounds : function(domElement, left, top, width, height, callback){
        if(domElement.moveAnim){
            domElement.moveAnim.cancel();
        }
        var style = domElement.style;
        var components = {left:[0,left],top:[0,top],width:[0,width],height:[0,height]};
        for(var c in components){
            var value = style[c];
            value = parseInt(value.substring(0,value.length-2));
            if((!value) && (value != 0)){
                tl.dom.basic.setBounds(domElement, left, top, width, height, callback);
                return;
            }else{
                var array = components[c];
                array[0] = value; // set first value to origin
                array[1] -= value; // set second value to the delta
            }
        }
        domElement.boundsAnim = new tl.dom.Animation(function(value){
            for(var c in components){
                var array = components[c];
                style[c] = ((value * array[1])+array[0])+"px";
            }
        }, function(){
            domElement.boundsAnim = null;
            if(callback){
                callback();
            }
        });
    }
}
    
/** @class {static} tl.PaneAttrs.fadingDomFunctions Set of Animated Dom functions for node manipulation, where nodes fade in and out  */
tl.dom.fading = tl.copyAttributes(tl.dom.animated, {
    //Remove an element from the dom
    remove : function(domElement, callback){
        if(domElement.fadeAnim){
            domElement.fadeAnim.cancel();
        }
        var setOpacity = this.setOpacity;
        domElement.fadeAnim = new tl.dom.Animation(function(value){
            value = 1-value;
            tl.dom.setOpacity(domElement, value);
        }, function(){
            if(domElement.boundsAnim){
                domElement.boundsAnim.cancel();
                domElement.boundsAnim = null;
            }
            tl.dom.basic.remove(domElement);
            domElement.fadeAnim = null;
            domElement = setOpacity = null;
        });
    },
    //Insert an element into the dom
    insert : function(parentDomElement, childDomElement, beforeDomElement, callback){
        if(childDomElement.fadeAnim){
            childDomElement.fadeAnim.cancel();
        }
        tl.dom.setOpacity(childDomElement, 0.01);
        tl.dom.basic.insert(parentDomElement,childDomElement,beforeDomElement);
        childDomElement.fadeAnim = new tl.dom.Animation(function(value){
            tl.dom.setOpacity(childDomElement, value);
        }, function(){
            childDomElement.fadeAnim = null;
            if(callback){
                callback();
            }
            parentDomElement = childDomElement = beforeDomElement = callback = null;
        });
    }
});

/**
 * @class {static} tl.Bookmarking
 * Service for adding bookmarkable state to layout widgets. Note that the underlying
 * encoding methods should be considered subject to change
 **/
tl.Bookmarking = new function() {
    var _this = this;

    /**
     * @function ?
     * Load a string value mapped from the key given
     * @return a string value, or null if the key was not mapped
     */
    this.loadValue = function(key){
        var ret = _this.loadAll()[key];
        return ret ? ret : null;
    }

    /**
     * @function ?
     * Load a collection of strings mapped from the key given
     * @param key the string key
     * @return object containing keys mapped to values.
     */
    this.loadValues = function(key){
        var value = _this.loadValue(key);
        if(value){
            var ret = value.split("&");
            for(var i = ret.length-1; i >= 0; i--){
                ret[i] = decodeURIComponent(ret[i]);
            }
            return ret;
        }else{
            return [];
        }
    }

    /** @function ?
     * Store the string -> string key value mapping given
     * @param key key to map from
     * @param value value to map to
     * @return object containing keys mapped to values.
     */
    this.storeValue = function(key, value){
        var attrs = _this.loadAll();
        attrs[key] = value;
        _this.storeAll(attrs);
    }

    /** @function ?
     * Map a collection of strings from the key given
     * @param key the key to map from
     * @param values values to map to
     */
    this.storeValues = function(key, values){
        if(values.length){
            var value = [];
            for(var i = 0; i < values.length; i++){
                value[i] = encodeURIComponent(values[i]);
            }
            _this.storeValue(key, value.join("&"));
        }else{
            _this.storeValue(key, null);
        }
    }

    /** @function ?
     * Load an object of string -> string key value pairs from the url hash parameter
     * @return object containing keys mapped to values.
     */
    this.loadAll = function(){
        var ret = {};
        var items = decodeURIComponent(location.hash.substring(1)).split(";");
        for(var i = 0; i < items.length; i++){
            var parts = items[i].split("=");
            if(parts.length == 2){
                ret[parts[0]] = decodeURIComponent(parts[1]);
            }
        }
        return ret;
    }

    /**
     * @function ?
     * Store an object of string -> string key value pairs to the url hash parameter
     * @param attrs object containing keys mapped to values.
     **/
    this.storeAll = function(attrs){
        var hash = [];
        for(var i in attrs){
            if(attrs[i]){
                hash.push(i,"=",encodeURIComponent(attrs[i]), ";");
            }
        }
        if(hash.length){
            hash.pop();
        }
        location.replace("#"+encodeURIComponent(hash.join("")));
    }
}/** @class tl.ElementRootPane */
/** @constructor ?
 * A pane that will be backed by the DOM element given (the element should have position
 * relative or absolute)
 * @param element the DOM element on which the RootPane will be based
 * @param {optional} contentPane content pane to be placed in the root pane (An instance of tl.Pane)
 */
tl.ElementRootPane = function(element, contentPane){
    var _this = this;
    var processInterval;

    if(window.attachEvent){
        window.attachEvent("onresize", function(){
            //IE fires more resize events than ff - so mute them!
            if(processInterval){
                window.clearInterval(processInterval);
            }
            processInterval = window.setInterval(function(){
                window.clearInterval(processInterval);
                processInterval = null;
                processSize();
            }, 80);
        });
        window.attachEvent("onunload", function(){
            element = null;
            contentPane.dispose();
        });
    }else{
        window.addEventListener("resize", function(){
            processSize();
        }, false);
        window.addEventListener("unload", function(){
            element = null;
            contentPane.dispose();
        }, false);
    }

    /** @function setContentPane
     * Set the content pane to be contained within this root pane
     * @param pane the content pane (An instance of tl.Pane)
     **/
    this.setContentPane = function(pane){
        if(contentPane){
            element.removeChild(contentPane.getElement());
        }
        contentPane = pane;
        element.appendChild(contentPane.getElement());
        processSize();
    }

    /** @function getInnerRect
     * Get the inner rect for this pane
     * @return the inner rect for the document body ([left,top,width,height])
     */
    this.getInnerRect = function(){
        return [0, 0, element.clientWidth, element.clientHeight];
    }

    /** @function getElement
     * Get the element for this pane
     * @return the DOM element for this pane
     */
    this.getElement = function(){
        return element;
    }

    /** Process a change in size for this pane*/
    function processSize(){
        var contentElement = contentPane.getElement();
        var display = contentElement.style.display;
        contentElement.style.display = "none";
        var rect = _this.getInnerRect();
        contentElement.style.display = display;
        contentElement.style.position = "relative";
        contentElement.style.top = "0px";
        contentElement.style.left = "0px";
        contentPane.update(rect);
    }

    if(contentPane){
        element.appendChild(contentPane.getElement());
        processSize();
    }
}
/** @end */
/** @class {static} tl.BodyPane
 * A special static Pane instance representing the document body. Setting the content pane
 * for this objectcauses the pane to fill all available space in the document.
 */
tl.BodyPane = new function(){
    var rootPane;

    /** @function setContentPane
     * Set a pane to fill the current document body.
     * @param pane the pane (An instance of tl.Pane)
     */
    this.setContentPane = function(pane){
        if(!rootPane){
            var rootDiv = document.createElement("div");
            rootDiv.style.position = "absolute";
            rootDiv.style.padding = "0px";
            rootDiv.style.margin = "0px";
            rootDiv.style.left = rootDiv.style.top = "0px";
            rootDiv.style.width = rootDiv.style.height = "100%";
            document.body.appendChild(rootDiv);
            rootPane = new tl.ElementRootPane(rootDiv);
        }
        rootPane.setContentPane(pane);
    }

    /** @function getInnerRect
     * Get the inner rect for this pane
     * @return the inner rect for the document body ([left,top,width,height])
     */
    this.getInnerRect = function(){
        return [0, 0, document.body.clientWidth, document.body.clientHeight];
    }

    /** Process a new size for the document*/
    function processSize(){
        if(rootDiv){
            rootDiv.style.display = "none";
            var rect = tl.BodyPane.getInnerRect();
            rootDiv.style.display = "block";
            var element = contentPane.getElement();
            element.style.position = "absolute";
            element.style.top = "0px";
            element.style.left = "0px";
            contentPane.update(rect, false);
        }
    }
}

/** @class {static} tl.PaneAttrs
 * This object contains the outline of default / accepted attributes for tl.Pane instances
 */
tl.PaneAttrs = {
    /** @property ? Aliases for the pane */
    aliases : null,
    /** @property ? Padding for the pane - Number / Array of 1, 2 or 4 numbers (default : [0,0,0,0]) */
    padding : [0,0,0,0],
    /** @property ? Margin for the pane - Number / Array of 1, 2 or 4 numbers (default : [0,0,0,0]) */
    margin : [0,0,0,0],
    /** @property ? Border size for the pane - Number / Array of 1, 2 or 4 numbers (default : [0,0,0,0]) */
    border : [0,0,0,0],
    /** @property ? CSS class name(s) to be applied to the pane */
    className : "tl_pane",
    /** @property ?  Flag indicating if the flow of inner panes is horizontal. False implies vertical flow (default : false) */
    horizontal : false,
    /** @property ?  Flag indicating that overflowing content should scroll, rather than just be hidden (default : false) */
    scrolling : false,
    /** @property ?  Sizing mode for the Pane. (Default AUTO) @see tl.Sizing */
    units : tl.Sizing.AUTO,
    /** @property ?  Size for the pane - not applicable unless units is PERCENT or PIXEL */
    size : 0,
    /** @property tl.PaneAttrs.domFunctions Dom functions to be used by a pane. By Default, the animated functions (tl.dom.animated) will be used */
    domFunctions : tl.dom.animated,
    /**
     * @function tl.PaneAttrs.httpResponseHandler
     * Default handler for XMLHttpResponses - default places raw data in pane, with no session timeout or error handling
     * @param pane the pane for the response
     * @param xhr XMLHttpRequest instance.
     */
    httpResponseHandler : function(pane, xhr){
        //if(xhr.status == 566){} // Handle session timeout
        pane.getElement().innerHTML = xhr.responseText;
    }
}

/** @class tl.Pane
 * Generic Layout Pane object, with a layout that flows either horizontally or vertically, similar to the BoxLayout in
 * swing. Sizing is always done from the top down, so if an underlying element overflows the space alloted to it, it will
 * not be given more space.
 */
/** @constructor ?
 * @param {optional} attrs - attributes object for this pane
 * @see tl.PaneAttrs
 */
tl.Pane = function(attrs){
    var _this = this;
    var outerRect;
    var children = [];
    var element = document.createElement("div");
    var xhr;
    var eventChannel;

    /**
     * @function ?
     * Get / Set the attributes for this layout pane - the returned object graph
     * should be regarded as immutable - edits to it could result in undefined behaviour.
     * If an attrs object was specified as a parameter its attributes will override
     * any existing ones, and this will be returned.
     * @param {optional} _attrs new attributes to be applied to this pane
     * @return _this if _attrs was defined, the current attrs otherwise.
     * @see tl.PaneAttrs
     */
    this.attrs = function(_attrs){
        if(_attrs){
            var forceUpdate = _attrs.horizontal || _attrs.margin || _attrs.border || attrs.padding;
            _attrs = tl.copyAttributes(attrs, _attrs);
            processAttrs(_attrs, forceUpdate);
            return this;
        }else{
            return attrs; // TODO: maybe defensive copy?
        }
    }

    /**
     * @function ?
     * Get the outer rect defining the total space this pane may occupy - including borders, padding and margin
     * @return array of 4 numbers representing the outer rect defining the total space this pane may occupy [x1,y1,x2,y2]
     */
    this.getOuterRect = function(){
        return outerRect ? tl.toRect(outerRect) : null;
    }

    /**
     * @function ? Get the event channel for this pane
     * @return the event channel for this pane
     */
    this.getEventChannel = function(){
        if(eventChannel == null){
            eventChannel = new tl.EventChannel(); // lazily initialize event channel
        }
        return eventChannel;
    }

    /**
     * @function ?
     * Get the inner rect defining the actual space available to components within this pane
     * @return array of 4 numbers representing the actual space available to components within this pane [x1,y1,x2,y2]
     */
    this.getInnerRect = function(){
        var rect = _this.getOuterRect();
        if(rect){
            var _attrs = attrs;
            var margin = _attrs.margin;
            var border = _attrs.border;
            var padding = _attrs.padding;
            rect[2] -= (margin[0] + margin[2] + border[0] + border[2] + padding[2] + rect[0]);
            rect[3] -= (margin[1] + margin[3] + border[1] + border[3] + padding[3] + rect[1]);
            rect[0] = padding[0];
            rect[1] = padding[1];
        }
        return rect;
    }

    /**
     * @function ?
     * Get the DOM element for this pane - can be used to manually insert elements into this pane - it is not a good idea to mix
     * this with adding child panes
     * @return the DOM element backing this pane
     */
    this.getElement = function(){
        return element
    }

    /**
     * @function ?
     * Get/Set the html contained in this pane. it is not a good idea to mix
     * this with adding child panes
     * @param {optional} html html content for the pane
     * @return this if html content was defined, the html content otherwise
     */
    this.html = function(html){
        if(html == undefined){
            return element.innerHTML;
        }else{
            element.innerHTML = html;
            return _this;
        }
    }

    /**
     * @function ?
     * Adopt the DOM nodes given - if it is previosuly in the DOM, remove it and append it
     * to this panel - accepts 0 - N parameters, and returns this
     * @param node 0 - N nodes or ids of elements accepted as parameters
     */
    this.adopt = function(node){
        for(var i = 0; i < arguments.length; i++){
            var n = arguments[i];
            if(typeof n == "string"){
                n = document.getElementById(n);
                if(!n){
                    throw new Error("Unknown Element : "+arguments[i]);
                }
            }
            if(n.parentNode){
                n.parentNode.removeChild(n);
            }
            attrs.domFunctions.insert(element, n);
        }
        return _this;
    }
    
    /**
     * @function ?
     * Load content from the URL given and place it in this pane. The content processor is
     * responsible for taking the XMLHttpResponse and putting it in the pane.
     * @param url the url to which the request should be sent
     * @param {optional} method the request method (At present only "GET", "POST" and "JSONP" are reliably supported)
     * @param {optional} sendData string of data to send
     * @param {optional} httpResponseHandler function to call on response from server
     */
    this.load = function(url,method,sendData,httpResponseHandler){
        if(!method){
            method = "POST";
        }
        if(!sendData){
            sendData = "";
        }
        if(!httpResponseHandler){
            httpResponseHandler = attrs.httpResponseHandler;
        }
        if(eventChannel){
            eventChannel.fireEvent(tl.Event.LOAD_START, _this, url);
        }
        if(method == "JSONP"){
            var callback = "jsonp"+tl.nextId();
            window[callback] = function(response){
                var responseObject = {
                    responseObject : response,
                    responseText : (typeof response == "string") ? response : JSON.stringify(response)
                }
                httpResponseHandler(_this, responseObject);
                if(eventChannel){
                    eventChannel.fireEvent(tl.Event.LOAD_END, _this, url, xhr)
                }
                window[callback] = httpResponseHandler = null; // prevent memory leaks
            }
            var script = document.createElement("script");
            if(url.indexOf("?") >= 0){
                url += "&jsonp_callback="+callback;
            }else{
                url += "?jsonp_callback="+callback;
            }
            script.src = url;
            var head = document.getElementsByTagName("head").length ? document.getElementsByTagName("head")[0] : document.body;
            head.appendChild(script);
            head = null;
        }else{
            if(xhr){
                xhr.abort(); // cancel any loading request in progress
            }
            xhr = newXHR();
            xhr.onreadystatechange = function(){
                if(xhr.readyState==4){
                    httpResponseHandler(_this, xhr);
                    if(eventChannel){
                        eventChannel.fireEvent(tl.Event.LOAD_END, _this, url, xhr)
                    }
                    xhr = httpResponseHandler = null; // clean up the mess
                }
            } 
            xhr.open(method,url,true);
            xhr.send(sendData);
        }
    }

    /**
     * @function ?
     * Insert a child pane into this pane
     * @param pane the child pane object
     * @param index (optional - defaults to end) index at which to insert pane
     * @param noUpdate (optional - defaults to false) flag that causes no visual update to be performed to the child nodes. (useful for consolidating multiple updates into a single update)
     * @param noAnim (optional - defaults to false) flag that causes this operation to take place without animation
     * @return this
     */
    this.insertChild = function(pane, index, noUpdate, noAnim){
        if(index == undefined){
            index = children.length;
        }
        var paneElement = pane.getElement();
        children.splice(index,0,pane);
        var domFunctions = noAnim ? tl.dom.basic : attrs.domFunctions;
        domFunctions.insert(element, paneElement, ((index+1) < children.length) ? children[index+1].getElement() : null);
        pane.inserted(_this);
        if(outerRect && (!noUpdate)){
            updateChildren(noAnim);
        }
        return _this;
    }

    /**
     * @function ?
     * Remove the child pane at the index given
     * @param index the index of the child to remove
     * @param noUpdate (optional - defaults to false) flag that causes no visual update to be performed to the child nodes. (useful for consolidating multiple updates into a single update)
     * @param noAnim (optional - defaults to false) flag that causes this operation to take place without animation
     * @return this
     */
    this.removeChild = function(index, noUpdate, noAnim){
        var child = children[index];
        var domFunctions = noAnim ? tl.dom.basic : attrs.domFunctions;
        domFunctions.remove(child.getElement());
        children.splice(index, 1);
        child.removed(_this);
        if(outerRect && (!noUpdate)){
            updateChildren(noAnim);
        }
        return _this;
    }

    /**
     * @function ? Get the child pane at the index given
     * @return the child pane at the index given
     */
    this.getChild = function(index){
        return children[index]
    }

    /**
     * @function ? Get all child panes - returned object should be considered immutable
     * @return all child panes
     */
    this.getChildren = function(){
        return children;
    }

    /**
     * @function ? Update this pane to use the outerRect given, effectively resizing it
     * @param _outerRect the new outerRect for this pane
     * @param {optional} noAnim - flag that when true, no animation should be involved. (it should be instant)
     * @param {optional} forceUpdate - flag that when true, updates to child panes should occur even if there is no change to the size of the outerRect
     */
    this.update = function(_outerRect, noAnim, forceUpdate){
        _outerRect = tl.toRect(_outerRect); // set outer rect
        if(forceUpdate || (!outerRect) || (outerRect[0] != _outerRect[0]) || (outerRect[1] != _outerRect[1]) || (outerRect[2] != _outerRect[2]) || (outerRect[3] != _outerRect[3])){
            if((_outerRect[0] < _outerRect[2]) && (_outerRect[1] < _outerRect[3])){
                if(eventChannel){
                    eventChannel.fireEvent(tl.Event.UPDATING, _this, _outerRect);
                }
                outerRect = _outerRect;
            
                //Set element attributes
                var style = element.style;
                var toRect = tl.toRect;
                var _attrs = attrs;
                
                var callback;
                if(eventChannel){
                    callback = function(){
                        eventChannel.fireEvent(tl.Event.UPDATED, _this, _outerRect);
                    }
                }
                var innerRect = _this.getInnerRect(); //derive inner rect
                var domFunctions = noAnim ? tl.dom.basic : _attrs.domFunctions; // if animation is off, use the basic dom function
                domFunctions.setBounds(element, Math.round(outerRect[0]), Math.round(outerRect[1]), Math.round(innerRect[2]-innerRect[0]), Math.round(innerRect[3]-innerRect[1]), callback);
                updateChildren(noAnim, forceUpdate, innerRect);
            }
        }
    }

    /** @function ? Dispose of this pane and all child panes */
    this.dispose = function(){
        if(attrs.aliases && attrs.aliases.length){
            _this.handleAliasing([], attrs.aliases); // remove aliases
        }
        var c = children;
        for(var i = c.length-1; i >= 0; i--){
            c[i].dispose();
        }
        if(element.parentNode){
            element.parentNode.removeChild(element);
        }
        if(eventChannel){
            eventChannel.fireEvent(tl.Event.DISPOSED, _this);
            eventChannel.dispose();
        }
        element = outerRect = children = _this = null;
    }

    /** @function ? Callback function invoked when this pane was removed from its parent pane */
    this.removed = function(parent){
        if(eventChannel){
            eventChannel.fireEvent(tl.Event.REMOVED, _this, parent);
        }
    }

    /** @function ? Callback function invoked when this pane was inserted to a parent pane */
    this.inserted = function(parent){
        if(eventChannel){
            eventChannel.fireEvent(tl.Event.INSERTED, _this, parent);
        }
    }

    /**
     * @function {protected} ? Function called internally from attrs to apply/remove aliases for a pane.
     * This function is public so as to allow it to be overridden by wrappers/subclasses.
     */
    this.handleAliasing = function(aliases, prevAliases){
        var i;
        if(prevAliases){
            for(i = prevAliases.length-1; i >= 0; i--){
                tl.removeAlias(prevAliases[i], _this);
            }
        }
        for(i = aliases.length-1; i >= 0; i--){
            tl.addAlias(aliases[i], _this);
        }
    }

    /** Update the child panes */
    function updateChildren(noAnim, forceUpdate, innerRect){
        if(!innerRect){
            innerRect = _this.getInnerRect();
        }
        var offset = attrs.horizontal ? 0 : 1;
        var totalSize = (innerRect[2+offset] - innerRect[offset]);
        var remainingSize = totalSize;
        var numAutoSizedPanes = 0;
        var c = children;
        var i, pane, units, size, paneAttrs;
        for(i = c.length-1; i >= 0; i--){
            pane = c[i];
            paneAttrs = pane.attrs();
            units = paneAttrs.units;
            size = paneAttrs.size;
            if((!(units && size)) || (size < 0)){
                units = tl.Sizing.AUTO;
            }
            if(units ==  "px"){
                remainingSize -= size;
            }else if(units == tl.Sizing.PERCENT){
                remainingSize -= (size*totalSize/100);
            }else{
                numAutoSizedPanes++;
            }
        }
        var allAutoSized = false;
        if(Math.round(remainingSize) < 0){
            remainingSize = totalSize;
            if(!attrs.scrolling){ //if not scrolling, all are auto...
                allAutoSized = true;
                numAutoSizedPanes = children.length;
            }
        }
        var autoSize = remainingSize/numAutoSizedPanes;
        var rect = innerRect.slice(0,4);
        rect[2+offset] = innerRect[offset];
        for(i = 0; i < c.length; i++){
            rect[offset] = rect[2+offset];
            pane = c[i];
            paneAttrs = pane.attrs();
            units = paneAttrs.units;
            size = paneAttrs.size;
            if((!allAutoSized) && (units == "px")){
                rect[2+offset] = rect[offset] + size;
            }else if((!allAutoSized) && (units == "%")){
                rect[2+offset] = rect[offset] + (size*totalSize/100);
            }else{
                rect[2+offset] = rect[offset]+autoSize;
            }
            var e = pane.getElement();
            e.style.position = "absolute";
            var m
            pane.update(rect, noAnim, forceUpdate);
        }
    }

    /** Process attribute updates for the pane */
    function processAttrs(_attrs, forceUpdate) {
        if(_attrs.aliases){
            _this.handleAliasing(_attrs.aliases, attrs.aliases);
        }
        attrs = _attrs;
        element.className = _attrs.className;
        var style = element.style;
        style.overflow = _attrs.scrolling ? "auto": "hidden";
        var toRect = tl.toRect;
        var margin = _attrs.margin = toRect(_attrs.margin);
        style.marginLeft = margin[0]+"px";
        style.marginTop = margin[1]+"px";
        style.marginRight = margin[2]+"px";
        style.marginBottom = margin[3]+"px";

        var border = _attrs.border = toRect(_attrs.border);
        style.borderStyle = "solid";
        style.borderLeftWidth = border[0]+"px";
        style.borderTopWidth = border[1]+"px";
        style.borderRightWidth = border[2]+"px";
        style.borderBottomWidth = border[3]+"px";

        var padding = _attrs.padding = toRect(_attrs.padding);
        style.paddingLeft = padding[0]+"px";
        style.paddingTop = padding[1]+"px";
        style.paddingRight = padding[2]+"px";
        style.paddingBottom = padding[3]+"px";

        if(outerRect){
            _this.update(outerRect, false, forceUpdate);
        }
    }

    /**
     * A cross browser create for an XMLHttpRequest
     * @return a new XMLHttpRequest
     */
    function newXHR(){
        try{
            return new XMLHttpRequest();// Firefox, Opera 8.0+, Safari
        }catch(e){
            try{
                return new ActiveXObject("Msxml2.XMLHTTP");
            }catch(e){
                try{
                    return new ActiveXObject("Microsoft.XMLHTTP");
                }
                catch(e){
                    return null;
                }
            }
        }
    }

    if(!attrs){
        attrs = {};
    }
    attrs = tl.copyAttributes(tl.PaneAttrs, attrs);
    processAttrs(attrs, true);
}

/** @class {static} tlSplitPaneAttrs
 * This object contains the outline of default / accepted attributes for tl.SplitPane instances.
 * @extends tl.PaneAttrs
 */
tl.SplitPaneAttrs = tl.copyAttributes(tl.PaneAttrs, {
    /** @property ? Flag indicating that this split pane may be resized (Default : true) */
    resizable : true,
    /** @property ? Flag indicating that the split pane can be collapsed by mouse interraction (Default : true) */
    collapsable : true,
    /** @property ? Flag indicating whether resizing / collapsing should be applied to pane B (Default : false) */
    manageB : false,
    /** @property ? Min size (in pixels) for the managed part of a split pane (Default : 20) */
    minSize : 20,
    /** @property ? Max size (in pixels) for the managed part of a split pane (Default : 0 - no max size) */
    maxSize : 0,
    /** @property ? Flag indicating whether the split pane is horizontal (Default true) */
    horizontal : true,
    /**
     * @property ? Attributes for pane A
     * @see tl.PaneAttrs
     */
    paneAAttrs : tl.copyAttributes(tl.PaneAttrs,{}),
    /**
     * @property ? Attributes for pane B
     * @see tl.PaneAttrs
     */
    paneBAttrs : tl.copyAttributes(tl.PaneAttrs,{}),
    /**
     * @property ? Attributes for the divisor. Customisations have been made as follows:
     * className - "tl_divisor_vertical tl_divisor_horizontal", activeClass - "tl_active",
     * collapsedClass - "tl_collapsed", units - PIXEL, size - 10, border - 1
     * @see tl.PaneAttrs
     */
    divisorAttrs : tl.copyAttributes(tl.PaneAttrs, {
        /** Takes either of the values specified depending on the orientation of the pane */
        className : "tl_divisor_vertical tl_divisor_horizontal",
        /** Class to be appended when active*/
        activeClass : "tl_active",
        /** Collapsed class for divisor*/
        collapsedClass : "tl_collapsed",
        /** Default divisor size is 10 pixel*/
        units : tl.Sizing.PIXEL,
        /** Default divisor size is 10 pixel*/
        size : 10,
        /** Default population*/
        border: 1
    }),
    /** @property ? The css class name for split panes (Default "tl_split") */
    className : "tl_split",
    /** @property ? Whether the managed pane is collapsed. (Default : false) */
    collapsed : false,
    /** @property ? Whether the split pane state is bookmarkable. (Default : false) */
    bookmarkable : false
});

/** @class {static} tl.SplitEvent event definitions for tab panes. */
tl.SplitEvent = {
    /** @property {static final string} ? Indicates a split pane was collapsed {params (splitPane)} */
    COLLAPSED : "collapsed",
    /** @property {static final string} ? Indicates a split pane was expanded {params (splitPane)} */
    EXPANDED : "expanded"
}

/** @class tl.SplitPane
 * A Split Pane object, allowing 2 content panes to be dynamically resized against each other using the mouse
 */
/** @constructor ?
 * @param {optional} attrs - attributes object for this pane
 * @see tl.SplitPaneAttrs
 */
tl.SplitPane = function(attrs){

    var _this = this;
    if(!attrs){
        attrs = {};
    }
    if(!attrs.id){
        attrs.id = tl.nextId();
    }
    if(typeof attrs.horizontal == "undefined"){
        attrs.horizontal = true;
    }
    if(!attrs.divisorAttrs){
        attrs.divisorAttrs = tl.copyAttributes(tl.SplitPaneAttrs.divisorAttrs);
        attrs.divisorAttrs.className = null;
    }
    if(!attrs.divisorAttrs.className){
        attrs.divisorAttrs.className = "tl_divisor_" + (attrs.horizontal ? "vertical" : "horizontal");
    }
    attrs = tl.copyAttributes(tl.SplitPaneAttrs, attrs);
    var pane = new tl.Pane(attrs); // outer pane
    this.id = attrs.id;
    if(attrs.bookmarkable){
        new function(){
            var size = parseInt(tl.Bookmarking.loadValue(_this.id));
            if(!size){
                if(size == 0){
                    attrs.collapsed = true;
                }
            }else{
                var attrName = attrs.manageB ? "paneBAttrs" : "paneAAttrs";
                attrs[attrName] = tl.copyAttributes(attrs[attrName], {size:size,units:"px"});
            }
        }
    }
    var paneA = new tl.Pane(attrs.paneAAttrs); // Left / Top pane
    var paneB = new tl.Pane(attrs.paneBAttrs); // Right / Bottom pane
    var divisor = new tl.Pane(attrs.divisorAttrs); // Divisor pane
    var dragOrigin; // Drag origin screen coordinates
    var mouseMoved; // Flag indicating that the mouse moved
    var sizeOrigin; // Original size when dragging
    var collapsed = attrs.collapsed; // Flag for collapsed state
    var eventChannel;
    
    /**
     * @function ?
     * Get / Set the attributes for this layout pane - the returned object graph
     * should be regarded as immutable - edits to it could result in undefined behaviour.
     * If an attrs object was specified as a parameter its attributes will override
     * any existing ones, and this will be returned.
     * @param {optional} _attrs new attributes to be applied to this pane
     * @return _this if _attrs was defined, the current attrs otherwise.
     * @see tl.SplitPaneAttrs
     */
    this.attrs = function(_attrs){
        if(_attrs){
            tl.copyAttributes(attrs, _attrs);
            pane.attrs(_attrs);
            paneA.attrs(_attrs.paneAAttrs);
            paneB.attrs(_attrs.paneBAttrs);
            divisor.attrs(_attrs.divisorAttrs);
            if(_attrs.collapsed != collapsed){
                _this.toggle();
            }
            attrs = _attrs;
            return _this;
        }else{
            return attrs;
        }
    }

    /** @function ? Dispose of this pane and all child panes */
    this.dispose = function(){
        pane.dispose();
        pane = _this = attrs = paneA = paneB = divisor = null;
    }

    /**
     * @function ? Get the Left or Top pane. (depending on whether the layout is horizontal or not)
     * @return paneA
     */
    this.getPaneA = function(){
        return paneA;
    }

    /**
     * @function ? Get the Right or Bottom pane. (depending on whether the layout is horizontal or not)
     * @return paneB
     */
    this.getPaneB = function(){
        return paneB;
    }

    /**
     * @function ?
     * Get the outer rect defining the total space this pane may occupy - including borders, padding and margin
     * @return array of 4 numbers representing the outer rect defining the total space this pane may occupy [x1,y1,x2,y2]
     */
    this.getOuterRect = pane.getOuterRect;
    /**
     * @function ?
     * Get the inner rect defining the actual space available to components within this pane
     * @return array of 4 numbers representing the actual space available to components within this pane [x1,y1,x2,y2]
     */
    this.getInnerRect = pane.getInnerRect;
    /**
     * @function ?
     * Get the DOM element for this pane - can be used to manually insert elements into this pane - it is not a good idea to mix
     * this with adding child panes
     * @return the DOM element backing this pane
     */
    this.getElement = pane.getElement;
    /**
     * @function ? Update this pane to use the outerRect given, effectively resizing it
     * @param _outerRect the new outerRect for this pane
     * @param {optional} noAnim - flag that when true, no animation should be involved. (it should be instant)
     * @param {optional} forceUpdate - flag that when true, updates to child panes should occur even if there is no change to the size of the outerRect
     */
    this.update = function(_outerRect, noAnim, forceUpdate){
        //if it looks like overflow will occur, reset managed pane to AUTO.
        var managed = attrs.manageB ? paneB : paneA;
        var managedAttrs = managed.attrs();
        if(managedAttrs.units != tl.Sizing.AUTO){
            var _attrs = attrs;
            var padding = tl.toRect(_attrs.padding);
            var margin = tl.toRect(_attrs.margin);
            var border = tl.toRect(_attrs.border);
            var availableSize;
            if(_attrs.horizontal){
                availableSize = _outerRect[2] - (_outerRect[0] + padding[0] + padding[2] + margin[0] + margin[2] + border[0] + border[2]);
            }else{
                availableSize = _outerRect[3] - (_outerRect[1] + padding[1] + padding[3] + margin[1] + margin[3] + border[1] + border[3]);
            }
            availableSize -= divisor.attrs().size;
            if(availableSize <= managedAttrs.size){
                managed.attrs({units:tl.Sizing.AUTO,size:0}); // Reset size management
            }
        }
        pane.update(_outerRect, noAnim, forceUpdate);
    }
    /**
     * @function ? Get the event channel for this pane
     * @return the event channel for this pane
     */
    this.getEventChannel = function(){
        eventChannel = pane.getEventChannel();
        return eventChannel;
    }
    /** @function ? Callback function invoked when this pane was inserted to a parent pane */
    this.inserted = pane.inserted;
    /** @function ? Callback function invoked when this pane was removed from its parent pane */
    this.removed = pane.removed;

    /** @function ? Toggle the managed pane expanded/collapsed state. */
    this.toggle = function(noAnim) {
        //expand/collapse panel
        if(collapsed){
            //divisor.className = attrs.divisorAttrs.className;
            pane.insertChild((attrs.manageB ? paneB : paneA), (attrs.manageB ? 2 : 0), false, noAnim);
        }else{
            //divisor.className = attrs.divisorAttrs.className+" "+attrs.divisorAttrs.collapsedClass;
            pane.removeChild((attrs.manageB ? 2 : 0), false, noAnim);
        }
        collapsed = !collapsed;
        if(attrs.bookmarkable){
            tl.Bookmarking.storeValue(_this.id, collapsed ? "0" : (attrs.manageB ? paneB : paneA).attrs().size);
        }
        if(eventChannel){
            eventChannel.fireEvent((collapsed ? tl.SplitEvent.COLLAPSED : tl.SplitEvent.EXPANDED), _this);
        }
        return _this;
    }

    /** callback function for mousedown on divisor */
    function mousedown(event){
        event = event ? event : window.event;
        dragOrigin = [event.clientX, event.clientY];
        sizeOrigin = (attrs.manageB ? paneB : paneA).getOuterRect();
        mouseMoved = false;
        //Change cursor, divisor class, and draw rect...
        var element = divisor.getElement();
        element.className = element.className + " " + attrs.divisorAttrs.activeClass;
        //Attach move & up listener to window
        if(window.attachEvent){
            document.body.attachEvent("onmousemove", mousemove);
            document.body.attachEvent("onmouseup", mouseup);
        }else{
            window.addEventListener("mousemove", mousemove, true);
            window.addEventListener("mouseup", mouseup, true);
        }
    }

    /** callback function for mmousemove after mousedown on divisor */
    function mousemove(event){
        if(!collapsed && attrs.resizable){
            mouseMoved = true;
            event = event ? event : window.event;
            var panel = attrs.manageB ? paneB : paneA;
            var delta;
            var size;
            var innerRect = _this.getInnerRect();
            var maxSize;
            if(attrs.horizontal){
                delta = attrs.manageB ? (dragOrigin[0] - event.clientX) : (event.clientX - dragOrigin[0]);
                size = (sizeOrigin[2] - sizeOrigin[0]);
                maxSize = attrs.maxSize ? attrs.maxSize : (innerRect[2] - innerRect[0] - attrs.minSize);
            }else{
                delta = attrs.manageB ? (dragOrigin[1] - event.clientY) : (event.clientY - dragOrigin[1]);
                size = (sizeOrigin[3] - sizeOrigin[1]);
                maxSize = attrs.maxSize ? attrs.maxSize : (innerRect[3] - innerRect[1] - attrs.minSize);
            }
            var paneAttrs = panel.attrs();
            paneAttrs.size = paneAttrs.size = size+delta;
            if(paneAttrs.size < attrs.minSize){
                paneAttrs.size = attrs.minSize;
            }else if(paneAttrs.size > maxSize){
                paneAttrs.size = maxSize;
            }
            paneAttrs.units = "px";
            pane.update(pane.getOuterRect(), true, true);
            if(attrs.bookmarkable){
                tl.Bookmarking.storeValue(_this.id, (attrs.manageB ? paneB : paneA).attrs().size);
            }
        }
    }

    /** callback function for mouseup after mousedown on divisor */
    function mouseup(){
        var element = divisor.getElement();
        element.className = attrs.divisorAttrs.className;
        if(!mouseMoved && attrs.collapsable){
            _this.toggle();
        }
        if(window.attachEvent){
            document.body.detachEvent("onmousemove", mousemove);
            document.body.detachEvent("onmouseup", mouseup);
        }else{
            window.removeEventListener("mousemove", mousemove, true);
            window.removeEventListener("mouseup", mouseup, true);
        }
    }

    /**
     * @function {protected} ? Function called internally from attrs to apply/remove aliases for a pane.
     * This function is public so as to allow it to be overridden by wrappers/subclasses.
     */
    pane.handleAliasing = function(aliases, prevAliases){
        var i;
        if(prevAliases){
            for(i = prevAliases.length-1; i >= 0; i--){
                tl.removeAlias(prevAliases[i], _this);
            }
        }
        for(i = aliases.length-1; i >= 0; i--){
            tl.addAlias(aliases[i], _this);
        }
    }

    pane.attrs(attrs);
    if(!collapsed || attrs.manageB){
        pane.insertChild(paneA);
    }
    pane.insertChild(divisor);
    if((!collapsed) || (!attrs.manageB)){
        pane.insertChild(paneB);
    }
    pane.attrs({domFunctions : tl.dom.fading});
    divisor.getElement().onkeydown = function(event){
        event = event ? event : window.event;
        if(event.keyCode == 13){
            _this.toggle();
        }
    }
    divisor.getElement().tabIndex = 0; // The divisor should be keyboard accessible
    divisor.getElement().onmousedown = mousedown;
}
/** @class {static} tl.TabPaneAttrs
 * This object contains the outline of default / accepted attributes for TTabPane instances
 * @extends tl.PaneAttrs
 */
tl.TabPaneAttrs = tl.copyAttributes(tl.PaneAttrs, {
    /** @property ? default 5 Pixel margin*/
    margin : 5,
    /** @property ? tabs are vertical - header on top, main beneath*/
    horizontal : false,
    /** @property ? The css class name for this pane ("tl_tabs") */
    className : "tl_tabs",
    /** @property ?  Attributes for the header that will contain the tabs*/
    headerAttrs : tl.copyAttributes(tl.PaneAttrs,{className : "tl_tabs_header", horizontal : true, units : tl.Sizing.PIXEL, size : 30}),
    /** @property ?  Attributes for the main panel where tab content will be displayed */
    mainAttrs : tl.copyAttributes(tl.PaneAttrs,{className : "tl_tabs_main", border : 1, padding : 5, domFunctions : tl.dom.fading}),
    /** @property ?  Attributes for a tab */
    tabAttrs : tl.copyAttributes(tl.PaneAttrs,{className : "tl_tab", activeClass : "tl_activeTab", margin : [1,2,1,0], border : [1,1,1,0]}),
    /** @property ?  If true, the application will attempt to synchronize tab events with the back button - changing this value once it has been set is not supported. */
    bookmarkable : true
});

/** @class {static} tl.TabEvent event definitions for tab panes. */
tl.TabEvent = {
    /** @property {static final string} ? Indicates a tab was activated {params (index, tab, tabPane)} */
    ACTIVATED : "tabActivated",
    /** @property {static final string} ? Indicates a tab was deactivated {params (index, tab, tabPane)} */
    DEACTIVATED : "tabDeactivated",
    /** @property {static final string} ? Indicates a tab was deselected {params (index, tab, tabPane)} */
    INSERTED : "tabInserted",
    /** @property {static final string} ? Indicates a tab was deselected {params (index, tab, tabPane)} */
    REMOVED : "tabRemoved"
}

/** @class tl.TabPane
 * Implementation of an tab pane that allows a single element to open at a time.
 */
/** @constructor ?
 * @param {optional} attrs - attributes object for this pane
 * @param {optional} toAdopt - dom node to adopt as tabs
 * @see tl.TabPaneAttrs
 */
tl.TabPane = function(attrs, toAdopt){
    var _this = this;
    _this.id = (attrs && attrs.id) ? attrs.id : tl.nextId();
    attrs = tl.copyAttributes(tl.TabPaneAttrs, attrs);
    var pane = new tl.Pane();
    var headerPane = new tl.Pane(attrs.headerAttrs);
    var mainPane = new tl.Pane(attrs.mainAttrs);
    pane.insertChild(headerPane);
    pane.insertChild(mainPane);
    var tabs = [];
    var activeTab = null;
    var eventChannel;

    /** @function ?
     * Get / Set the attributes for this layout pane - the returned object graph
     * should be regarded as immutable - edits to it could result in undefined behaviour.
     * If an attrs object was specified as a parameter its attributes will override
     * any existing ones, and this will be returned.
     * @param {optional} _attrs new attributes to be applied to this pane
     * @return _this if _attrs was defined, the current attrs otherwise.
     * @see tl.TabPaneAttrs
     */
    this.attrs = function(_attrs){
        if(_attrs){
            tl.copyAttributes(attrs, _attrs);
            pane.attrs(_attrs);
            headerPane.attrs(_attrs.headerAttrs);
            mainPane.attrs(_attrs.mainAttrs);
            for(var i = 0; i < tabs.length; i++){
                tabs[i].tabPane.attrs(_attrs.tabAttrs);
            }
            attrs = _attrs;
            return _this;
        }else{
            return attrs;
        }
    }

    /**
     * @function ?
     * Add a tab to this tab pane. If  this is the first tab to be added, the tab will activate.
     * Alternatively, if the tab pane is bookmarkable and the bookmarking indicates that the
     * tab is active, the tab will activate
     * @param title the title for the tab
     * @param pane the pane to add
     * @param index {optional} the index at which to add the pane, starting at 0
     * @param id {optional} a unique id for the tab. (Used in bookmarking)
     * @return this
     */
    this.insertTab = function(title, pane, index, id){
        if(arguments.length < 3){
            index = tabs.length;
        }
        var tabPane = new tl.Pane(attrs.tabAttrs);
        var tab = tabs[index] = {id:id?id:tl.nextId(),title:title,contentPane:pane,tabPane:tabPane};
        var element = tabPane.getElement();
        element.setAttribute("title", title);
        element.innerHTML = title;
        element.tabIndex = 0;
        element.onclick = function(){
            activateTab(tab);
        }
        element.onkeydown = function(event){
            event = event ? event : window.event;
            if(event.keyCode == 13){
                activateTab(tab);
            }
        }
        headerPane.insertChild(tabPane, index);
        if(!activeTab){
            attrs.bookmarkable = false;
            activateTab(tab);
            attrs.bookmarkable = true;
        }else if(attrs.bookmarkable && (tl.Bookmarking.loadValue(_this.id) == tab.id)){
            activateTab(tab);
        }
        tabPane = element = pane = null; // clear up potential memory leaks
        if(eventChannel){
            eventChannel.fireEvent(tl.TabEvent.INSERTED, index, tab, _this);
        }
        return _this;
    }

    /**
     * @function ?
     * Remove a tab from this tab pane. If the  tab is active and there are other tabs,
     * the first tab is activated.
     * @param index the index of the tab to remove
     * @return this
     */
    this.removeTab = function(index){
        var tab = tabs[index];
        if(activeTab == tab){
            if(tabs.length == 1){
                mainPane.removeChild(0)
                activeTab = null;
                if(eventChannel){
                    eventChannel.fireEvent(tl.TabEvent.DEACTIVATED, index, tab, _this);
                }
            }else{
                _this.activateTab(index ? (index - 1) : 1);
            }
        }
        headerPane.removeChild(index);
        tabs.splice(index, 1);
        if(eventChannel){
            eventChannel.fireEvent(tl.TabEvent.REMOVED, index, tab, _this);
        }
        return _this;
    }

    /**
     *@function ?
     * Get the tab at the index given
     * @param index the index for the tab
     * @return the tab at the index given. Changing attributes of this may result in undefined behaviour
     */
    this.getTab = function(index){
        return tabs[index];
    }

    /**
     * @function ?
     * Get all tabs. Changing elements of the returned array may result in undefined behaviour.
     * @return all tabs
     */
    this.getTabs = function(){
        return tabs;
    }
       
    /**
     * @function ?
     * Get the index of the tab given
     * @return the index of the tab given, or -1 if the tab was not found
     */
    this.indexOfTab = function(tab){
        var t;
        for(t = tabs.length-1; t >= 0; t--){
            if(tabs[t] == tab){
                break;
            }
        }
        return t;
    }
         
    /**
     * @function ?
     * Get the current active tab
     * @return the current active tab
     */
    this.getActiveTab = function(){
        return activeTab;
    }
    
    /** @function ?
     *  Activate the tab at the index given (if already active this has no effect)
     *  @param index the index of the tab to activate
     *  @return this
     */
    this.activateTab = function(index){
        activateTab(tabs[index]);
        return _this;
    }

    /** @function ? Dispose of this tab pane */
    this.dispose = function(){
        pane.dispose();
        attrs = pane = _this = headerPane = mainPane = tabs = activeTab = null;
    }

   /**
     * @function ?
     * Get the outer rect defining the total space this pane may occupy - including borders, padding and margin
     * @return array of 4 numbers representing the outer rect defining the total space this pane may occupy [x1,y1,x2,y2]
     */
    this.getOuterRect = pane.getOuterRect;
    /**
     * @function ?
     * Get the inner rect defining the actual space available to components within this pane
     * @return array of 4 numbers representing the actual space available to components within this pane [x1,y1,x2,y2]
     */
    this.getInnerRect = pane.getInnerRect;
    /**
     * @function ?
     * Get the DOM element for this pane - can be used to manually insert elements into this pane - it is not a good idea to mix
     * this with adding child panes
     * @return the DOM element backing this pane
     */
    this.getElement = pane.getElement;
    /**
     * @function ? Update this pane to use the outerRect given, effectively resizing it
     * @param _outerRect the new outerRect for this pane
     * @param {optional} noAnim - flag that when true, no animation should be involved. (it should be instant)
     * @param {optional} forceUpdate - flag that when true, updates to child panes should occur even if there is no change to the size of the outerRect
     */
    this.update = pane.update;
    /**
     * @function ? Get the event channel for this pane
     * @return the event channel for this pane
     */
    this.getEventChannel = function(){
        eventChannel = pane.getEventChannel();
        return eventChannel;
    }
    /** @function ? Callback function invoked when this pane was inserted to a parent pane */
    this.inserted = pane.inserted;
    /** @function ? Callback function invoked when this pane was removed from its parent pane */
    this.removed = pane.removed;

    /** Activate the tab given */
    function activateTab(tab){
        if(attrs.bookmarkable){
            tl.Bookmarking.storeValue(_this.id, tab.id);
        }
        if(activeTab != tab){
            if(activeTab){
                activeTab.tabPane.getElement().className = attrs.tabAttrs.className;
                mainPane.removeChild(0);
                if(eventChannel){
                    eventChannel.fireEvent(tl.TabEvent.DEACTIVATED, _this.indexOfTab(activeTab), activeTab, _this);
                }
            }
            activeTab = tab;
            tab.tabPane.getElement().className = attrs.tabAttrs.className+" "+attrs.tabAttrs.activeClass;
            mainPane.insertChild(tab.contentPane);
            if(eventChannel){
                eventChannel.fireEvent(tl.TabEvent.ACTIVATED, _this.indexOfTab(tab), tab, _this);
            }
        }
    }

    // Function called internally from attrs to apply/remove aliases for a pane.
    // This function is publis so as to allow it to be overridden by wrappers/subclasses.
    pane.handleAliasing = function(aliases, prevAliases){
        var i;
        if(prevAliases){
            for(i = prevAliases.length-1; i >= 0; i--){
                tl.removeAlias(prevAliases[i], _this);
            }
        }
        for(i = aliases.length-1; i >= 0; i--){
            tl.addAlias(aliases[i], _this);
        }
    }

    pane.attrs(attrs);
    if(toAdopt){
        tl.domElementToTabs(this, toAdopt);
    }
}
/** @class {static} tl.AccordionPaneAttrs
 * This object contains the outline of default / accepted attributes for tl.AccordionPane instances
 * @extends tl.PaneAttrs
 */
tl.AccordionPaneAttrs = tl.copyAttributes(tl.PaneAttrs, {
    /** @property ? Accordions are normally vertical. Horizontal accordions are currently not well suppored. */
    horizontal : false,
    /** @property ? Flag indicating that the accordion should only active a single element at a time and cannot active multiple elements at a time (Default true) */
    singleMode : true,
    /** @property ? The css class name for this pane (Default "tl_accordion") */
    className : "tl_accordion",
    /** @property ? Attributes for the main panels where tab content will be displayed. Default to tl.PaneAttrs with className "tl_accordion_main"  and border [1,0,1,0] */
    mainAttrs : tl.copyAttributes(tl.PaneAttrs,{className : "tl_accordion_main", border : [1,0,1,0]}),
    /** @property ? Attributes for a tab. Default to tl.PaneAttrs with className "tl_tab", activeClass "tl_activeTab", units PIXEL, size 30, and border 1 */
    tabAttrs : tl.copyAttributes(tl.PaneAttrs,{className : "tl_tab", activeClass : "tl_activeTab", units : tl.Sizing.PIXEL, size : 30, border : 1}),
    /** @property ? flag that if true, the application will attempt to synchronize tab events with the back button - changing this value once it has been set is not supported. (Default : true) */
    bookmarkable : true
});

/** @class tl.AccordionPane
 * Implementation of an accordion pane that allows a single element to active at a time, or multiple elements to be active at a time.
 */
/** @constructor ?
 * @param {optional} attrs - attributes object for this pane
 * @param {optional} toAdopt - dom node to adopt as tabs
 * @see tl.AccordionPaneAttrs
 */
tl.AccordionPane = function(attrs, toAdopt){
    var _this = this;
    attrs = tl.copyAttributes(tl.AccordionPaneAttrs, attrs);
    _this.id = attrs.id ? attrs.id : tl.nextId();
    var pane = new tl.Pane();
    var tabs = [];
    var eventChannel;
    
    /**
     * @function ?
     * Get / Set the attributes for this layout pane - the returned object graph
     * should be regarded as immutable - edits to it could result in undefined behaviour.
     * If an attrs object was specified as a parameter its attributes will override
     * any existing ones, and this will be returned.
     * @param {optional} _attrs new attributes to be applied to this pane
     * @return _this if _attrs was defined, the current attrs otherwise.
     * @see tl.AccordionPaneAttrs
     */
    this.attrs = function(_attrs){
        if(_attrs){
            tl.copyAttributes(attrs, _attrs);
            pane.attrs(_attrs);
            var mainAttrs = _attrs.mainAttrs;
            var tabAttrs = _attrs.tabAttrs;
            var singleMode = _attrs.singleMode;
            var deactivate = false;
            for(var i = 0; i < tabs.length; i++){
                var tab = tabs[i];
                tab.tabPane.attrs(tabAttrs);
                if(tab.active){
                    tab.tabPane.getElement().className = attrs.tabAttrs.className+" "+attrs.tabAttrs.activeClass;
                }
                tab.mainPane.attrs(mainAttrs);
                if(singleMode){
                    if(tab.active){
                        if(deactivate){
                            deactivateTab(tab);
                        }else{
                            deactivate = true;
                        }
                    }
                }
            }
            attrs = _attrs;
            return _this;
        }else{
            return attrs;
        }
    }

    /**
     * @function ?
     * Add a tab to this accordion pane. If the accordion pane is in single mode and
     * this is the first tab to be added, the tab will activate. Alternatively, if the
     * accordion pane is bookmarkable and the bookmarking indicates that the tab is active,
     * the tab will activate
     * @param title the title for the tab
     * @param contentPane the pane to add
     * @param index {optional} the index at which to add the pane, starting at 0
     * @param id {optional} a unique id for the tab. (Used in bookmarking)
     * @param activate whether the tab should be active
     * @return this
     */
    this.insertTab = function(title, contentPane, index, id, activate){
        if(arguments.length < 3){
            index = tabs.length;
        }
        var _attrs = attrs;
        var tabPane = new tl.Pane(_attrs.tabAttrs);
        var mainPane = new tl.Pane(_attrs.mainAttrs).insertChild(contentPane);
        var tab = {id:id?id:tl.nextId(),title:title,mainPane:mainPane,tabPane:tabPane,active:false};
        var element = tabPane.getElement();
        element.setAttribute("title", title);
        element.innerHTML = title;
        element.tabIndex = 0;
        element.onclick = function(){
            toggleTab(tab);
        }
        element.onkeydown = function(event){
            event = event ? event : window.event;
            if(event.keyCode == 13){
                toggleTab(tab);
            }
        }
        var offset = 0;
        if(index == tabs.length){
            offset = pane.getChildren().length-index;
        }else{
            for(var j = 0; j < index; j++){
                if(tabs[j].active){
                    offset++
                }
            }
        }
        tabs.splice(index, 0, tab);
        pane.insertChild(tabPane, index+offset);

        if(eventChannel){
            eventChannel.fireEvent(tl.TabEvent.INSERTED, index, tab, _this);
        }

        var i,bookmarkedActive;
        if(arguments.length < 5){ // call did not explicitly say whether the tab should be active

            //Check bookmarks
            if(attrs.bookmarkable && (bookmarkedActive = tl.Bookmarking.loadValues(_this.id)).length){
                activate = false;
                for(i = bookmarkedActive.length-1; i >= 0; i--){
                    if(tab.id == bookmarkedActive[i]){
                        activate = true;
                        break;
                    }
                }
            }else{ // otherwise if nothing else is active...
                activate = true;
                for(i = tabs.length-1; i >= 0; i--){
                    if(tabs[i].active){
                        activate = false;
                        break;
                    }
                }
            }
        }
        if(activate){
            toggleTab(tab);
        }
        tabPane = mainPane = element = null; // clear up potential memory leaks
        return _this;
    }

    /**
     * @function ?
     * Remove a tab from this accordion pane. If the pane is in single mode and the
     * tab is active and there are other tabs, the first tab is activated.
     * @param index the index of the tab to remove
     * @return this
     */
    this.removeTab = function(index){
        var tab = tabs[index];
        var children = pane.getChildren();
        for(var i = children.length-1; i >= 0; i--){
            if((children[i] == tab.mainPane) || (children[i] == tab.tabPane)){
                pane.removeChild(i);
            }
        }
        tabs.splice(index, 1);
        if(eventChannel){
            eventChannel.fireEvent(tl.TabEvent.DEACTIVATED, index, tab, _this);
        }
        if(tab.active && attrs.singleMode && tabs.length){
            _this.toggleTab(0);
        }
        return _this;
    }

    /**
     *@function ?
     * Get the tab at the index given
     * @param index the index for the tab
     * @return the tab at the index given. Changing attributes of this may result in undefined behaviour
     */
    this.getTab = function(index){
        return tabs[index];
    }

    /**
     * @function ?
     * Get all tabs. Changing elements of the returned array may result in undefined behaviour.
     * @return all tabs
     */
    this.getTabs = function(){
        return tabs;
    }
       
    /**
     * @function ?
     * Get the index of the tab given
     * @return the index of the tab given, or -1 if the tab was not found
     */
    this.indexOfTab = function(tab){
        var t;
        for(t = tabs.length-1; t >= 0; t--){
            if(tabs[t] == tab){
                break;
            }
        }
        return t;
    }
       
    /**
     * @function ?
     * Get an array containing all currently active tabs
     * @return an array containing all active tabs
     */
    this.getActiveTabs = function(){
        var ret = [];
        for(var i = 0; i < tabs.length; i++){
            if(tabs[i].active){
                ret.push(tabs[i]);
            }
        }
        return ret;
    }
    
    /**
     * @function ?
     * Toggle the the tab at the index given.
     * @param index the index for the tab
     * @return this
     */
    this.toggleTab = function(index){
        toggleTab(tabs[index]);
        return _this;
    }

    /** @function ? Dispose of this accordion pane */
    this.dispose = function(){
        pane.dispose();
        attrs = pane = _this = tabs = null;
    }

    /**
     * @function ?
     * Get the outer rect defining the total space this pane may occupy - including borders, padding and margin
     * @return array of 4 numbers representing the outer rect defining the total space this pane may occupy [x1,y1,x2,y2]
     */
    this.getOuterRect = pane.getOuterRect;
    /**
     * @function ?
     * Get the inner rect defining the actual space available to components within this pane
     * @return array of 4 numbers representing the actual space available to components within this pane [x1,y1,x2,y2]
     */
    this.getInnerRect = pane.getInnerRect;
    /**
     * @function ?
     * Get the DOM element for this pane - can be used to manually insert elements into this pane - it is not a good idea to mix
     * this with adding child panes
     * @return the DOM element backing this pane
     */
    this.getElement = pane.getElement;
    /**
     * @function ? Get the event channel for this pane
     * @return the event channel for this pane
     */
    this.getEventChannel = function(){
        eventChannel = pane.getEventChannel();
        return eventChannel;
    }
    /**
     * @function ? Update this pane to use the outerRect given, effectively resizing it
     * @param _outerRect the new outerRect for this pane
     * @param {optional} noAnim - flag that when true, no animation should be involved. (it should be instant)
     * @param {optional} forceUpdate - flag that when true, updates to child panes should occur even if there is no change to the size of the outerRect
     */
    this.update = pane.update;
    /** @function ? Callback function invoked when this pane was inserted to a parent pane */
    this.inserted = pane.inserted;
    /** @function ? Callback function invoked when this pane was removed from its parent pane */
    this.removed = pane.removed;

    /** Toggle the visibility of the tab given */
    function toggleTab(tab){
        if(attrs.singleMode){
            for(var i = tabs.length-1; i >= 0; i--){
                if((tabs[i] != tab) && (tabs[i].active)){
                    deactivateTab(tabs[i], true);
                }
            }
        }
        if(tab.active){
            if(!attrs.singleMode){
                deactivateTab(tab); // single mode always has one active
            }
        }else{
            activateTab(tab);
        }
        pane.update(pane.getOuterRect());
    }

    /** Deactivate the tab given */
    function deactivateTab(tab, noUpdate) {
        tab.active = false;
        tab.tabPane.getElement().className = attrs.tabAttrs.className;
        var children = pane.getChildren();
        for(var i = children.length-1; i >= 0; i--){
            if(children[i] == tab.mainPane){
                pane.removeChild(i, noUpdate);
                break;
            }
        }
        if(eventChannel){
            eventChannel.fireEvent(tl.TabEvent.DEACTIVATED, _this.indexOfTab(tab), tab, _this);
        }
        if(attrs.bookmarkable){
            var activeTabs = tl.Bookmarking.loadValues(_this.id);
            for(var j = activeTabs.length; j >= 0; j--){
                if(activeTabs[j] == tab.id){
                    activeTabs.splice(j,1);
                }
            }
            tl.Bookmarking.storeValues(_this.id, activeTabs);
        }
    }

    /** Activate the tab given */
    function activateTab(tab) {
        tab.active = true;
        tab.tabPane.getElement().className = attrs.tabAttrs.className+" "+attrs.tabAttrs.activeClass;
        var children = pane.getChildren();
        for(var i = children.length-1; i >= 0; i--){
            if(children[i] == tab.tabPane){
                pane.insertChild(tab.mainPane, i+1);
                break;
            }
        }
        if(eventChannel){
            eventChannel.fireEvent(tl.TabEvent.ACTIVATED, _this.indexOfTab(tab), tab, _this);
        }
        if(attrs.bookmarkable){
            var activeTabs = tl.Bookmarking.loadValues(_this.id);
            for(var j = activeTabs.length; j >= 0; j--){
                if(activeTabs[j] == tab.id){
                    activeTabs.splice(j,1);
                }
            }
            activeTabs.push(tab.id);
            tl.Bookmarking.storeValues(_this.id, activeTabs);
        }
    }

    // Function called internally from attrs to apply/remove aliases for a pane.
    // This function is publis so as to allow it to be overridden by wrappers/subclasses.
    pane.handleAliasing = function(aliases, prevAliases){
        var i;
        if(prevAliases){
            for(i = prevAliases.length-1; i >= 0; i--){
                tl.removeAlias(prevAliases[i], _this);
            }
        }
        for(i = aliases.length-1; i >= 0; i--){
            tl.addAlias(aliases[i], _this);
        }
    }

    pane.attrs(attrs);
    if(toAdopt){
        tl.domElementToTabs(this, toAdopt);
    }
}
/** @class {static} tl.PopupPaneAttrs
 * This object contains the outline of default / accepted attributes for TPopupPane instances.
 */
tl.PopupPaneAttrs = {
    /** Aliases for the popup */
    aliases : null,
    /** @property ? DOM Node to which this pane will be appended - defaults to document.body at runtime*/
    parentNode : null,
    /** @property ? flag indicating whether an outside click will close the popup, (Default true) */
    outsideClickCloses : true,
    /** @property ? Time for which this pane will be displayed. The default of 0 implies the frame will not be automatically hidden */
    displayTimeout : 0,
    /** @property ? width of the popup - Default width is 300 */
    width : 300,
    /** @property ? Units for width - PIXEL or PERCENT - NOTE that auto is not supported (Default PIXEL) */
    widthUnits : tl.Sizing.PIXEL,
    /** @property ? height of the popup - Default width is 100 */
    height : 100,
    /** @property ? Units for height - PIXEL or PERCENT - NOTE that auto is not supported (Default PIXEL)  */
    heightUnits : tl.Sizing.PIXEL,
    /** @property ? Position on the x axis - default 0 */
    x : 0,
    /** @property ? Units for position on X axis - default is AUTO */
    xUnits : tl.Sizing.AUTO,
    /** @property ? Flag indicating that x axis is inverted (x refers to "right" instead of "left") (Default false) */
    xInvert : false,
    /** @property ? Position on the y axis - default 0 */
    y : 0,
    /** @property ? Units for position on Y axis - default is AUTO  */
    yUnits : tl.Sizing.AUTO,
    /** @property ? Flag indicating that y axis is inverted (x refers to "right" instead of "left") (Default false) */
    yInvert : false,
    /** @property ? flag indicating whether this pane blocks access to other panes when visible (causes opaque underlay) (Default true) */
    modal : false,
    /** @property ? Flag indicating whether the pane is currently visible - set to false to hide the pane. (Default true) */
    visible : true,
    /** @property ? Dom functions for inserting / removing popups */
    domFunctions : tl.dom.fading,
    /** @property ? flag indicating whether the popup will be disposed of when it is hidden */
    disposeOnHide : false,
    /** @property ? value that if defined will be used to explicitly set the z-index of the pane */
    zIndex : "",
    /** @property ? value that if defined will be used to explicitly set the z-index of the underlay */
    underlayZIndex : ""
}

/** @class tl.PopupPane
 * Popup Panes can be used to display panes within a popup that floats above
 * the rest of the content on the page
 */
/** @constructor ?
 * @param {optional} attrs - attributes object for this pane
 * @param {optional} contentPane - content pane to display in the popup - defaults to new tl.Pane instance
 * @see tl.PopupPaneAttrs
 */
tl.PopupPane = function(attrs, contentPane){
    var _this = this;
    if(!contentPane){
        contentPane = new tl.Pane();
    }
    var outerRect;
    var modal = false;
    var timeout;
    var handler;
    var visible = false;

    /**
     * @function ?
     * Get / Set the attributes for this layout pane - the returned object graph
     * should be regarded as immutable - edits to it could result in undefined behaviour.
     * If an attrs object was specified as a parameter its attributes will override
     * any existing ones, and this will be returned.
     * @param {optional} _attrs new attributes to be applied to this pane
     * @return _this if _attrs was defined, the current attrs otherwise.
     * @see tl.PopupPaneAttrs
     */
    this.attrs = function(_attrs){
        if(_attrs){
            if(_attrs.aliases){
                _this.handleAliasing(_attrs.aliases, attrs.aliases);
            }
            attrs = tl.copyAttributes(attrs, _attrs);
            var newModal = attrs.visible && attrs.modal;
            if(modal != newModal){
                modal = newModal;
                if(modal){
                    tl.PopupPaneUnderlay.insert();
                    tl.PopupPaneUnderlay.zIndex(attrs.underlayZIndex);
                }else{
                    tl.PopupPaneUnderlay.remove();
                }
            }
            if(attrs.visible){
                contentPane.getElement().style.zIndex = attrs.zIndex;
            }
            if(visible != attrs.visible){
                visible = attrs.visible;
                if(visible){
                    attrs.domFunctions.insert(attrs.parentNode, contentPane.getElement());
                    contentPane.inserted();
                    _this.update([0, 0, attrs.parentNode.clientWidth, attrs.parentNode.clientHeight], true);
                }else{
                    attrs.domFunctions.remove(contentPane.getElement());
                    contentPane.removed();
                    if(_attrs.disposeOnHide){
                        _this.dispose();
                    }
                }
            }
            cleanUp();
            if(attrs.displayTimeout){
                timeout = window.setTimeout(function(){
                    timeout = null;
                    _this.attrs({visible:false});
                }, attrs.displayTimeout);
            }
            if(attrs.outsideClickCloses){
                handler = checkClick;
                window.setTimeout(function(){ // Add these handlers later or they will run for the current event and maybe immediatly close the popup
                    if(window.attachEvent){
                        document.body.attachEvent("onclick", handler);
                    }else{
                        window.addEventListener("click", handler, false);
                    }
                },1);
            }
            return this;
        }else{
            return attrs;
        }
    }

    /**
     * @function ?
     * Get the outer rect defining the total space this pane may occupy - including borders, padding and margin
     * @return array of 4 numbers representing the outer rect defining the total space this pane may occupy [x1,y1,x2,y2]
     */
    this.getOuterRect = function(){
        return tl.toRect(outerRect);
    }

    /**
     * @function ?
     * Get the inner rect defining the actual space available to components within this pane
     * @return array of 4 numbers representing the actual space available to components within this pane [x1,y1,x2,y2]
     */
    this.getInnerRect = function(){
        var width, height, left, top;
        if(attrs.widthUnits == "px"){
            width = attrs.width;
        }else{
            width = Math.round(outerRect[2] * attrs.width / 100);
        }
        if(attrs.heightUnits == "px"){
            height = attrs.height;
        }else{
            height = Math.round(outerRect[3] * attrs.height / 100);
        }
        if(attrs.xUnits == "px"){
            left = attrs.x;
        }else if(attrs.xUnits == tl.Sizing.PERCENT){
            left = Math.round((outerRect[2] - outerRect[0]) * attrs.x / 100);
        }else{
            left = Math.round((outerRect[2] - (outerRect[0] + width)) / 2);
        }
        if(attrs.xInvert){
            left = outerRect[2] - (outerRect[0] + left + width);
        }
        if(attrs.yUnits == "px"){
            top = attrs.y;
        }else if(attrs.yUnits == tl.Sizing.PERCENT){
            top = Math.round((outerRect[3] - outerRect[1]) * attrs.y / 100);
        }else{
            top = Math.round((outerRect[3] - (outerRect[1] + height)) / 2);
        }
        if(attrs.yInvert){
            top = outerRect[3] - (outerRect[1] + top + height);
        }
        return [left,top,left+width,top+height];
    }

    /**
     * @function ? Update this pane to use the outerRect given, effectively resizing it
     * @param _outerRect the new outerRect for this pane
     * @param {optional} noAnim - flag that when true, no animation should be involved. (it should be instant)
     * @param {optional} forceUpdate - flag that when true, updates to child panes should occur even if there is no change to the size of the outerRect
     */
    this.update = function(_outerRect, noAnim){
        outerRect = tl.toRect(_outerRect);
        var rect = _this.getInnerRect();
        var style = contentPane.getElement().style;
        style.position = "absolute";
        style.left = rect[0]+"px";
        style.top = rect[1]+"px";
        contentPane.update(rect, noAnim);
    }

    /** @function ? Dispose of this pane and all child panes */
    this.dispose = function(){
        if(attrs.aliases && attrs.aliases.length){
            _this.handleAliasing([], attrs.aliases);
        }
        contentPane.dispose();
        if(modal){
            tl.PopupPaneUnderlay.remove();
        }
        cleanUp();
        _this = contentPane = null;
    }

    /** @function ? Toggle the visibility this pane.
     * @return this
     */
    this.toggle = function(){
        _this.attrs({visible : !attrs.visible});
        return _this;
    }

    /** @function ? Get the content pane
     * @return the contentPane
     */
    this.getContentPane = function(){
        return contentPane;
    }
    
    /** @function ? Get the event channel for this popup pane
     * @return the event channel for this popup pane
     */
    this.getEventChannel = contentPane.getEventChannel;
    
    /** @function ? Get the DOM element for this popup pane
     * @return the DOM element for this popup pane
     */
    this.getElement = contentPane.getElement;

    /**
     * @function {protected} ? Function called internally from attrs to apply/remove aliases for a pane.
     * This function is publis so as to allow it to be overridden by wrappers/subclasses.
     */
    this.handleAliasing = function(aliases, prevAliases){
        var i;
        if(prevAliases){
            for(i = prevAliases.length-1; i >= 0; i--){
                tl.removeAlias(prevAliases[i], _this);
            }
        }
        for(i = aliases.length-1; i >= 0; i--){
            tl.addAlias(aliases[i], _this);
        }
    }

    /** Clean up this pane */
    function cleanUp(){
        if(timeout){
            window.clearTimeout(timeout);
            timeout = null;
        }
        if(handler){
            if(window.detachEvent){
                document.body.detachEvent("onclick", handler);
            }else{
                window.removeEventListener("click", handler, false);
            }
            handler = null;
        }
    }

    /**
     * Examine a click event - if it originated outside the popup, close the popup
     */
    function checkClick(event) {
        if(!event) event = window.event;
        var source = event.target ? event.target : event.srcElement;
        while(source){
            if(source == contentPane.getElement()){
                return;
            }
            source = source.parentNode;
        }
        _this.attrs({visible:false});
    }

    attrs = tl.copyAttributes(tl.PopupPaneAttrs, attrs);
    if(!attrs.parentNode){
        attrs.parentNode = document.body;
    }
    this.attrs(attrs);
}

/**
 * @class tl.PopupPaneUnderlay
 * Underlay object for popup panes
 */
tl.PopupPaneUnderlay = new function() {
    var count = 0;
    var underlay = document.createElement("div");
    underlay.className = "tl_underlay";
    underlay.style.position = "absolute";
    underlay.style.top = underlay.style.left = "0px";
    underlay.style.width = underlay.style.height = "100%";

    /** @function ? called when a popup was inserted / shown */
    this.insert = function(){
        count++;
        if(count == 1){
            document.body.appendChild(underlay);
        }
    }

    /** @function ? called when a popup was removed / hidden */
    this.remove = function(){
        count--;
        if(count == 0){
            underlay.parentNode.removeChild(underlay);
        }
    }

    /** @function ? set/get the z index of the underlay */
    this.zIndex = function(zIndex){
        if(arguments.length){
            underlay.style.zIndex = zIndex;
            return this;
        }else{
            return underlay.style.zIndex;
        }
    }
}


/** @class {static} tl.WindowAttrs
 * This object contains the outline of default / accepted attributes for tl.Window instances.
 * A tl.Window is a popup that creates an in page window using javascript and html.
 * @extends tl.PopupPaneAttrs
 */
tl.WindowAttrs = tl.copyAttributes(tl.PopupPaneAttrs, {
    /** @property ? String title for window panes to be displayed in the toolbar (Default : "Untitled") */
    title : "Untitled",
    /** @property ? flag indicating whether if the window may be resized, (Default : true) */
    resizable : true,
    /** @property ? flag indicating whether if the window is movable, (Default : true) */
    movable : true,
    /** @property ? flag indicating whether if the window is closable, (Default : true) */
    closable : true,
    /** @property ? The offset of the shadow for the window in the x direction (0 for x and y implies no shadow - default 5) */
    xShadow : 5,
    /** @property ? The offset of the shadow for the window in the y direction (0 for x and y implies no shadow - default 5) */
    yShadow : 5,
    /**
     * @property ?
     * Attributes for the window pane. Default border [1,1,2,2], className "tl_window"
     * @see tl.PaneAttrs
     */
    windowAttrs : {
        /** Border size for the window */
        border : [1,1,2,2],
        /** Class name for window*/
        className:"tl_window"
    },
    /** @property ? Min window width (Default 60) */
    minWidth : 60,
    /** @property ? Min window height (Default 50) */
    minHeight : 50

});

/** @class tl.Window
 * A tl.Window is an extension of tl.PopupPane that creates a DHTML window.
 * @extends tl.PopupPane
 */
/** @constructor ?
 * @param {optional} attrs - attributes object for this pane
 * @param {optional} contentPane - content pane to display in the popup - defaults to new tl.Pane instance
 * @see tl.WindowAttrs
 */
tl.Window = function(attrs, contentPane){
    var _this = this;
    var popup = new tl.PopupPane(attrs, new tl.Pane({className:"tl_layout"}));
    if(!contentPane){
        contentPane = new tl.Pane();
    }
    /** Parent pane of content pane*/
    var contentParent;
    /** Drag origin point*/
    var dragOrigin;
    /** Origin point for drag operations*/
    var rectOrigin;
    /** Array of 4 boolean flag indicating what a drag operation is altering - [left,top,width,height]*/
    var dragFlags;
    
    /**
     * @function ?
     * Get / Set the attributes for this layout pane - the returned object graph
     * should be regarded as immutable - edits to it could result in undefined behaviour.
     * If an attrs object was specified as a parameter its attributes will override
     * any existing ones, and this will be returned.
     * @param {optional} _attrs new attributes to be applied to this pane
     * @return _this if _attrs was defined, the current attrs otherwise.
     * @see tl.WindowAttrs
     */
    this.attrs = function(_attrs){
        if(_attrs){
            popup.attrs(_attrs);
            _attrs = popup.attrs();
            processAttrs(_attrs);
            return this;
        }else{
            return attrs;
        }
    }
    /** @function ? Get the content pane for the window
     * @return the contentPane
     */
    this.getContentPane = function(){
        return contentPane;
    }

    /** @function ? Get the event channel for this popup pane
     * @return the event channel for this popup pane
     */
    this.getEventChannel = popup.getEventChannel;
    
    /** @function ? Get the DOM element for this popup pane
     * @return the DOM element for this popup pane
     */
    this.getElement = popup.getElement;

    /**
     * @function ?
     * Get the outer rect defining the total space this pane may occupy - including borders, padding and margin
     * @return array of 4 numbers representing the outer rect defining the total space this pane may occupy [x1,y1,x2,y2]
     */
    this.getOuterRect = popup.getOuterRect;

    /**
     * @function ?
     * Get the inner rect defining the actual space available to components within this pane
     * @return array of 4 numbers representing the actual space available to components within this pane [x1,y1,x2,y2]
     */
    this.getInnerRect = popup.getInnerRect;

    /**
     * @function ? Update this pane to use the outerRect given, effectively resizing it
     * @param _outerRect the new outerRect for this pane
     * @param {optional} noAnim - flag that when true, no animation should be involved. (it should be instant)
     * @param {optional} forceUpdate - flag that when true, updates to child panes should occur even if there is no change to the size of the outerRect
     */
    this.update = popup.update;

    /** @function ? Dispose of this pane and all child panes */
    var popupDispose = popup.dispose;
    this.dispose = popup.dispose = function(){
        mouseUp();
        popupDispose();
        if(window.attachEvent){
            window.detachEvent("onunload", _this.dispose);
        }else{
            window.removeEventListener("unload", _this.dispose, true);
        }
        popup = contentPane = contentParent = _this = popupDispose = attrs = null;
    }
    
    /**
     * @function ? Toggle the visibility this pane.
     * @return this
     */
    this.toggle = function(){
        _this.attrs({visible : !popup.attrs().visible});
        return _this;
    }

    /**
     * @function ?
     * Move this window in front of any other DHTML windows with the same parentNode
     * @return this
     */
    this.moveToFront = function() {
        var element = popup.getContentPane().getElement();
        var parent = element.parentNode;
        var children = parent.childNodes;
        if(children[children.length-1] != element){
            parent.removeChild(element);
            parent.appendChild(element); // Make it last element - move to top
        }
        return _this;
    }

    /** Process changed attributes */
    function processAttrs(attrs){
        var childPanes,i,childPane;
        if(contentParent){
            // take the main pain out of the tree so it will not be disposed of
            childPanes = contentParent.getChildren();
            for(i = childPanes.length-1; i >= 0; i--){
                if(childPanes[i] == contentPane){
                    contentParent.removeChild(i);
                    break;
                }
            }
            //Dispose of all panes
            contentParent = popup.getContentPane();
            childPanes = contentParent.getChildren();
            for(i = childPanes.length-1; i >= 0; i--){
                childPane = childPanes[i];
                contentParent.removeChild(i);
                childPane.dispose();
            }
        }

        contentParent = createOutline(attrs, popup.getContentPane()); //Append appropriate shadows to pane
        addTitleBar(attrs, contentParent); // add the title bar to the pane
        if(attrs.resizable){
            contentParent = addResizeBars(contentParent);
        }
        contentParent.insertChild(contentPane); //add the main pane to the pane
    }

    /** Add a title bar */
    function addTitleBar(attrs,currentPane) {
        var titleBar = new tl.Pane({className:"tl_win_heading",size:30,units:"px",horizontal:true});
        currentPane.insertChild(titleBar);
        var titlePane = new tl.Pane({className:""});
        titleBar.html(attrs.title);
        titleBar.insertChild(titlePane);
        titlePane.getElement().title = attrs.title;
        //titlePane.getElement().innerHTML = titlePane.getElement().title = attrs.title;
        if(attrs.closable){
            //Add a close button
            var closeBtn = new tl.Pane({className:"tl_closeBtn",size:30,units:"px",margin:5});
            closeBtn.getElement().title = "Close";
            closeBtn.getElement().onclick = _this.toggle;
            closeBtn.getElement().tabIndex = 0;
            titleBar.insertChild(closeBtn);
        }
        if(attrs.movable){
            titleBar.getElement().onmousedown = titleMouseDown; //Add a drag handler to the titlePane
        }
    }

    /** Create the outline of the window, including appropriate shadows */
    function createOutline(attrs,currentPane) {
        var p1,p2,p3
        if(attrs.xShadow > 0){
            if(attrs.yShadow > 0){
                //Set up outer layout
                p1 = new tl.Pane({className:"tl_layout",horizontal:true});
                currentPane.insertChild(p1);
                currentPane = p1;

                //Create and attach left (main) layout panel
                p1 = new tl.Pane({className:"tl_layout"});
                currentPane.insertChild(p1);

                //Create and attach right hand shadow
                p2 = new tl.Pane({className:"tl_layout",size:attrs.xShadow,units:"px"});
                currentPane.insertChild(p2);
                p3 = new tl.Pane({className:"tl_layout",size:attrs.yShadow,units:"px"});
                p2.insertChild(p3);
                p3 = new tl.Pane({className:"tl_shadow"});
                p2.insertChild(p3);

                currentPane = p1; // move in a level

                //Create and attach top (main) panel
                p1 = new tl.Pane(attrs.windowAttrs);
                currentPane.insertChild(p1);

                //Create and attach bottom shadow
                p2 = new tl.Pane({className:"tl_layout",horizontal:true,size:attrs.yShadow,units:"px"});
                currentPane.insertChild(p2);
                p3 = new tl.Pane({className:"tl_layout",size:attrs.xShadow,units:"px"});
                p2.insertChild(p3);
                p3 = new tl.Pane({className:"tl_shadow"});
                p2.insertChild(p3);

                currentPane = p1; // move in a level
            }else{
                currentPane.attrs({horizontal:true});
                p1 = new tl.Pane(attrs.windowAttrs);
                currentPane.insertChild(p1);
                p2 = new tl.Pane({className:"tl_shadow",size:attrs.xShadow,units:"px"});
                currentPane.insertChild(p2);
                currentPane = p1;
            }
        }else if(attrs.yShadow > 0){
            p1 = new tl.Pane(attrs.windowAttrs)
            currentPane.insertChild(p1);
            p2 = new tl.Pane({className:"tl_shadow",size:attrs.yShadow,units:"px"});
            currentPane.insertChild(p2);
            currentPane = p1;
        }else{
            p1 = new tl.Pane(attrs.windowAttrs);
            currentPane.insertChild(p1);
            currentPane = p1;
        }
        return currentPane;
    }

    /** Add resize bars to the window */
    function addResizeBars(currentPane) {
       var inner = new tl.Pane({className:"tl_layout",horizontal:true});
       currentPane.insertChild(inner);
       var bottom = new tl.Pane({size:10,units:"px",className:"tl_layout",horizontal:true});
       var horizontalResizer = new tl.Pane({className:"tl_divisor_horizontal"});
       bottom.insertChild(horizontalResizer);
       var cornerResizer = new tl.Pane({size:10,units:"px",className:"tl_resizer"});
       bottom.insertChild(cornerResizer);
       currentPane.insertChild(bottom);
       var ret = new tl.Pane({className:"tl_layout"});
       inner.insertChild(ret);
       var verticalResizer = new tl.Pane({size:10,units:"px",className:"tl_divisor_vertical"});
       inner.insertChild(verticalResizer);
       horizontalResizer.getElement().onmousedown = horizontalMouseDown;
       verticalResizer.getElement().onmousedown = verticalMouseDown;
       cornerResizer.getElement().onmousedown = cornerMouseDown;
       return ret;
    }

    /** Callback function for mousedown on title bar */
    function titleMouseDown(event) {
        mouseDown(event, true, true, false, false);
    }

    /** Callback function for mousedown on horizontal resize bar */
    function horizontalMouseDown(event) {
        contentPane.getElement().style.display = "none";
        mouseDown(event, false, false, false, true);
    }

    /** Callback function for mousedown on vertical resize bar */
    function verticalMouseDown(event) {
        contentPane.getElement().style.display = "none";
        mouseDown(event, false, false, true, false);
    }

    /** Callback function for mousedown on corner resize element */
    function cornerMouseDown(event) {
        contentPane.getElement().style.display = "none";
        mouseDown(event, false, false, true, true);
    }

    /** Global mousedown function for moving / resizing */
    function mouseDown(event, left, top, width, height) {
        event = event ? event : window.event;
        dragOrigin = [event.clientX, event.clientY];
        rectOrigin = popup.getInnerRect();
        dragFlags = [left,top,width,height];

        //Attach drag listener to window
        if(window.attachEvent){
            document.body.attachEvent("onmousemove", mouseMove);
            document.body.attachEvent("onmouseup", mouseUp);
        }else{
            window.addEventListener("mousemove", mouseMove, true);
            window.addEventListener("mouseup", mouseUp, true);
        }
    }
    
    /** Global mousemove function for moving / resizing */
    function mouseMove(event) {
        event = event ? event : window.event;
        var delta = [event.clientX - dragOrigin[0], event.clientY - dragOrigin[1]];
        var popupAttrs = {
            x : rectOrigin[0],
            xUnits : "px",
            xInvert : false,
            y : rectOrigin[1],
            yUnits : "px",
            yInvert : false
        };
        if(dragFlags[0]){ // alter left
            popupAttrs.x += delta[0];
        }
        if(dragFlags[1]){ // alter top
            popupAttrs.y += delta[1];
        }
        var attrs = popup.attrs();
        if(dragFlags[2]){ // alter width
            popupAttrs.width = delta[0]+rectOrigin[2]-rectOrigin[0];
            if(popupAttrs.width < attrs.minWidth){
                popupAttrs.width = attrs.minWidth;
            }
            popupAttrs.widthUnits = "px";
        }
        if(dragFlags[3]){ // alter height
            popupAttrs.height = delta[1]+rectOrigin[3]-rectOrigin[1];
            if(popupAttrs.height < attrs.minHeight){
                popupAttrs.height = attrs.minHeight;
            }
            popupAttrs.heightUnits = "px";
        }
        for(var i in popupAttrs){
            attrs[i] = popupAttrs[i];
        }
        popup.update(popup.getOuterRect(), true);
    }

    /** Global mouseup function for moving / resizing */
    function mouseUp() {
        contentPane.getElement().style.display = "block";
        if(window.attachEvent){
            document.body.detachEvent("onmousemove", mouseMove);
            document.body.detachEvent("onmouseup", mouseUp);
        }else{
            window.removeEventListener("mousemove", mouseMove, true);
            window.removeEventListener("mouseup", mouseUp, true);
        }
    }

    var popupElement = popup.getContentPane().getElement();
    if(window.attachEvent){
        popupElement.attachEvent("onmousedown", this.moveToFront);
        window.attachEvent("onunload", this.dispose);
    }else{
        popupElement.addEventListener("mousedown", this.moveToFront, true);
        window.addEventListener("unload", this.dispose, true);
    }
    popupElement = null;

    attrs = tl.copyAttributes(tl.WindowAttrs, attrs);
    processAttrs(attrs);
}/** @class tl.LazyLoadingPane
 * Pane that loads content from a url, only doing so the first time it is updated.
 * @extends tl.Pane
 */
/** @constructor ?
 * @param {optional} attrs - attributes object for this pane
 * @see tl.PaneAttrs
 */
tl.LazyLoadingPane = function(attrs){
    var _this = new tl.Pane(attrs);
    var loaded; // flag indicating whether or not a load has occured.

    var superUpdate = _this.update; // preserve reference to super function

    /**
     * @function ? Update this pane to use the outerRect given, effectively resizing it. Also kicks off the loading
     * of content for the first time the pane is loaded.
     * @param _outerRect the new outerRect for this pane
     * @param {optional} noAnim - flag that when true, no animation should be involved. (it should be instant)
     * @param {optional} forceUpdate - flag that when true, updates to child panes should occur even if there is no change to the size of the outerRect
     */
    _this.update = function(_outerRect, noAnim, forceUpdate){
        if(_outerRect){
            superUpdate(_outerRect, noAnim);
            if(!loaded){
                loaded = true;
                var attrs = _this.attrs();
                _this.load(attrs.url, attrs.method, attrs.sendData, attrs.httpResponseHandler); // if this is the first update, load the data for the pane
            }
        }
    }

    return _this;
}

/** @class {static} tl.MenuAttrs
 * This object contains the outline of default / accepted attributes for tl.Menu instances
 * @extends tl.PaneAttrs
 */
tl.MenuAttrs = {
    /** @property ? 1 pixel padding */
    padding : 1,
    /** @property ? Menu is Horizontal or vertical */
    horizontal : false,
    /** @property ? The css class name for this pane (Default "tl_menu") */
    className : "tl_menu",
    /** @property ? The height of menu items */
    itemHeight : 30
}

/**
 * @class tl.Menu
 * Implementation of menu to which menu items / sub menus may be added
 * @extends tl.Pane
 */
/** @constructor ?
 * @param {optional} attrs - attributes object for this pane
 * @param {optional} contextManager - manager for menu item navigation (generated if missing)
 * @see tl.MenuAttrs
 */
tl.Menu = function(attrs, contextManager) {
    var pane = new tl.Pane(tl.copyAttributes(tl.MenuAttrs, attrs));
    var interest = 0;
    if(!contextManager){
        contextManager = new tl.MenuContextManager(pane);
    }

    /**
     * @function ? Get the menu context manager for this menu
     * @return the tl.MenuContextManager instance for this menu
     */
    pane.getMenuContextManager = function(){
        return contextManager;
    }

    /**
     * @function ? Add a menu item to this menu
     * @param menuItemAttrs attributes for the menu item - see tl.MenuItemAttrs
     * @param {optional} index index of the menu item (in cases where insertChild has been used directly, this is not supported)
     * @return this
     */
    pane.addItem = function(menuItemAttrs, index){
        var attrs = pane.attrs();
        if((!attrs.horizontal) && attrs.itemHeight){
            if(!menuItemAttrs){
                menuItemAttrs = {};
            }
            menuItemAttrs.size = attrs.itemHeight;
            menuItemAttrs.units = tl.Sizing.PIXEL;
        }
        var item = new tl.MenuItem(menuItemAttrs, pane.attrs().horizontal, contextManager);
        contextManager.add(pane, item, index);
        pane.insertChild(item, index);
        return pane;
    }


    /**
     * @function ? Create a sub menu for this menu and return it.
     * @param menuItemAttrs attributes for the menu item - see tl.MenuItemAttrs
     * @param {optional} popupAttrs attributes for the popup
     * @param {optional} subMenuAttrs attributes for the sub menu
     * @param {optional} index index of the menu item (in cases where insertChild has been used directly, this is not supported)
     * @return the created sub menu (An instance of tl.Menu)
     */
    pane.createSubMenu = function(menuItemAttrs, popupAttrs, subMenuAttrs, index){
        var attrs = pane.attrs();
        if((!attrs.horizontal) && attrs.itemHeight){
            if(!menuItemAttrs){
                menuItemAttrs = {};
            }
            menuItemAttrs.size = attrs.itemHeight;
            menuItemAttrs.units = tl.Sizing.PIXEL;
        }
        var item = new tl.MenuItem(menuItemAttrs, pane.attrs().horizontal, contextManager);
        contextManager.add(pane, item, index);
        pane.insertChild(item, index);
        var menu = new tl.Menu(subMenuAttrs, contextManager);
        var popup = new tl.MenuPopup(popupAttrs, contextManager, menu);
        contextManager.add(item, popup);
        contextManager.add(popup, menu);
        return menu;
    }

    /**
     * @function ? Remove the child pane at the index given
     * @param index the index of the child to remove
     * @return this
     */
    var paneRemoveChild = pane.removeChild;
    pane.removeChild = function(index){
        var removedPane = pane.getChild(index);
        pane.removeChild(index);
        contextManager.remove(removedPane);
        return pane;
    }

    /**
     * @function ? Increase Interest in this menu. Passes increase to parent and insures
     * that the menu stays open while there is interest in it (mouseover/focus)
     */
    pane.incInterest = function(){
        var parent = contextManager.getParent(pane);
        if(parent){
            parent.incInterest();
        }
    }

    /**
     * @function ? Decrease Interest in this menu. Passes decrease to parent and insures
     * that the menu closes when there is no longer interest in it
     */
    pane.decInterest = function(){
        var parent = contextManager.getParent(pane);
        if(parent){
            parent.decInterest();
        }
    }

    /**
     * @function ? Focus on this menu - Add focus to the first element if existing.
     */
    pane.focus = function(){
        var child = contextManager.getFirstChild(pane);
        if(child){
            child.focus();
        }
    }

    /**
     * @function ? Dispose of this menu - remove from context manager and prevent memory
     * leaks
     */
    var paneDispose = pane.dispose;
    pane.dispose = function(){
        // remove any interest in parent generated from this item
        var parent = contextManager.getParent(pane);
        while(interest){
            interest--;
            parent.decInterest();
        }
        contextManager.remove(pane); // remove from context manager
        paneDispose(); // dispose of menu item
        pane = attrs = contextManager = paneDispose = null;
    }


    /**
     * @function ? Adopt the contents of the node given as additional menu elements. Adopted
     * elements are typically a list containing links / other lists. Links get converted into
     * menu items, and lists get converted to submenus
     * @return this
     */
    pane.adopt = function(node) {
        if(typeof node == "string"){
            node = document.getElementById(node);
        }
        var elements = getChildElements(node);
        for(var i = 0; i < elements.length; i++){
            var element = elements[i];
            var innerElements = getChildElements(element);
            if((innerElements.length == 1) && (innerElements[0].nodeName.toLowerCase() == "a")){
                var link = innerElements[0];
                pane.addItem({text:link.innerHTML, href:link.href, onclick:link.onclick, target:link.target});
            }else if(innerElements.length == 2){
                var type = innerElements[1].nodeName.toLowerCase();
                if((type == "ul") || (type == "ol")){
                    pane.createSubMenu({text:innerElements[0].innerHTML})
                        .adopt(innerElements[1]);
                }else{
                    throw "InvalidDOMStructure";
                }
            }else{
                throw "InvalidDOMStructure";
            }
        }
        if(node.parentNode){
            node.parentNode.removeChild(node);
        }
        return pane;
    }

    //Get the childnodes of node that are elements
    function getChildElements(node) {
        var ret = [];
        var children = node.childNodes;
        for(var i = 0; i < children.length; i++){
            var child = children[i];
            if(child.nodeType == 1){
                ret.push(child);
            }
        }
        return ret;
    }

    return pane;
}


/**
 * @class {static} tl.MenuContextManager
 * Context manager for menus - coordinates parent/child/sibling releationships between
 * menu elements. It does not have responsibility for displaying elements
 */
/** @constructor ?
 * @param root - the root pane
 */
tl.MenuContextManager = function(root) {

    /**
     * @function ? Add a pane
     * @param parent the parent pane for the pane to add
     * @param pane the pane to add
     * @param {optional} index the index of the pane
     * @throws error if parent could not be found
     */
    this.add = function(parent, pane, index){
        var parentNode = findNode(root, parent);
        if(!parentNode.children){
            parentNode.children = [];
        }
        if(index == undefined){
            index = parentNode.children.length;
        }
        parentNode.children.splice(index, 0, {pane:pane});
    }

    /**
     * @function ? Remove a pane. Does nothing if the pane was not present
     * @param pane the pane to remove
     */
    this.remove = function(pane){
        var parentNode = findParent(pane);
        if(parentNode){
            var nodes = parentNode.children;
            for(var i = nodes.length-1; i >= 0; i--){
                if(nodes[i].pane == pane){
                    nodes.splice(i, 1);
                }
            }
        }
    }

    /**
     * @function ? Get the next pane after that given
     * @param pane the pane
     * @return the next pane after that given (or null if the pane was not found)
     */
    this.getNext = function(pane){
        var parent = findParent(root, pane);
        if(parent){
            var siblings = parent.children;
            if(siblings[siblings.length-1].pane == pane){
                return parent.pane;
            }else{
                for(var i = siblings.length-2; i >= 0; i--){
                    if(siblings[i].pane == pane){
                        return siblings[i+1].pane;
                    }
                }
            }
        }
        return null;
    }

    /**
     * @function ? Get the previous pane before that given
     * @param pane the pane
     * @return the previous pane before that given (or null if the pane was not found)
     */
    this.getPrev = function(pane){
        var parent = findParent(root, pane);
        if(parent){
            var siblings = parent.children;
            if(siblings[0].pane == pane){
                return siblings[siblings.length-1].pane;
            }else{
                for(var i = siblings.length-1; i > 0; i--){
                    if(siblings[i].pane == pane){
                        return siblings[i-1].pane;
                    }
                }
            }
        }
        return null;
    }

    /**
     * @function ? Get the parent pane of that given
     * @param pane the pane
     * @return the parent pane (or null if the parent was not found)
     */
    this.getParent = function(pane){
        var node = findParent(root, pane);
        return node ? node.pane : null;
    }

    /**
     * @function ? Get the first child of the pane given
     * @param pane the pane
     * @return the first child (or null pane was not found or has no children)
     */
    this.getFirstChild = function(pane){
        var node = findNode(root, pane);
        return (node && node.children && node.children.length) ? node.children[0].pane : null;
    }

    /**
     * @function ? Get all children of the pane given
     * @param pane the pane
     * @return array of child panes (empty array if pane not found or has no children)
     */
    this.getChildren = function(pane){
        var ret = [];
        var node = findNode(root, pane);
        if(node && node.children){
            var nodes = node.children;
            for(var i = 0; i < nodes.length; i++){
                ret.push(nodes[i].pane);
            }
        }
        return ret;
    }

    //find a node containing the pane given under the node given
    function findNode(node, pane) {
        if(node.pane == pane){
            return node;
        }else{
            var nodes = node.children;
            if(nodes){
                for(var i = nodes.length-1; i >= 0; i--){
                    var ret = findNode(nodes[i], pane);
                    if(ret){
                        return ret;
                    }
                }
            }
            return null;
        }
    }
    //find the parent of the node containing the pane given under the node given
    function findParent(node, pane) {
        var nodes = node.children;
        if(nodes){
            for(var i = nodes.length-1; i >= 0; i--){
                if(nodes[i].pane == pane){
                    return node; // found parent
                }else{
                    var ret = findParent(nodes[i], pane);
                    if(ret){
                        return ret;
                    }
                }
            }
        }
        return null;
    }

    root = {pane:root};
}
/** @class {static} tl.MenuItemAttrs
 * This object contains the outline of default / accepted attributes for tl.MenuItem instances
 * @extends tl.PaneAttrs
 */
tl.MenuItemAttrs = tl.copyAttributes(tl.PaneAttrs, {
    /** @property ? class name for the pane element */
    className : "tl_menu_item",
    /** @property ? class name for link */
    linkClass : "tl_menu_link",
    /** @property ? class for when the item is active (has interest) */
    activeClass : "tl_menu_active",
    /** @property ? default href for links - javascript:; */
    href : "javascript:;",
    /** @property ? Default text - "Menu Item" */
    text : "Menu Item",
    /** @property ? default onclick function */
    onclick : null,
    /** @property ? Default padding - 5px on the left */
    padding:[5,0,0,0],
    /** @property ? Default Margin - 1px */
    margin:1,
    /** @property ? Default border - 1px */
    border:1
});

/**
 * @class tl.MenuItem
 * Implementation of menu item in a menu
 * @extends tl.Pane
 */
/** @constructor ?
 * @param {optional} attrs - attributes object for this pane
 * @param horizontal - whether the parent menu is horizontal or vertical (used for positioning sub menus)
 * @param {optional} contextManager - manager for menu item navigation (generated if missing)
 * @see tl.MenuItemAttrs
 */
tl.MenuItem = function(attrs, horizontal, contextManager) {
    var pane = new tl.Pane(tl.copyAttributes(tl.MenuItemAttrs, attrs));
    var interest = 0;
    var link = document.createElement("a");

    /**
     * @function ? Increase Interest in this menu item. Passes increase to parent and insures
     * that the menu stays open while there is interest in it (mouseover/focus). Also swaps
     * css class to attrs.activeClass
     */
    pane.incInterest = function(){
        contextManager.getParent(pane).incInterest(); // increase interest in parent nodes too
        interest++;
        if(interest == 1){
            pane.getElement().className = attrs.activeClass;
        }
    }

    /**
     * @function ? Decrease Interest in this menu. Passes decrease to parent and insures
     * that the menu closes when there is no longer interest in it. Also swaps css class to
     * attrs.className.
     */
    pane.decInterest = function(){
        interest--;
        if(!interest){
            pane.getElement().className = attrs.className;
        }
        contextManager.getParent(pane).decInterest(); // decrease interest in parent nodes too...
    }

    /**
     * @function ? Focus on this menu item - Add focus to the first element if existing.
     */
    pane.focus = function(){
        link.focus();
    }

    /**
     * @function ?
     * Get / Set the attributes for this layout pane - the returned object graph
     * should be regarded as immutable - edits to it could result in undefined behaviour.
     * If an attrs object was specified as a parameter its attributes will override
     * any existing ones, and this will be returned.
     * @param {optional} _attrs new attributes to be applied to this pane
     * @return _this if _attrs was defined, the current attrs otherwise.
     * @see tl.PaneAttrs
     */
    var paneAttrs = pane.attrs;
    pane.attrs = function(_attrs){
        if(_attrs){
            attrs = _attrs;
            paneAttrs(attrs);
            link.className = attrs.linkClass;
            link.href = attrs.href;
            link.innerHTML = attrs.text;
            link.onclick = click;
            link.target = attrs.target ? attrs.target : "";
            return pane;
        }else{
            return paneAttrs();
        }
    }

    /**
     * @function ? Dispose of this menu - remove from context manager and prevent memory
     * leaks
     */
    var paneDispose = pane.dispose;
    pane.dispose = function(){

        // remove any interest in parent generated from this item
        var parent = contextManager.getParent(pane);
        while(interest){
            interest--;
            parent.decInterest();
        }
        
        contextManager.remove(pane); // remove from context manager
        paneDispose(); // dispose of menu item
        pane = attrs = contextManager = link = paneAttrs = paneDispose = null;
    }

    // Increment interest in this item (If the item has a child, increment by proxy...)
    function incInterest(){
        var child = contextManager.getFirstChild(pane);
        if(child){
            child.incInterest();
        }else{
            pane.incInterest();
        }
    }

    // Decrement interest in this item (If the item has a child, decrement by proxy...)
    function decInterest(){
        var child = contextManager.getFirstChild(pane);
        if(child){
            child.decInterest();
        }else{
            pane.decInterest();
        }
    }

    //called in response to a click
    function click() {
        if(attrs.onclick){
            attrs.onclick();
        }
        link.blur();
    }

    //On key down
    link.onkeydown = function(event) {
        event = event ? event : window.event;
        var target;
        var keyCode = event.keyCode;
        if(horizontal ? (keyCode == 37) : (keyCode == 38)){ // if horizontal and left or vertical and up, select prev
            target = contextManager.getPrev(pane);
        }else if(horizontal ? (keyCode == 39) : (keyCode == 40)){ // if horizontal and right or vertical and down, select next
            target = contextManager.getNext(pane);
        }else if(horizontal ? (keyCode == 40) : (keyCode == 39)){ // if horizontal and down or vertical and right
            target = contextManager.getFirstChild(pane);
        }else if((keyCode == 27) || (horizontal ? (keyCode == 38) : (keyCode == 37))){ // if esc or horizontal and up or vertical and right
            pane.decInterest();
            target = contextManager.getFirstChild(pane);
            if(target){
                target.decInterest();
            }
            return;
        }
        if(target){
            target.focus(); // switch focus to a new menu item...
        }
    }
    link.onfocus = link.onmouseover = incInterest;
    link.onblur = link.onmouseout = decInterest;
    pane.getElement().appendChild(link);
    pane.attrs(attrs);
    return pane;
}


/** @class {static} tl.MenuPopupAttrs
 * This object contains the outline of default / accepted attributes for tl.MenuPopup instances
 * @extends tl.PopupPaneAttrs
 */
tl.MenuPopupAttrs = tl.copyAttributes(tl.PopupPaneAttrs, {
    /** @property ? Flag indicating that the popup should have its height set by the number of elements in it. */
    manageSize : true,
    /** @property ? Height of menu items in pixels (default 30) */
    itemHeight : 30,
    /** @property ? default width for popup menus (200) */
    width : 200,
    /** @property ? default width units for popup menus (px) */
    widthUnits : tl.Sizing.PIXEL,
    /** @property ? Default visibility (visible:false) */
    visible : false,
    /** @property ? Default outside click closes (and disposes) - false */
    outsideClickCloses : false
});

/**
 * @class tl.MenuPopup
 * Implementation of popup to contain a menu
 * @extends tl.PopupPane
 */
/** @constructor ?
 * @param {optional} attrs - attributes object for this pane
 * @param {optional} contextManager - manager for menu item navigation (generated if missing)
 * @param {optional} contentPane - preset content pane to use within the popup
 * @see tl.MenuPopupAttrs
 */
tl.MenuPopup = function(attrs, contextManager, contentPane) {
    attrs = tl.copyAttributes(tl.MenuPopupAttrs, attrs);
    var popupPane = new tl.PopupPane(attrs, contentPane);
    var interest = 0;
    var interval;


    /**
     * @function ? Increase Interest in this popup. Passes increase to parent and insures
     * that the menu stays open while there is interest in it (mouseover/focus).
     */
    popupPane.incInterest = function(){
        var parentItem = contextManager.getParent(popupPane);
        parentItem.incInterest(); // increase interest in parent nodes too
        interest++;
        clearInterval();
        if(interest == 1){
            var _attrs = {visible:true};
            var attrs = popupPane.attrs();
            var rect = parentItem.getOuterRect();
            var horizontal = contextManager.getParent(parentItem).attrs().horizontal;
            if(attrs.manageSize){
                _attrs.height = popupPane.getContentPane().getChildren().length * attrs.itemHeight;
                _attrs.heightUnits = tl.Sizing.PIXEL;
                if(horizontal){
                    _attrs.width = rect[2] - rect[0];
                    _attrs.widthUnits = tl.Sizing.PIXEL;
                }
            }
            var elem = parentItem.getElement();
            var x = 0;
            var y = 0;
            while(elem){
                x += elem.offsetLeft;
                y += elem.offsetTop;
                elem = elem.offsetParent;
            }
            if(horizontal){
                y += rect[3] - rect[1];
            }else{
                x += rect[2] - rect[0];
            }
            _attrs.x = x;
            _attrs.y = y;
            _attrs.xUnits = _attrs.yUnits = tl.Sizing.PIXEL;

            popupPane.attrs(_attrs);
        }
    }

    /**
     * @function ? Decrease Interest in this popup. Passes decrease to parent and insures
     * that the menu closes when there is no longer interest in it
     */
    popupPane.decInterest = function(){
        interest--;
        if(!interest){
            clearInterval();
            hideLater();
        }
        contextManager.getParent(popupPane).decInterest(); // decrease interest in parent nodes too...
    }

    /**
     * @function ? Focus on this popup - Add focus to the first element if existing.
     */
    popupPane.focus = function(){
        var firstChild = contextManager.getFirstChild(popupPane);
        if(firstChild){
            firstChild.focus();
        }
    }

    /**
     * @function ? Dispose of this popup - remove from context manager and prevent memory
     * leaks
     */
    var popupPaneDispose = popupPane.dispose;
    popupPane.dispose = function(){
        clearInterval();
        // remove any interest in parent generated from this item
        if(window.attachEvent){
            window.detachEvent("onunload", popupPane.dispose);
        }else{
            window.removeEventListener("unload", popupPane.dispose, true);
        }
        var p = popupPane;
        popupPane = null;
        var parent = contextManager.getParent(p);
        while(interest){
            interest--;
            parent.decInterest();
        }
        popupPaneDispose();
        popupPaneDispose = popupPane = contextManager = attrs = contentPane = null;
    }

    // Hide the popup after half a second - this delay allows the mouse to momentarily slide off the
    // popup without it getting hidden - makes popups easier to use.
    function hideLater() {
        interval = window.setInterval(function(){
            clearInterval();
            popupPane.attrs({visible:false});
        },500);
    }

    //Clear the hide interval
    function clearInterval() {
        if(interval){
            window.clearInterval(interval);
            interval = 0;
        }
    }

    if(window.attachEvent){
        window.attachEvent("onunload", popupPane.dispose);
    }else{
        window.addEventListener("unload", popupPane.dispose, true);
    }

    return popupPane;
}
