package com.espatial.doc4js;

import com.espatial.doc4js.config.RenderBuilder;
import com.espatial.doc4js.config.ResourceSetBuilder;
import com.espatial.doc4js.model.JSElement;
import com.espatial.doc4js.parser.CommentParser;
import com.espatial.doc4js.parser.JSDocParser;
import com.espatial.doc4js.render.JSDocRender;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.Collection;

/**
 * Hello world!
 *
 */
public class App {

    public static void main(String[] args) throws Exception {

        String configFile = (args.length > 0) ? args[0] : "config/settings.json";

        JsonObject config = new JsonParser().parse(new BufferedReader(new FileReader(configFile))).getAsJsonObject();

        execute(config);
    }

    public static void execute(JsonObject config) throws NullPointerException, IOException {
        
        //Get the resources to be processed
        Collection<URL> sources = new ResourceSetBuilder().getResources(config.getAsJsonObject("parse").get("sources"));

        JSDocParser parser = new CommentParser();
        JSElement root = parser.parseAll(sources);
        
        //Filter root at this point - includes and excludes may be defined 

        //Render the result
        JsonElement render = config.get("render");
        if(render.isJsonArray()){
            for(JsonElement rend : render.getAsJsonArray()){
                render(root, rend.getAsJsonObject());
            }
        }else{
            render(root, render.getAsJsonObject());
        }
    }

    private static void render(JSElement root, JsonObject renderElement) throws IOException {
        JSDocRender render = new RenderBuilder().buildRender(renderElement);
        render.render(root);
    }

    
}
