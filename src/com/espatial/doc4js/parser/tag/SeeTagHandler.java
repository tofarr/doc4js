package com.espatial.doc4js.parser.tag;

import com.espatial.doc4js.parser.error.ParseErrorType;
import com.espatial.doc4js.model.JSElement;
import com.espatial.doc4js.model.JSFunction;
import com.espatial.doc4js.model.JSParamSet;
import com.espatial.doc4js.model.JSReference;
import java.util.List;

/**
 *
 * @author tofarrell
 */
public class SeeTagHandler implements TagHandler {

    @Override
    public void handleTag(JSTag tag, TagHandlerContext context) {
        String text = tag.getText();
        int prevIndex = 0;
        int index = 0;
        while (index < text.length() && (text.charAt(index) > ' ')) {
            if (text.charAt(index) == '.') {
                String key = text.substring(prevIndex, index);
                if (AbstractElementTagHandler.isValidKey(key)) {
                    prevIndex = index + 1;
                } else { // The key was not valid
                    context.handleError(tag, ParseErrorType.INVALID_ELEMENT_NAME, key);
                    return;
                }
            }
            index++;
            
        }
        
        String key = text.substring(prevIndex, index);
        if(AbstractElementTagHandler.isValidKey(key)){
            JSElement element = context.getLastDefined();
            String description = (index < text.length()) ? text.substring(index+1) : "";
            JSReference reference = new JSReference(text.substring(0,index), description);
            if(element instanceof JSFunction){
                JSFunction function = (JSFunction)element;
                int numParamSets = function.numParamSets();
                if(numParamSets == 0){
                    function.addReference(reference);
                }else{
                    function.getParamSet(numParamSets-1).addReference(reference);
                }
            }else{
                element.addReference(reference);
            }
        }else{
            context.handleError(tag, ParseErrorType.INVALID_ELEMENT_NAME, key);
        }
    }

    @Override
    public String getTagName() {
        return "see";
    }
}
