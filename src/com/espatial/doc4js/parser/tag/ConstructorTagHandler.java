
package com.espatial.doc4js.parser.tag;

import com.espatial.doc4js.parser.error.ParseErrorType;
import com.espatial.doc4js.model.JSClass;
import com.espatial.doc4js.model.JSElement;

/**
 * This is here mainly for compatibility with jgrousedoc. It is not really nescessary unless you define the constructor after functions for a class
 * @author tofarrell
 */
public class ConstructorTagHandler implements TagHandler {

    @Override
    public void handleTag(JSTag tag, TagHandlerContext context) {
        JSElement parent = context.getCurrentParent();
        if(parent instanceof JSClass){
            JSClass jsClass = (JSClass)parent;
            context.setLastDefined(jsClass.getConstructor());
        }else{
            context.handleError(tag, ParseErrorType.CONSTRUCTOR_FOR_NON_CLASS);
        }
    }

    @Override
    public String getTagName() {
        return "constructor";
    }
    
}
