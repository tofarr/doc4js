package com.espatial.doc4js.parser.tag;

import com.espatial.doc4js.parser.error.ParseErrorType;
import com.espatial.doc4js.model.JSElement;
import com.espatial.doc4js.model.JSFunction;
import com.espatial.doc4js.model.JSParamSet;

/**
 *
 * @author tofarrell
 */
public class ParamSetTagHandler implements TagHandler {

    @Override
    public void handleTag(JSTag tag, TagHandlerContext context) {
        JSElement element = context.getLastDefined();
        if (element instanceof JSFunction) {
            JSFunction function = (JSFunction) element;
            
            JSParamSet paramSet = new JSParamSet();
            function.addParamSet(paramSet);
            
            String text = tag.getText();
            if((text != null) && (!text.isEmpty())){
                paramSet.setDescription(text);
            }

        } else {
            context.handleError(tag, ParseErrorType.PARAMS_FOR_NON_FUNCTION);
        }
    }

    @Override
    public String getTagName() {
        return "paramset";
    }
}
