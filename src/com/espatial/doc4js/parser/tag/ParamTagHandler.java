package com.espatial.doc4js.parser.tag;

import com.espatial.doc4js.parser.error.ParseErrorType;
import com.espatial.doc4js.model.JSParam;
import com.espatial.doc4js.model.JSParamSet;
import com.espatial.doc4js.model.JSElement;
import com.espatial.doc4js.model.JSFunction;

/**
 *
 * @author tofarrell
 */
public class ParamTagHandler implements TagHandler {

    @Override
    public void handleTag(JSTag tag, TagHandlerContext context) {
        JSElement element = context.getLastDefined();
        if (element instanceof JSFunction) {
            JSFunction function = (JSFunction) element;
            // add a parameter to a function...
            String text = tag.getText();

            String expectedType = null;
            int index = 0;
            if (text.charAt(0) == '{') { // we define a type / optional variable
                do {
                    index++;
                    if (index >= text.length()) {
                        context.handleError(tag, ParseErrorType.UNCLOSED_STRING_LITERAL, '{');
                        return;
                    }
                } while (text.charAt(index) != '}');
                expectedType = text.substring(1, index);
                index++;
                while ((index < text.length()) && (text.charAt(index) <= ' ')) {
                    index++;
                }
            }

            int indexStart = index;
            while ((index < text.length()) && (text.charAt(index) > ' ')) {
                index++;
            }
            String name = text.substring(indexStart, index);
            if (!AbstractElementTagHandler.isValidKey(name)) {
                context.handleError(tag, ParseErrorType.INVALID_ELEMENT_NAME, name);
                return;
            }
            index++;
            String description = (index < text.length()) ? text.substring(index) : null;


            //Now do we add this to the existing argument set.
            int numParamSets = function.numParamSets();
            JSParamSet paramSet;
            if (numParamSets == 0) { // If there were no argument sets, then we define one
                paramSet = new JSParamSet();
                function.addParamSet(paramSet);
            } else {
                paramSet = function.getParamSet(numParamSets-1); // get the last argument set
            }

            paramSet.addParam(new JSParam(name, description, expectedType));

        } else {
            context.handleError(tag, ParseErrorType.PARAMS_FOR_NON_FUNCTION);
        }
    }

    @Override
    public String getTagName() {
        return "param";
    }
}
