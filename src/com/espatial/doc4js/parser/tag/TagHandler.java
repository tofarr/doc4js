package com.espatial.doc4js.parser.tag;

/**
 * Handler that updates / addes JSElements based on the instructions found in tags
 * @author tofar_000
 */
public interface TagHandler {

    public void handleTag(JSTag tag, TagHandlerContext context);
    
    public String getTagName();
}
