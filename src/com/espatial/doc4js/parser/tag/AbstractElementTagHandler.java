package com.espatial.doc4js.parser.tag;

import com.espatial.doc4js.parser.error.ParseErrorType;
import com.espatial.doc4js.model.JSClass;
import com.espatial.doc4js.model.JSCodeBlock;
import com.espatial.doc4js.model.JSElement;
import com.espatial.doc4js.model.JSFunction;
import com.espatial.doc4js.model.JSNamespace;
import com.espatial.doc4js.model.JSProperty;
import com.espatial.doc4js.model.JSReference;
import java.util.regex.Pattern;

/**
 *
 * @author tofarrell
 */
public abstract class AbstractElementTagHandler implements TagHandler {

    private static Pattern PATTERN = Pattern.compile("[a-zA-Z0-9$€£_]*");
    private final String tagName;

    public AbstractElementTagHandler(String tagName) {
        this.tagName = tagName;
    }

    public AbstractElementTagHandler() {
        String name = getClass().getSimpleName().toLowerCase();
        tagName = name.replace("taghandler", "");
    }

    @Override
    public void handleTag(JSTag tag, TagHandlerContext context) {
        JSElement element = handleTagInternal(tag, context);
        if(element != null) {
            context.setCurrentParent(element);
        }
    }

    protected abstract JSElement createElement(String key, String description);

    public String getTagName() {
        return tagName;
    }

    static boolean isValidKey(String key) {
        if((key == null) || key.isEmpty()){
            return false;
        }
        if(PATTERN.matcher(key).matches()){
            char c = key.charAt(0);
            if(c < '0' || (c > '9')){
                return true;
            }
        }
        return false;
    }

    protected JSElement handleTagInternal(JSTag tag, TagHandlerContext context) {
        String text = tag.getText();
        int prevIndex = 0;
        int index = 0;
        JSElement parent = context.getCurrentParent();
        boolean useLocalParent = true;
        while (index < text.length() && (text.charAt(index) > ' ')) {
            if (text.charAt(index) == '.') {
                String key = text.substring(prevIndex, index);
                if (isValidKey(key)) {
                    if (useLocalParent) {
                        useLocalParent = false;
                        parent = context.getRoot();
                    }
                    JSElement child = parent.getChild(key);
                    if(child == null){
                        child = new JSElement(key, null);
                        parent.putChild(child);                      
                    }
                    parent = child;
                    prevIndex = index + 1;
                } else { // The key was not valid
                    context.handleError(tag, ParseErrorType.INVALID_ELEMENT_NAME, key);
                    return null;
                }
            }
            index++;
        }
        String key = text.substring(prevIndex, index);
        if (isValidKey(key)) {
            JSElement element = createElement(key, text.substring(index).trim());
            
            if(parent instanceof JSFunction){
                context.handleError(tag, ParseErrorType.CHILD_OF_FUNCTION);
            }

            JSElement child = parent.getChild(key);
            if(child != null){ // Element already existed...
                if (JSElement.class.equals(child.getClass())) { // If element type was not explicitly specified
                    for(JSElement grandChild : child.getChildren()){
                        child.removeChild(grandChild.getKey());
                        element.putChild(grandChild);
                    }
                    for(JSCodeBlock codeBlock : child.getCodeBlocks()){
                        element.addCodeBlock(codeBlock);
                    }
                    for(JSReference reference : child.getReferences()){
                        element.addReference(reference);
                    }
                } else {
                    context.handleError(tag, ParseErrorType.EXISTING_ELEMENT);
                    return null;
                }
            }
            
            parent.putChild(element);
            context.setLastDefined(element);
            return element;
        } else { // The key was not valid, so
            context.handleError(tag, ParseErrorType.INVALID_ELEMENT_NAME, key);
            return null;
        }
    }

    public static class NamespaceTagHandler extends AbstractElementTagHandler {

        @Override
        protected JSNamespace createElement(String key, String descrption) {
            return new JSNamespace(key, descrption);
        }
    }

    public static class FunctionTagHandler extends AbstractElementTagHandler {

        @Override
        protected JSFunction createElement(String key, String descrption) {
            return new JSFunction(key, descrption);
        }

        @Override
        public void handleTag(JSTag tag, TagHandlerContext context) {
            handleTagInternal(tag, context);
        }
    }

    public static class PropertyTagHandler extends AbstractElementTagHandler {

        @Override
        protected JSProperty createElement(String key, String descrption) {
            return new JSProperty(key, descrption);
        }

        @Override
        public void handleTag(JSTag tag, TagHandlerContext context) {
            handleTagInternal(tag, context);
        }
    }

    public static class ClassTagHandler extends AbstractElementTagHandler {

        @Override
        protected JSClass createElement(String key, String descrption) {
            return new JSClass(key, descrption);
        }
        
        @Override
        public void handleTag(JSTag tag, TagHandlerContext context) {
            JSClass element = (JSClass)handleTagInternal(tag, context);
            if(element != null) {
                context.setCurrentParent(element);
                context.setLastDefined(element.getConstructor());
            }
        }
    }
}
