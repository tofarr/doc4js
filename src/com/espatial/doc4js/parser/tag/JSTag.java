package com.espatial.doc4js.parser.tag;

import java.util.Objects;
import java.util.regex.Pattern;

/**
 * Immutable Class encapsulating a tag within a comment in a javascript
 * document.
 *
 * @author tofarrell
 */
public final class JSTag implements Cloneable {

    private static Pattern PATTERN = Pattern.compile("[a-zA-Z]+");
    private final int lineNumber;
    private final String tagName;
    private final String text;

    /**
     * Create a new isntance of JSTag
     *
     * @param lineNumber the line number on which the tag was located
     * @param tagName the name of the tag
     * @param text text of the tag - (if null, gets assigned "")
     * @throws IllegalArgumentException if the line number was less than 0 or
     * the tag name was not valid
     * @throws NullPointerException if location or tag name was null
     */
    public JSTag(int lineNumber, String tagName, String text) throws IllegalArgumentException, NullPointerException {
        tagName = tagName.toLowerCase();
        if ((lineNumber < 0) || (!isValidTagName(tagName))) {
            throw new IllegalArgumentException();
        }
        this.lineNumber = lineNumber;
        this.tagName = tagName.toLowerCase();
        this.text = (text == null) ? "" : text;
    }
    
    public static boolean isValidTagName(String tagName){
        return PATTERN.matcher(tagName).matches();
    }

    public int getLineNumber() {
        return lineNumber;
    }

    public String getTagName() {
        return tagName;
    }

    public String getText() {
        return text;
    }

    @Override
    public String toString() {
        return "JSTag{" + "lineNumber=" + lineNumber + ", tagName=" + tagName + ", text=" + text + '}';
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + lineNumber;
        hash = 89 * hash + tagName.hashCode();
        hash = 89 * hash + text.hashCode();
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof JSTag){
            final JSTag other = (JSTag) obj;
            return (lineNumber == other.lineNumber) 
                && tagName.equals(other.tagName)
                && text.equals(other.text);
        }
        return false;
    }
    
    @Override
    public JSTag clone(){
        return this;
    }
    
}
