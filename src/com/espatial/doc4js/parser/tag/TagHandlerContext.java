package com.espatial.doc4js.parser.tag;

import com.espatial.doc4js.parser.error.ErrorHandler;
import com.espatial.doc4js.parser.error.ParseError;
import com.espatial.doc4js.parser.error.ParseErrorType;
import com.espatial.doc4js.model.JSElement;
import java.net.URL;

/**
 *
 * @author tofarrell
 */
public class TagHandlerContext {

    private final JSElement root;
    private final ErrorHandler errorHandler;
    private URL url;
    private JSElement currentParent;
    private JSElement lastDefined;

    public TagHandlerContext(JSElement root, ErrorHandler errorHandler) {
        if((root == null) || (errorHandler == null)){
            throw new NullPointerException();
        }
        this.root = root;
        this.errorHandler = errorHandler;
        this.currentParent = root;
        this.lastDefined = root;
    }

    public URL getUrl() {
        return url;
    }

    public void setUrl(URL url) {
        this.url = url;
    }

    public JSElement getRoot() {
        return root;
    }

    public JSElement getCurrentParent() {
        return currentParent;
    }

    public void setCurrentParent(JSElement currentParent) {
        this.currentParent = currentParent;
    }

    public JSElement getLastDefined() {
        return lastDefined;
    }

    public void setLastDefined(JSElement lastDefined) {
        this.lastDefined = lastDefined;
    }

    public ErrorHandler getErrorHandler(){
        return errorHandler;
    }
    
    public void handleError(JSTag tag, ParseErrorType error, Object... arguments) {
        errorHandler.handleError(new ParseError(url, tag.getLineNumber(), error, arguments));
    }
}
