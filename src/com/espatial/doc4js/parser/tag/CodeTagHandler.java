
package com.espatial.doc4js.parser.tag;

import com.espatial.doc4js.parser.error.ParseErrorType;
import com.espatial.doc4js.model.JSCodeBlock;
import com.espatial.doc4js.model.JSCodeBlock.JSCodeBlockType;
import com.espatial.doc4js.model.JSElement;
import com.espatial.doc4js.model.JSFunction;
import com.espatial.doc4js.model.JSProperty;

/**
 * Handler for code tag used to define code samples
 * @author tofarrell
 */
public class CodeTagHandler implements TagHandler {

    @Override
    public void handleTag(JSTag tag, TagHandlerContext context) {
        
        String text = tag.getText();
        
        //First string is type
        int index = 0;
        do{
            index++;
            if(index >= text.length()){
                context.handleError(tag, ParseErrorType.INVALID_CODE_BLOCK);
                return;
            }
        }while(text.charAt(index) > ' ');
        JSCodeBlockType codeBlockType;
        try{
            codeBlockType = JSCodeBlockType.valueOf(text.substring(0, index).toUpperCase());
        }catch(IllegalArgumentException ex){
            context.handleError(tag, ParseErrorType.UNKNOWN_CODE_BLOCK_TYPE, text.substring(0, index).toUpperCase());
            return;
        }
        
        while(text.charAt(index) <= ' '){
            index++;
        }
        
        //If there is a {}, then this is the description...
        String description = null;
        char c;
        if((c = text.charAt(index)) == '{'){
            int start = index+1;
            do{
                index++;
                if(c == '\\'){
                    index++;
                }
                if(index >= text.length()){
                    context.handleError(tag, ParseErrorType.INVALID_CODE_BLOCK);
                    return;
                }
            }while((c = text.charAt(index)) != '}');
            description = text.substring(start, index);
            index++;
        }
        
        //rest is actual code - do simple indentation on it
        String code = indent(text, index);
        
        JSCodeBlock codeBlock = new JSCodeBlock(description, code.toString(), codeBlockType);

        JSElement element = context.getLastDefined();
        if(element instanceof JSFunction){
            JSFunction function = (JSFunction)element;
            int numParamSets = function.numParamSets();
            if(numParamSets == 0){
                function.addCodeBlock(codeBlock);
            }else{
                function.getParamSet(numParamSets-1).addCodeBlock(codeBlock);
            }
        }else{
            if(element instanceof JSProperty){
                element = context.getCurrentParent();
            }
            element.addCodeBlock(codeBlock);
        }
    }

    @Override
    public String getTagName() {
        return "code";
    }
    
    /**
     * Method for doing simple code indentation
     * @param text the text to indent
     * @param index the index within the text to begin output
     * @return indented text
     */
    static String indent(String text, int index){
        StringBuilder code = new StringBuilder();
        int indent = 0;
        for(int i = index; i < text.length(); i++){
            char c = text.charAt(i);
            switch(c){
                case '\n':
                    code.append(c);
                    for(int n = indent; n > 0; n--){
                        code.append(' ');
                    }
                    break;
                case '{':
                    indent += 4;
                    code.append(c);
                    break;
                case '}':
                    indent -= 4;
                    code.append(c);
                    break;
                default:
                    code.append(c);
                    //No action required
            }
        }
        return code.toString();
    }
    
}
