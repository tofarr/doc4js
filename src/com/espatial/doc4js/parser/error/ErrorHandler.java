/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.espatial.doc4js.parser.error;

import java.net.URL;

/**
 *
 * @author tofarrell
 */
public interface ErrorHandler {

    /**
     * Process an error / inconsistency in a file
     *
     * @param error the error
     */
    public void handleError(ParseError error);
}
