package com.espatial.doc4js.parser.error;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tofarrell
 */
public class LoggingErrorHandler implements ErrorHandler {
    
    private static final Logger LOG = Logger.getLogger(LoggingErrorHandler.class.getName());
     
    @Override
    public void handleError(ParseError error) {
        LOG.log(Level.WARNING, "{0}\t:\t{1}\t:\t{2}", new Object[]{error.getUrl(), error.getLineNumber(), error.getType().getMessage(error.getArguments())});
    }
}
