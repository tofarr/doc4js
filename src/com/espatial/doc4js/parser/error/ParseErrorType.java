package com.espatial.doc4js.parser.error;

import java.text.MessageFormat;

/**
 *
 * @author tofarrell
 */
public enum ParseErrorType {
    //TODO: It may make sense to load message strings from a resource bundle...
    UNEXPECTED_EOF("Unexpected End of File in Comment!"),
    NO_TAGS_IN_COMMENT("Found comment which contained no tags!"),
    INVALID_TAG_NAME("Invalid Tag Name : {0}"),
    UNKNOWN_TAG("Unknown Tag : {0}"),
    INVALID_ELEMENT_NAME("Invalid javascript element name : {0}"), 
    EXISTING_ELEMENT("Attempting to redefine existing element"),
    CHILD_OF_FUNCTION("Defining an element as a child of a function"),
    CONSTRUCTOR_FOR_NON_CLASS("Attempting to define constructor for non class!"),
    PARAMS_FOR_NON_FUNCTION("Attempting to define parameters for an object which is not a function!"),
    RETURN_FOR_NON_FUNCTION("Attempting to define return type for an object which is not a function!"),
    UNCLOSED_STRING_LITERAL("Unclosed string literal : {0}"),
    INVALID_CODE_BLOCK("Invalid code block : {0}"),
    UNKNOWN_CODE_BLOCK_TYPE("Unknown code block type : {0}"),
    GENERAL("Error : {0}");
    
    private final String msg;

    ParseErrorType(String msg) {
        this.msg = msg;
    }
    
    public String getMessage(Object... arguments){
        return MessageFormat.format(msg, arguments);
    }
}
