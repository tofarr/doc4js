package com.espatial.doc4js.parser.error;

import java.net.URL;
import java.util.Arrays;

/**
 *
 * @author tofar_000
 */
public final class ParseError implements Cloneable {
    
    private final URL url;
    private final int lineNumber;
    private final ParseErrorType type;
    private final Object[] arguments;

    public ParseError(URL url, int lineNumber, ParseErrorType type, Object... arguments) throws NullPointerException, IllegalArgumentException {
        if((url == null) || (type == null)){
            throw new NullPointerException();
        }
        if(lineNumber < 0){
            throw new IllegalArgumentException();
        }
        this.url = url;
        this.lineNumber = lineNumber;
        this.type = type;
        this.arguments = (arguments == null) ? new Object[0] : arguments.clone();
    }

    public URL getUrl() {
        return url;
    }

    public int getLineNumber() {
        return lineNumber;
    }

    public ParseErrorType getType() {
        return type;
    }

    public Object[] getArguments() {
        return arguments;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + url.hashCode();
        hash = 53 * hash + lineNumber;
        hash = 53 * hash + this.type.hashCode();
        hash = 53 * hash + Arrays.deepHashCode(this.arguments);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof ParseError){
            
        final ParseError other = (ParseError) obj;
        return url.equals(other.url)
            && (lineNumber == other.lineNumber)
            && (type == other.type)
            && Arrays.deepEquals(this.arguments, other.arguments);
        }
        return false;
    }

    @Override
    public String toString() {
        return "ParseError{" + "url=" + url + ", lineNumber=" + lineNumber + ", type=" + type + ", arguments=" + arguments + '}';
    }

    @Override
    protected ParseError clone() {
        return this;
    }
    
    
}
