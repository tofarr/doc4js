package com.espatial.doc4js.parser;

import com.espatial.doc4js.model.JSElement;
import com.espatial.doc4js.parser.comment.CommentExtractor;
import com.espatial.doc4js.parser.comment.CommentTagHandler;
import com.espatial.doc4js.parser.error.ErrorHandler;
import com.espatial.doc4js.parser.error.LoggingErrorHandler;
import java.io.IOException;
import java.net.URL;
import java.util.Collection;

/**
 *
 * @author tofarrell
 */
public class CommentParser implements JSDocParser {

    private final CommentTagHandler commentTagHandler;
    private final CommentExtractor commentExtractor;
    
    public CommentParser(CommentTagHandler commentTagHandler){
        this.commentTagHandler = commentTagHandler;
        this.commentExtractor = new CommentExtractor(commentTagHandler, commentTagHandler.getContext().getErrorHandler());
    }
    
    public CommentParser(ErrorHandler errorHandler) throws NullPointerException {
        if (errorHandler == null) {
            throw new NullPointerException();
        }
        this.commentTagHandler = new CommentTagHandler(errorHandler);
        this.commentExtractor = new CommentExtractor(commentTagHandler, errorHandler);
    }

    public CommentParser() {
        this(new LoggingErrorHandler());
    }

    @Override
    public void parse(URL url) throws NullPointerException, IOException {
        commentExtractor.extract(url);
    }

    @Override
    public JSElement parseAll(Collection<URL> urls) throws NullPointerException, IOException {
        for (URL url : urls) {
            parse(url);
        }
        return getRoot();
    }
    
    @Override
    public JSElement parseAll(URL... urls) throws NullPointerException, IOException {
        for (URL url : urls) {
            parse(url);
        }
        return getRoot();
    }

    @Override
    public JSElement getRoot() {
        return commentTagHandler.getContext().getRoot();
    }

    public CommentTagHandler getCommentTagHandler() {
        return commentTagHandler;
    }
}
