package com.espatial.doc4js.parser.comment;

import java.net.URL;

/**
 * Handler for comments in javascript files
 * @author tofarrell
 */
public interface CommentHandler {

    /**
     * Handle a comment from a javascript file
     * @param comment comment to process
     */
    public void handleComment(Comment comment);
}
