package com.espatial.doc4js.parser.comment;

import com.espatial.doc4js.parser.tag.JSTag;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * Immutable class defining a comment section from a javascript file
 *
 * @author tofarrell
 */
public final class Comment implements Cloneable {

    private final URL url;
    private final int lineNumber;
    private final List<JSTag> tags;

    /**
     * Create a new instance of Comment
     *
     * @param url the url of the file where this comment was retrieved
     * @param lineNumber the line number
     * @param tags list of tags
     * @throws NullPointerException if tags was null or url was null
     * @throws IllegalArgumentException if tags was empty or lineNumber was less
     * than 0
     */
    public Comment(URL url, int lineNumber, List<JSTag> tags) throws NullPointerException, IllegalArgumentException {
        if (url == null) {
            throw new NullPointerException();
        }
        if ((lineNumber < 0) || tags.isEmpty()) {
            throw new IllegalArgumentException();
        }
        this.url = url;
        this.lineNumber = lineNumber;
        this.tags = Collections.unmodifiableList(new ArrayList<>(tags));
    }

    public URL getUrl() {
        return url;
    }

    public int getLineNumber() {
        return lineNumber;
    }

    public List<JSTag> getTags() {
        return tags;
    }

    @Override
    public String toString() {
        return "Comment{" + "url=" + url + ", lineNumber=" + lineNumber + ", tags=" + tags + '}';
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 41 * hash + url.hashCode();
        hash = 41 * hash + lineNumber;
        hash = 41 * hash + tags.hashCode();
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Comment){
            final Comment other = (Comment) obj;
            return url.equals(other.url)
                && (lineNumber == other.lineNumber)
                && tags.equals(other.tags);
        }
        return true;
    }
    
    @Override
    public Comment clone(){
        return this;
    }
}
