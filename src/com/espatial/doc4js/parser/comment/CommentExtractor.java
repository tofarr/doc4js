package com.espatial.doc4js.parser.comment;

import com.espatial.doc4js.parser.error.ErrorHandler;
import com.espatial.doc4js.parser.error.ParseError;
import com.espatial.doc4js.parser.error.ParseErrorType;
import com.espatial.doc4js.parser.tag.JSTag;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.Reader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Object for extracting comments from JS files
 * @author tofarrell
 */
public class CommentExtractor {

    private final CommentHandler commentHandler;
    private final ErrorHandler errorHandler;

    /**
     * Create a new instance of CommentExtractor
     * @param commentHandler handler for comments
     * @param errorHandler handler for errors
     * @throws NullPointerException
     */
    public CommentExtractor(CommentHandler commentHandler, ErrorHandler errorHandler) throws NullPointerException {
        if((commentHandler == null) || (errorHandler == null)){
            throw new NullPointerException();
        }
        this.commentHandler = commentHandler;
        this.errorHandler = errorHandler;
    }

    /**
     * Extract Javascript comments from the url given and pass them to the defined comment processor
     *
     * @param url the url from which to extract javascript comments
     * @throws IOException if there was an error reading from the url
     * @throws NullPointerException if url was null
     */
    public void extract(final URL url) throws IOException, NullPointerException {
        extract(url, new InputStreamReader(url.openStream()));
    }
    
    /**
     * Extract Javascript comments from the url given and pass them to the defined comment processor
     *
     * @param url the url from which to extract javascript comments
     * @param reader the reader for data
     * @throws IOException if there was an error reading from the url
     * @throws NullPointerException if url was null
     */
    public void extract(final URL url, Reader reader) throws IOException, NullPointerException {
        try (final LineNumberReader lineNumberReader = new LineNumberReader(reader)) {
            while (skipToNextJSComment(lineNumberReader)) {
                int lineNumber = lineNumberReader.getLineNumber()+1;
                List<JSTag> tags = parseJSTags(url, errorHandler, lineNumberReader);
                if (tags == null) {
                    errorHandler.handleError(new ParseError(url, lineNumber, ParseErrorType.UNEXPECTED_EOF));
                    break;
                } else if (tags.isEmpty()) {
                    errorHandler.handleError(new ParseError(url, lineNumber, ParseErrorType.NO_TAGS_IN_COMMENT));
                } else {
                    commentHandler.handleComment(new Comment(url, lineNumber, tags));
                }
            }
        }
    }

    /**
     * Skip to the next JSDoc comment from the reader given
     *
     * @param reader reader for javascript file
     * @return next js doc comment - list of lines
     * @throws EOFException if an unexpected end of file was reached
     * @throws IOException if there was an error reading
     */
    static boolean skipToNextJSComment(Reader reader) throws IOException {
        int c;
        while ((c = reader.read()) > 0) {
            if (c == '/') {
                c = reader.read();
                if(c == '/'){ // line comment - skip to next newline char
                    do{
                        c = reader.read();
                    }while ((c > 0) && (c != '\n'));
                }else if (c == '*') {
                    c = reader.read();
                    if(c == '*'){ // found a '/' followed by '**'
                        return true;
                    }else if(c <= 0){
                        return false;
                    }
                } else if (c <= 0) {
                    return false; // catch special case where / is last character in stream
                }
            }
        }
        return false;
    }

    /**
     * Process a js comment
     *
     * @param reader the reader from which to parse js tags
     * @return array of js tags, or null if end of file was reached
     * @throws IOException if there was an error reading
     */
    static List<JSTag> parseJSTags(URL url, ErrorHandler errorHandler, LineNumberReader reader) throws IOException {
        int lineNumber = reader.getLineNumber()+1;
        List<String> lines = readRestOfComment(reader);
        if (lines == null) {
            return null; // unexpected end of file
        } else {
            List<JSTag> ret = new ArrayList<>();
            String tagName = null;
            int tagLineNumber = -1;
            StringBuilder data = new StringBuilder();
            for (int i = 0, size = lines.size(); i < size; i++) {
                String line = lines.get(i);
                if ((!line.isEmpty()) && (line.charAt(0) == '@')) {
                    int start = 1;
                    while ((start < line.length()) && (line.charAt(start) <= ' ')) {
                        start++;
                    }
                    int end = start + 1;
                    while ((end < line.length()) && (line.charAt(end) > ' ')) {
                        end++;
                    }
                    if (tagName != null) {
                        if(JSTag.isValidTagName(tagName)){
                            removeTrailingNewLines(data);
                            ret.add(new JSTag(tagLineNumber, tagName, data.toString())); // If there is an existing tag, add it.
                        }else{
                            errorHandler.handleError(new ParseError(url, tagLineNumber, ParseErrorType.INVALID_TAG_NAME, tagName));
                        }
                    }
                    tagName = line.substring(start, Math.min(end, line.length()));
                    tagLineNumber = lineNumber + i;
                    data.setLength(0);
                    end++;
                    if(end < line.length()){
                        data.append(line.substring(end));
                    }
                } else if (tagName != null) {
                    if (data.length() > 0) {
                        data.append('\n').append(line);
                    }else if(!line.isEmpty()){ //do not add leading empty lines
                        data.append(line);
                    }
                }
            }
            if (tagName != null) {
                if(JSTag.isValidTagName(tagName)){
                    removeTrailingNewLines(data);
                    ret.add(new JSTag(tagLineNumber, tagName, data.toString()));
                }else{
                    errorHandler.handleError(new ParseError(url, tagLineNumber, ParseErrorType.INVALID_TAG_NAME, tagName));
                }
            }
            return ret;
        }
    }
    
    static void removeTrailingNewLines(StringBuilder str){
        while((str.length() > 0) && (str.charAt(str.length()-1) == '\n')){
            str.setLength(str.length()-1);
        }
    }

    /**
     * Read the rest of a comment from the reader given
     *
     * @param reader the reader from which to read
     * @return a string comment or null if end of file was unexpectedly reached
     */
    static List<String> readRestOfComment(Reader reader) throws IOException {
        StringBuilder str = new StringBuilder();
        List<String> lines = new ArrayList<>();
        int c;
        while ((c = reader.read()) > 0) {
            if (c == '*') {
                c = reader.read();
                if (c <= 0) {
                    return null; // unexpected end of file
                } else if (c == '/') {
                    processLine(str, lines);
                    return lines;
                } else {
                    str.append('*');
                }
            }
            if (c == '\n') { //new line
                processLine(str, lines);
                str.setLength(0); // reset stringbuilder for next line
            }else{
                str.append((char) c);
            }
        }
        return null;
    }
     
    /**
     * Process a line, adding it to the list of available lines if it does not contain only white space...
     *
     * @param reader the reader from which to read
     * @return a string comment or null if end of file was unexpectedly reached
     */
    static void processLine(StringBuilder line, List<String> lines){
        int start = 0;
        int end = line.length();
        if(end > 0){
            
            //Clip white space at the start
            while ((start < end) && (line.charAt(start) <= ' ')) {
                start++;
            }
            if ((start < end) && (line.charAt(start) == '*')) { // Cut off leading * on lines
                start++;
                while ((start < end) && (line.charAt(start) <= ' ')) {
                    start++;
                }
            }
            
            //Clip white space at the end
            do{
                end--;
            }while ((end > start) && (line.charAt(end) <= ' '));
            end++;
            
        }
        if(start != end){
            lines.add(line.substring(start, end));
        }else{
            lines.add("");
        }
    }
}
