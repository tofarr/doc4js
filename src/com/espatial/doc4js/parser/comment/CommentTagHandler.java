package com.espatial.doc4js.parser.comment;

import com.espatial.doc4js.parser.error.ErrorHandler;
import com.espatial.doc4js.parser.error.ParseErrorType;
import com.espatial.doc4js.model.JSElement;
import com.espatial.doc4js.parser.tag.AbstractElementTagHandler.ClassTagHandler;
import com.espatial.doc4js.parser.tag.AbstractElementTagHandler.FunctionTagHandler;
import com.espatial.doc4js.parser.tag.AbstractElementTagHandler.NamespaceTagHandler;
import com.espatial.doc4js.parser.tag.AbstractElementTagHandler.PropertyTagHandler;
import com.espatial.doc4js.parser.tag.CodeTagHandler;
import com.espatial.doc4js.parser.tag.ConstructorTagHandler;
import com.espatial.doc4js.parser.tag.JSTag;
import com.espatial.doc4js.parser.tag.ParamSetTagHandler;
import com.espatial.doc4js.parser.tag.ParamTagHandler;
import com.espatial.doc4js.parser.tag.ReturnTagHandler;
import com.espatial.doc4js.parser.tag.SeeTagHandler;
import com.espatial.doc4js.parser.tag.TagHandler;
import com.espatial.doc4js.parser.tag.TagHandlerContext;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author tofarrell
 */
public class CommentTagHandler implements CommentHandler {

    private final Map<String, TagHandler> handlers;
    private final TagHandlerContext context;

    public CommentTagHandler(Collection<TagHandler> handlers, TagHandlerContext context) {
        this.handlers = new HashMap<>();
        for (TagHandler handler : handlers) {
            this.handlers.put(handler.getTagName(), handler);
        }
        this.context = context;
    }

    public CommentTagHandler(JSElement root, ErrorHandler errorHandler) {
        this(getDefaultMainTagHandlers(), new TagHandlerContext(root, errorHandler));
    }

    public CommentTagHandler(ErrorHandler errorHandler) {
        this(new JSElement("window",null), errorHandler);
    }

    /**
     * Get the default handlers for main tags
     */
    static Collection<TagHandler> getDefaultMainTagHandlers() {
        return new ArrayList<>(Arrays.asList(
            new NamespaceTagHandler(),
            new PropertyTagHandler(),
            new FunctionTagHandler(),
            new ClassTagHandler(),
            new ConstructorTagHandler(),
            new CodeTagHandler(),
            new ParamSetTagHandler(),
            new ParamTagHandler(),
            new ReturnTagHandler(),
            new SeeTagHandler()
        ));
    }

    @Override
    public void handleComment(final Comment comment) {
        if(!comment.getUrl().equals(context.getUrl())){
            context.setUrl(comment.getUrl());
            context.setLastDefined(context.getRoot());
            context.setCurrentParent(context.getRoot());
        }
        for (JSTag tag : comment.getTags()) { //Process each tag in turn, updating the element as appropriate
            String tagName = tag.getTagName();
            TagHandler handler = handlers.get(tagName);
            if (handler == null) {
                context.handleError(tag, ParseErrorType.UNKNOWN_TAG, tagName);
            } else {
                try{
                    handler.handleTag(tag, context);
                }catch(Exception ex){                                                  // If an error occured while parsing that we don't
                    context.handleError(tag, ParseErrorType.GENERAL, ex.getMessage()); // explicitly support, mark it as general
                }
            }
        }
    }

    /**
     * Get the root JS element
     *
     * @return the root js element
     */
    public TagHandlerContext getContext() {
        return context;
    }
}
