package com.espatial.doc4js.parser;

import com.espatial.doc4js.model.JSElement;
import java.io.IOException;
import java.net.URL;
import java.util.Collection;

/**
 *
 * @author tofarrell
 */
public interface JSDocParser {

    public void parse(URL url) throws NullPointerException, IOException;
    
    public JSElement parseAll(Collection<URL> urls) throws NullPointerException, IOException;
    
    public JSElement parseAll(URL ... urls) throws NullPointerException, IOException;
    
    public JSElement getRoot();
}
