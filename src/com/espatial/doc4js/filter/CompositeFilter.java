package com.espatial.doc4js.filter;

import com.espatial.doc4js.model.JSElement;

/**
 *
 * @author tofar_000
 */
public class CompositeFilter implements JSElementFilter {

    private final JSElementFilter[] filters;

    public CompositeFilter(JSElementFilter[] filters) throws NullPointerException {
        for(int f = filters.length; f-- > 0;){
            if(filters[f] == null){
                throw new NullPointerException();
            }
        }
        this.filters = filters;
    }

    public JSElementFilter[] getFilters() {
        return filters;
    }

    @Override
    public JSElement filter(JSElement root) {
        for (JSElementFilter filter : filters) {
            root = filter.filter(root);
        }
        return root;
    }
}
