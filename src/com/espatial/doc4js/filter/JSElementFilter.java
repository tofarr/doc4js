package com.espatial.doc4js.filter;

import com.espatial.doc4js.model.JSElement;

/**
 * Object that performs some transformation on an element and returns an altered version of it
 * @author tofar_000
 */
public interface JSElementFilter {
    
    /**
     * Filter the element given and return a transformed version of it
     * @param root the element to transform
     * @return a transformed element
     */
    public JSElement filter(JSElement root);
}
