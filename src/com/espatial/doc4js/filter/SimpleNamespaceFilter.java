package com.espatial.doc4js.filter;

import com.espatial.doc4js.model.JSElement;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author tofar_000
 */
@SuppressWarnings("unchecked")
public class SimpleNamespaceFilter implements JSElementFilter {

    private final Map<String, Object> includeRoot;
    private final Map<String, Object> excludeRoot;

    public SimpleNamespaceFilter(String[] includes, String[] excludes) {
        this.includeRoot = buildTree(includes);
        this.excludeRoot = buildTree(excludes);
    }

    @Override
    public JSElement filter(JSElement root) throws NullPointerException {
        root = root.clone();
        if (includeRoot != null) {
            include(root, includeRoot);
        }
        if (excludeRoot != null) {
            exclude(root, excludeRoot);
        }
        return root;
    }

    static void include(JSElement element, Map<String, Object> currentLevel) {
        for (JSElement child : element.getChildren()) {
            String key = child.getKey();
            Map<String, Object> subLevel = (Map<String, Object>) currentLevel.get(key);
            if (subLevel == null) {
                element.removeChild(key);
            } else if(!subLevel.isEmpty()) {
                include(child, subLevel);
            }
        }
    }

    static void exclude(JSElement element, Map<String, Object> currentLevel) {
        for (JSElement child : element.getChildren()) {
            String key = child.getKey();
            Map<String, Object> subLevel = (Map<String, Object>) currentLevel.get(key);
            if (subLevel != null) {
                if(subLevel.isEmpty()) {
                    element.removeChild(key);
                }else{ 
                    exclude(child, subLevel);
                }
            }
        }
    }

    /**
     * Build a tree structure from the paths given
     */
    static Map<String, Object> buildTree(String[] paths) {
        Map<String, Object> ret;
        if (paths == null) {
            ret = null;
        } else {
            ret = new HashMap<>();
            for (String include : paths) {
                Map<String, Object> node = ret;
                for (String key : include.split("\\.")) {
                    Map<String, Object> subNode = (Map<String, Object>) node.get(key);
                    if (subNode == null) {
                        node.put(key, subNode = new HashMap<>());
                        node = subNode;
                    } else if (subNode.isEmpty()) {
                        node = subNode;
                        break; //whole block is included
                    } else {
                        node = subNode;
                    }
                }
                node.clear();
            }
        }
        return ret;
    }
}
