package com.espatial.doc4js.config;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.regex.Pattern;

/**
 *
 * @author tofarrell
 */
public class ResourceSetBuilder {

    /**
     * Default to include only javascript files
     */
    static final Pattern DEFAULT_INCLUDE = Pattern.compile(".*\\.js");

    public Collection<URL> getResources(JsonElement element) throws MalformedURLException {
        Collection<URL> results = new ArrayList<>();
        getResources(element, results);
        return results;
    }

    public void getResources(JsonElement element, Collection<URL> results) throws MalformedURLException {
        if (element.isJsonArray()) {
            JsonArray array = element.getAsJsonArray();
            for (int i = array.size(); i-- > 0;) {
                getResources(array.get(i), results);
            }
        } else if (element.isJsonObject()) {
            JsonObject obj = element.getAsJsonObject();
            List<Pattern> includes = extractPatternList(obj, "include");
            if (includes.isEmpty()) {
                includes.add(DEFAULT_INCLUDE);
            }
            List<Pattern> excludes = extractPatternList(obj, "exclude");
            File path = new File(obj.get("path").getAsString());
            processPath(path, includes, excludes, results);
        } else {
            String str = element.getAsString();
            try {
                URL url = new URL(str);
                results.add(url);
            } catch (Exception ex) {
                File path = new File(str);
                processPath(path, Arrays.asList(DEFAULT_INCLUDE), new ArrayList<Pattern>(), results);
            }
        }
    }

    private List<Pattern> extractPatternList(JsonObject json, String memberName) {
        List<Pattern> patterns = new ArrayList<>();
        if (json.has(memberName)) {
            JsonElement element = json.get(memberName);
            if (element.isJsonArray()) {
                JsonArray array = element.getAsJsonArray();
                for (int i = 0; i < array.size(); i++) {
                    patterns.add(Pattern.compile(array.get(i).getAsString()));
                }
            } else {
                patterns.add(Pattern.compile(element.getAsString()));
            }
        }
        return patterns;
    }

    private void processPath(File file, List<Pattern> includes, Collection<Pattern> excludes, Collection<URL> results) throws MalformedURLException {
        if (file.isDirectory()) {
            for (File child : file.listFiles()) {
                processPath(child, includes, excludes, results);
            }
        } else {
            String path = file.getAbsolutePath();
            for (Pattern exclude : excludes) {
                if (exclude.matcher(path).matches()) {
                    return;
                }
            }
            if (includes.isEmpty()) {
                results.add(file.toURI().toURL());
            } else {
                for (Pattern include : includes) {
                    if (include.matcher(path).matches()) {
                        results.add(file.toURI().toURL());
                        return;
                    }
                }
            }
        }
    }
}
