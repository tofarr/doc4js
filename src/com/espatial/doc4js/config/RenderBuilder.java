package com.espatial.doc4js.config;

import com.espatial.doc4js.filter.SimpleNamespaceFilter;
import com.espatial.doc4js.render.FilterRender;
import com.espatial.doc4js.render.JSDocRender;
import com.espatial.doc4js.render.JSONRender;
import com.espatial.doc4js.render.VelocityRender;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.io.File;

/**
 *
 * @author tofar_000
 */
public class RenderBuilder {

    public JSDocRender buildRender(JsonObject renderConfig) {
        JSDocRender render;
        if (renderConfig.has("type") && "json".equals(renderConfig.get("type").getAsString())) {
            File outputFile = renderConfig.has("outputFile") ? new File(renderConfig.get("outputFile").getAsString()) : JSONRender.DEFAULT_FILE;
            String jsonP = renderConfig.has("jsonP") ? renderConfig.get("jsonP").getAsString() : null;
            render = new JSONRender(outputFile, jsonP, JSONRender.DEFAULT_GSON);
        } else {
            File templateDir = renderConfig.has("templateDir") ? new File(renderConfig.get("templateDir").getAsString()) : VelocityRender.DEFAULT_TEMPLATE_DIR;
            File outputDir = renderConfig.has("outputDir") ? new File(renderConfig.get("outputDir").getAsString()) : VelocityRender.DEFAULT_OUTPUT_DIR;
            String title = renderConfig.has("title") ? renderConfig.get("title").getAsString() : "Javascript Documentation";
            String version = renderConfig.has("version") ? renderConfig.get("version").getAsString() : "1.0";
            String owner = renderConfig.has("owner") ? renderConfig.get("owner").getAsString() : "";
            String[] dependencies;
            if (renderConfig.has("dependencies")) {
                JsonArray depsArray = renderConfig.get("dependencies").getAsJsonArray();
                dependencies = new String[depsArray.size()];
                for (int d = dependencies.length; d-- > 0;) {
                    dependencies[d] = depsArray.get(d).getAsString();
                }
            } else {
                dependencies = null;
            }
            render = new VelocityRender(templateDir, outputDir, title, owner, version, dependencies);
        }
        render = applyFilters(render, renderConfig);
        return render;
    }

    JSDocRender applyFilters(JSDocRender render, JsonObject renderConfig) {
        String[] include = parseStringArray(renderConfig, "include");
        String[] exclude = parseStringArray(renderConfig, "exclude");
        if((include != null) || (exclude != null)){
            render = new FilterRender(new SimpleNamespaceFilter(include, exclude), render);
        }
        return render;
    }
    
    String[] parseStringArray(JsonObject obj, String key){
        String[] ret;
        if(obj.has(key)){
            JsonElement elem = obj.get(key);
            if(elem.isJsonArray()){
                JsonArray array = elem.getAsJsonArray();
                ret = new String[array.size()];
                for(int i = ret.length; i-- > 0;){
                    ret[i] = array.get(i).getAsString();
                }
            }else{
                ret = new String[]{elem.getAsString()};
            }
        }else{
            ret = null;
        }
        return ret;
    }
}
