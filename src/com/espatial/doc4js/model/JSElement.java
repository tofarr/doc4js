package com.espatial.doc4js.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;
import java.util.TreeMap;
import java.util.regex.Pattern;

/**
 *
 * @author tofarrell
 */
public class JSElement {

    public static final JSElement[] EMPTY = new JSElement[0];
    private static Pattern PATTERN = Pattern.compile("[a-zA-Z0-9$€£_]*");
    private final String type; //attribute included so that it will appear in json ;)
    private final String key;
    private final String description;
    private TreeMap<String, JSElement> children; //Everything can have children - even a function... (Though that is really messed up if it does)
    private List<JSCodeBlock> codeBlocks;
    private List<JSReference> references;
    private JSElement parent;
    private String path;

    public JSElement(String key, String description) {
        if (key == null) {
            throw new NullPointerException();
        }
        this.type = getClass().getSimpleName();
        this.key = key; // technically key can be anything if bracked notation is used...
        this.description = description;
        this.children = new TreeMap<>();
        this.codeBlocks = new ArrayList<>();
        this.references = new ArrayList<>();
    }

    public String getType() {
        return type;
    }

    public String getKey() {
        return key;
    }

    public String getDescription() {
        return description;
    }

    public String path() {
        if (path == null) {
            if (parent == null) {
                if("window".equals(key)){
                    path = "";
                }else{
                    path = isStandardKey(key) ? key : ("[\"" + key.replaceAll("\"", "\\\"") + "\"]");
                }
            } else {
                StringBuilder str = new StringBuilder(parent.path());
                if(isStandardKey(key)){
                    if(str.length() > 0){
                        str.append('.');
                    }
                    str.append(key);
                }else{
                    if(str.length() == 0){
                        str.append("window");
                    }
                    str.append("[\"").append(key.replaceAll("\"", "\\\"")).append("\"]");
                }
                path =  str.toString();
            }
        }
        return path;
    }

    public static boolean isStandardKey(String key) {
        if ((key == null) || key.isEmpty()) {
            return false;
        }
        if (PATTERN.matcher(key).matches()) {
            char c = key.charAt(0);
            if (c < '0' || (c > '9')) {
                return true;
            }
        }
        return false;
    }

    public int numChildren() {
        return children.size();
    }

    private void setParent(final JSElement parent) {
        this.parent = parent;
        this.path = null;
    }

    public void putChild(final JSElement element) throws IllegalArgumentException {
        if (element.parent != null) {
            if (element.parent != this) {
                throw new IllegalArgumentException("Element is already a child of another element!");
            }
        } else {
            if (children == null) {
                children = new TreeMap<>();
            }
            JSElement prev = children.put(element.getKey(), element);
            if (prev != null) {
                prev.setParent(null); // prev child is removed if it exists
            }
            element.setParent(this);
        }
    }

    public JSElement removeChild(final String key) {
        if (children == null) {
            return null;
        } else {
            JSElement child = children.remove(key);
            if (child != null) {
                child.setParent(null);
            }
            return child;
        }
    }

    public JSElement getChild(final String key) {
        return (children == null) ? null : children.get(key);
    }

    public JSElement[] getChildren() {
        return (children == null) ? EMPTY : children.values().toArray(new JSElement[children.size()]);
    }
    
    public List<JSProperty> childProperties(){
        if(children == null){
            return Collections.emptyList();
        }
        List<JSProperty> properties = new ArrayList<>();
        for(JSElement element : children.values()){
            if(element instanceof JSProperty){
                properties.add((JSProperty)element);
            }
        }
        return properties;
    }
    
    public List<JSFunction> childFunctions(){
        if(children == null){
            return Collections.emptyList();
        }
        List<JSFunction> functions = new ArrayList<>();
        for(JSElement element : children.values()){
            if(element instanceof JSFunction){
                functions.add((JSFunction)element);
            }
        }
        return functions;
    }
    
    public int numCodeBlocks(){
        return (codeBlocks == null) ? 0 : codeBlocks.size();
    }
    
    public final void addCodeBlock(JSCodeBlock codeBlock){
        if(codeBlock == null){
            throw new NullPointerException();
        }
        if (codeBlocks == null){
            codeBlocks = new ArrayList<>();
        }
        codeBlocks.add(codeBlock);
    }
    
    public void removeCodeBlock(int index){
        codeBlocks.remove(index);
    }
    
    public JSCodeBlock getCodeBlock(int index){
        return codeBlocks.get(index);
    }
    
    public JSCodeBlock[] getCodeBlocks(){
        return (codeBlocks == null) ? JSCodeBlock.EMPTY : codeBlocks.toArray(new JSCodeBlock[codeBlocks.size()]);
    }
    
    public final void addReference(JSReference reference){
        if(reference == null){
            throw new NullPointerException();
        }
        if (references == null){
            references = new ArrayList<>();
        }
        references.add(reference);
    }
    
    public int numReferences(){
        return (references == null) ? 0 : references.size();
    }
    
    public boolean removeReference(JSReference reference){
        return (references == null) ? false : references.remove(reference);
    }
    
    public JSReference getReference(int index){
        return references.get(index);
    }
    
    public JSReference[] getReferences(){
        return (references == null) ? JSReference.EMPTY : references.toArray(new JSReference[references.size()]);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 17 * hash + Objects.hashCode(this.type);
        hash = 17 * hash + Objects.hashCode(this.key);
        hash = 17 * hash + Objects.hashCode(this.description);
        hash = 17 * hash + Objects.hashCode(this.children);
        hash = 17 * hash + Objects.hashCode(this.codeBlocks);
        hash = 17 * hash + Objects.hashCode(this.references);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof JSElement) {
            final JSElement other = (JSElement) obj;
            return Objects.equals(this.type, other.type)
                    && Objects.equals(this.key, other.key)
                    && Objects.equals(this.description, other.description)
                    && Objects.equals(this.children, other.children)
                    && Objects.equals(this.codeBlocks, other.codeBlocks)
                    && Objects.equals(this.references, other.references);
        }
        return false;
    }

    @Override
    public String toString() {
        return "JSElement{" + "key=" + key + '}';
    }
    
    @Override
    public JSElement clone(){
        JSElement ret = new JSElement(key, description);
        copyTo(ret);
        return ret;
    }
    
    /**
     * Copy the internal state of this element to the element given
     */
    void copyTo(JSElement element){
        if(this.codeBlocks != null){
            element.codeBlocks = new ArrayList<>(this.codeBlocks);
        }
        if(this.references != null){
            element.references = new ArrayList<>(this.references);
        }
        if(this.children != null){
            for(JSElement child : children.values()){
                element.putChild(child.clone());
            }
        }
    }
}
