package com.espatial.doc4js.model;

import java.util.Objects;

/**
 * Javascript argument bean
 *
 * @author tofarrell
 */
public class JSParam implements Cloneable {

    public static final JSParam[] EMPTY = new JSParam[0];
    private final String name;
    private final String description;
    private final String expectedType;

    public JSParam(String name, String description, String expectedType) throws NullPointerException {
        this.name = name;
        this.description = description;
        this.expectedType = expectedType;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getExpectedType() {
        return expectedType;
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 61 * hash + Objects.hashCode(this.name);
        hash = 61 * hash + Objects.hashCode(this.description);
        hash = 61 * hash + Objects.hashCode(this.expectedType);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof JSParam) {
            final JSParam other = (JSParam) obj;
            return Objects.equals(this.name, other.name)
                    && Objects.equals(this.description, other.description)
                    && Objects.equals(this.expectedType, other.expectedType);
        }
        return false;
    }

    @Override
    public String toString() {
        return "JSParam{" + "name=" + name + '}';
    }

    @Override
    public JSParam clone(){
        return this;
    }
}
