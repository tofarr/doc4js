package com.espatial.doc4js.model;

import java.util.Objects;

/**
 *
 * @author tofarrell
 */
public class JSClass extends JSElement {

    private final JSFunction constructor;
    
    public JSClass(String key, String description, JSFunction constructor) {
        super(key, description);
        if(constructor == null){
            throw new NullPointerException();
        }
        this.constructor = constructor;
    }
    
    public JSClass(String key, String description){
        this(key, description, new JSFunction("constructor",null));
    }
    
    public JSFunction getConstructor() {
        return constructor;
    }

    @Override
    public int hashCode() {
        int hash = super.hashCode();
        hash = 97 * hash + Objects.hashCode(this.constructor);
        return hash;
    }

    @Override
    public boolean equals(final Object obj) {
        if (super.equals(obj) && (obj instanceof JSClass)) {
            final JSClass other = (JSClass) obj;
            return Objects.equals(this.constructor, other.constructor);
        }
        return false;
    }

    @Override
    public String toString() {
        return "JSClass{" + "key=" + getKey() + '}';
    }
    
    @Override
    public JSClass clone(){
        JSClass ret = new JSClass(getKey(), getDescription(), constructor.clone());
        copyTo(ret);
        return ret;
    }
}
