package com.espatial.doc4js.model;

import java.util.Objects;

/**
 *
 * @author tofarrell
 */
public class JSCodeBlock implements Cloneable {
    
    public static final JSCodeBlock[] EMPTY = new JSCodeBlock[0];
    
    public enum JSCodeBlockType {

        /**
         * Code sample is displayed but is not runnable
         */
        DISPLAYED_NOT_RUNNABLE, // Code sample is not runnable
        /**
         * Code sample is static (run on page load) and displayed
         */
        STATIC_DISPLAYED,
        /**
         * Code sample is static (run on page load) and not displayed
         */
        STATIC_HIDDEN,
        /**
         * Code sample is run on click
         */
        ONCLICK
    }
    private final String description;
    private final String code;
    private final JSCodeBlockType type;

    public JSCodeBlock(String description, String code, JSCodeBlockType type) {
        this.description = description;
        this.code = code;
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public String getCode() {
        return code;
    }

    public JSCodeBlockType getType() {
        return type;
    }
    
    public String runnableCode(){
        return code.replaceAll("\"", "\\\"");
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.description);
        hash = 47 * hash + Objects.hashCode(this.code);
        hash = 47 * hash + (this.type != null ? this.type.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final JSCodeBlock other = (JSCodeBlock) obj;
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        if (!Objects.equals(this.code, other.code)) {
            return false;
        }
        if (this.type != other.type) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "JSCodeBlock{" + "code=" + code + '}';
    }
    
    @Override
    public JSCodeBlock clone(){
        return this;
    }
}
