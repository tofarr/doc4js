package com.espatial.doc4js.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author tofarrell
 */
public class JSFunction extends JSElement {

    public static final JSFunction[] EMPTY = new JSFunction[0];
    private List<JSParamSet> paramSets;

    public JSFunction(String key, String description) {
        super(key, description);
        paramSets = new ArrayList<>();
    }
    
    public int numParamSets(){
        return (paramSets == null) ? 0 : paramSets.size();
    }
    
    public void addParamSet(JSParamSet paramSet){
        if(paramSet == null){
            throw new NullPointerException();
        }
        if(paramSets == null){
            paramSets = new ArrayList<>();
        }
        paramSets.add(paramSet);
    }
    
    public void removeParamSet(int index){
        paramSets.remove(index);
    }
    
    public JSParamSet getParamSet(int index){
        return paramSets.get(index);
    }
    
    public JSParamSet[] getParamSets(){
        return (paramSets == null) ? JSParamSet.EMPTY : paramSets.toArray(new JSParamSet[paramSets.size()]);
    }

    @Override
    public int hashCode() {
        int hash = super.hashCode();
        hash = 37 * hash + Objects.hashCode(this.paramSets);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (super.equals(obj) && (obj instanceof JSFunction)){
            final JSFunction other = (JSFunction) obj;
            return Objects.equals(this.paramSets, other.paramSets);
        }
        return false;
    }
    
    @Override
    public String toString() {
        return "JSFunction{" + "key=" + getKey() + '}';
    }

    @Override
    public JSFunction clone() {
        JSFunction ret = new JSFunction(getKey(), getDescription());
        this.copyTo(ret);
        if(this.paramSets != null){
            ret.paramSets = new ArrayList<>(this.paramSets.size());
            for(JSParamSet paramSet : this.paramSets){
                ret.paramSets.add(paramSet.clone());
            }
        }
        return ret;
    }
}
