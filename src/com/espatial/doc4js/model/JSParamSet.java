package com.espatial.doc4js.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Javascript argument set
 *
 * @author tofarrell
 */
public class JSParamSet implements Cloneable {

    public static final JSParamSet[] EMPTY = new JSParamSet[0];
    private String description;
    private List<JSReference> references;
    private List<JSParam> params;
    private List<JSCodeBlock> codeBlocks;
    private JSParam returnValue;

    public JSParamSet() {
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int numReferences() {
        return (references == null) ? 0 : references.size();
    }

    public final void addReference(JSReference reference) {
        if (reference == null) {
            throw new NullPointerException();
        }
        if (references == null) {
            references = new ArrayList<>();
        }
        references.add(reference);
    }

    public boolean removeReference(JSReference reference) {
        return (references == null) ? false : references.remove(reference);
    }

    public JSReference getReference(int index) {
        return references.get(index);
    }

    public JSReference[] getReferences() {
        return (references == null) ? JSReference.EMPTY : references.toArray(new JSReference[references.size()]);
    }

    public int numParams() {
        return (params == null) ? 0 : params.size();
    }

    public void addParam(JSParam param) {
        if (param == null) {
            throw new NullPointerException();
        }
        if (params == null) {
            params = new ArrayList<>();
        }
        params.add(param);
    }

    public void removeParam(int index) {
        params.remove(index);
    }

    public JSParam getParam(int index) {
        return params.get(index);
    }

    public JSParam[] getParams() {
        return (params == null) ? JSParam.EMPTY : params.toArray(new JSParam[params.size()]);
    }

    public int numCodeBlocks() {
        return (codeBlocks == null) ? 0 : codeBlocks.size();
    }

    public final void addCodeBlock(JSCodeBlock codeBlock) {
        if (codeBlock == null) {
            throw new NullPointerException();
        }
        if (codeBlocks == null) {
            codeBlocks = new ArrayList<>();
        }
        codeBlocks.add(codeBlock);
    }

    public void removeCodeBlock(int index) {
        codeBlocks.remove(index);
    }

    public JSCodeBlock getCodeBlock(int index) {
        return codeBlocks.get(index);
    }

    public JSCodeBlock[] getCodeBlocks() {
        return (codeBlocks == null) ? JSCodeBlock.EMPTY : codeBlocks.toArray(new JSCodeBlock[codeBlocks.size()]);
    }

    public JSParam getReturnValue() {
        return returnValue;
    }

    public void setReturnValue(JSParam returnValue) {
        this.returnValue = returnValue;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 11 * hash + Objects.hashCode(this.description);
        hash = 11 * hash + Objects.hashCode(this.references);
        hash = 11 * hash + Objects.hashCode(this.params);
        hash = 11 * hash + Objects.hashCode(this.codeBlocks);
        hash = 11 * hash + Objects.hashCode(this.returnValue);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof JSParamSet) {
            final JSParamSet other = (JSParamSet) obj;
            return Objects.equals(this.description, other.description)
                    && Objects.equals(this.references, other.references)
                    && Objects.equals(this.params, other.params)
                    && Objects.equals(this.codeBlocks, other.codeBlocks)
                    && Objects.equals(this.returnValue, other.returnValue);
        }
        return false;
    }

    @Override
    public String toString() {
        return "JSParamSet{" + "description=" + description + '}';
    }

    @Override
    public JSParamSet clone() {
        JSParamSet ret = new JSParamSet();
        if (codeBlocks != null) {
            ret.codeBlocks = new ArrayList<>(codeBlocks); // JSCodeBlock is immutable so this is fine
        }
        ret.description = description;
        if (ret.params != null) {
            ret.references = new ArrayList<>(references); // JSReference is immutable so this is fine
        }
        ret.returnValue = returnValue;
        return ret;
    }
}
