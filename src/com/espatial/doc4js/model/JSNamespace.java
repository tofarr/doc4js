package com.espatial.doc4js.model;

/**
 *
 * @author tofarrell
 */
public class JSNamespace extends JSElement {

    public JSNamespace(String key, String description) {
        super(key, description);
    }

    @Override
    public String toString() {
        return "JSNamespace{" + "key=" + getKey() + '}';
    }

    @Override
    public JSNamespace clone() {
        JSNamespace ret = new JSNamespace(getKey(), getDescription());
        this.copyTo(ret);
        return ret;
    }
}
