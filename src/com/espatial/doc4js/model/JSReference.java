package com.espatial.doc4js.model;

import java.util.Objects;

/**
 * A reference from one element to another within javascript documentation.
 *
 * @see tag
 * @author tofarrell
 */
public class JSReference implements Cloneable {

    public static JSReference[] EMPTY = new JSReference[0];
    private final String path;
    private final String description;

    public JSReference(String path, String description) throws NullPointerException, IllegalArgumentException {
        if (path.length() == 0) {
            throw new IllegalArgumentException();
        }
        this.path = path;
        this.description = description;
    }

    public String getPath() {
        return path;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + this.path.hashCode();
        hash = 53 * hash + Objects.hashCode(this.description);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof JSReference) {
            final JSReference other = (JSReference) obj;
            return this.path.equals(other.path)
                    && Objects.equals(this.description, other.description);
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        return "JSReference{" + "path=" + path + '}';
    }
    
    @Override
    public JSReference clone(){
        return this;
    }
}
