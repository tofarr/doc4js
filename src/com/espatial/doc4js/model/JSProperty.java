package com.espatial.doc4js.model;

import java.util.List;
import java.util.Objects;

/**
 *
 * @author tofarrell
 */
public class JSProperty extends JSElement {

    public static final JSProperty[] EMPTY = new JSProperty[0];
    //TODO: Auto add reference for type of default value? (Phase 2!)
    private String defaultValue;

    public JSProperty(String key, String description) {
        super(key, description);
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    @Override
    public int hashCode() {
        int hash = super.hashCode();
        hash = 97 * hash + Objects.hashCode(this.defaultValue);
        return hash;
    }

    @Override
    public boolean equals(final Object obj) {
        if (super.equals(obj) && (obj instanceof JSProperty)) {
            final JSProperty other = (JSProperty) obj;
            return Objects.equals(this.defaultValue, other.defaultValue);
        }
        return false;
    }

    @Override
    public String toString() {
        return "JSProperty{" + "key=" + getKey() + '}';
    }
    
    @Override
    public JSProperty clone(){
        JSProperty ret = new JSProperty(getKey(), getDescription());
        ret.defaultValue = defaultValue;
        copyTo(ret);
        return ret;
    }
}
