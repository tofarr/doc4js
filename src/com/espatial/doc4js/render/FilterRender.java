package com.espatial.doc4js.render;

import com.espatial.doc4js.filter.JSElementFilter;
import com.espatial.doc4js.model.JSElement;
import java.io.IOException;

/**
 * Render that performs some filter operation on the root element before delegating to another render
 * @author tofarrell
 */
public class FilterRender implements JSDocRender {

    /** Filter to apply */
    private final JSElementFilter filter;
    /** Delegate render */
    private final JSDocRender render;

    public FilterRender(JSElementFilter filter, JSDocRender render) throws NullPointerException {
        if((filter == null) || (render == null)){
            throw new NullPointerException();
        }
        this.filter = filter;
        this.render = render;
    }

    /**
     * Get the filter to apply
     * @return the filter to apply
     */
    public JSElementFilter getFilter() {
        return filter;
    }

    /**
     * Get the delegate render
     * @return the delegate render
     */
    public JSDocRender getRender() {
        return render;
    }

    @Override
    public void render(final JSElement root) throws IOException {
        JSElement filtered = filter.filter(root);
        render.render(filtered);
    }
}
