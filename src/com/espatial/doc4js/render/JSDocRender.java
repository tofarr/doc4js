package com.espatial.doc4js.render;

import com.espatial.doc4js.model.JSElement;
import java.io.IOException;

/**
 * Object responsible for creating javascript documentation based on a normalized structure of nodes.
 * 
 * Typically immutable.
 * 
 * @author tofarrell
 */
public interface JSDocRender {

    /**
     * Render / Save the JSElement given.
     * @param root the element to save
     * @throws IOException if there was an error
     */
    public void render(JSElement root) throws IOException;
}
