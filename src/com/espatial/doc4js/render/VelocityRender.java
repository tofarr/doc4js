package com.espatial.doc4js.render;

import com.espatial.doc4js.model.JSElement;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.MethodInvocationException;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;

/**
 *
 * @author tofarrell
 */
public class VelocityRender implements JSDocRender {

    public static final File DEFAULT_TEMPLATE_DIR = new File("config/template");
    public static final File DEFAULT_OUTPUT_DIR = new File("output");
    private final VelocityEngine velocityEngine;
    private final File templateDir;
    private final File outputDir;
    private final String title;
    private final String owner;
    private final String version;
    private final String[] dependencies;

    public VelocityRender(File templateDir, File outputDir, String title, String owner, String version, String[] dependencies) {
        Properties properties = new Properties();
        properties.setProperty("file.resource.loader.path", templateDir.toString());
        this.velocityEngine = new VelocityEngine(properties);
        this.templateDir = templateDir;
        this.outputDir = outputDir;
        this.title = title;
        this.owner = owner;
        this.version = version;
        this.dependencies = dependencies;
    }

    public VelocityRender() {
        this(DEFAULT_TEMPLATE_DIR, DEFAULT_OUTPUT_DIR, "Javascript Documentation", "", "1.0", null);
    }

    @Override
    public void render(JSElement root) throws IOException {

        VelocityContext context = createContext(root);

        //Load template
        Template template = null;
        try {
            template = velocityEngine.getTemplate("index.html");
        } catch (ResourceNotFoundException | ParseErrorException | MethodInvocationException ex) {
            throw new RuntimeException(ex); // This should not be possible unless the config / template has been screwed up
        }

        //Merge template with ctx and write to file
        delete(outputDir);
        outputDir.mkdirs();
        File file = new File(outputDir, "index.html");
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
            template.merge(context, writer);
        }

        //Copy library to output
        copy(new File(templateDir, "lib"), new File(outputDir, "lib"));
    }

    /**
     * Copy files/directories from src to dst
     */
    private static void copy(File src, File dst) throws IOException {
        if (src.isDirectory()) {
            dst.mkdirs();
            for (File file : src.listFiles()) {
                copy(file, new File(dst, file.getName()));
            }
        } else {
            if (!dst.exists()) {
                dst.createNewFile();
            }
            try (FileChannel srcChan = new FileInputStream(src).getChannel()) {
                try (FileChannel dstChan = new FileOutputStream(dst).getChannel()) {
                    dstChan.transferFrom(srcChan, 0, srcChan.size());
                }
            }
        }
    }

    /**
     * Create a ctx form the root element given
     */
    private VelocityContext createContext(JSElement root) throws IOException {
        VelocityContext ctx = new VelocityContext();

        ctx.put("title", title);
        ctx.put("version", version);
        ctx.put("owner", owner);
        String buildDate = new SimpleDateFormat("yyyy.MMMMM.dd").format(new Date());
        ctx.put("date", buildDate);
        String year = new SimpleDateFormat("yyyy").format(new Date());
        ctx.put("year", year);
        ctx.put("root", root);
        if (dependencies != null) {
            ctx.put("dependencies", dependencies);
        }

        return ctx;
    }

    public File getTemplateDir() {
        return templateDir;
    }

    public File getOutputDir() {
        return outputDir;
    }

    public VelocityEngine getVelocityEngine() {
        return velocityEngine;
    }

    public String getTitle() {
        return title;
    }

    public String getOwner() {
        return owner;
    }

    public String getVersion() {
        return version;
    }

    public String[] getDependencies() {
        return dependencies;
    }

    /**
     * Delete a file/directory
     */
    private static void delete(File file) {
        if (file.isDirectory()) {
            for (File child : file.listFiles()) {
                delete(child);
            }
        }
        file.delete();
    }
}
