package com.espatial.doc4js.render;

import com.espatial.doc4js.model.JSElement;
import com.google.gson.Gson;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

/**
 * JSDocRender that relies on gson
 *
 * @author tofarrell
 */
public class JSONRender implements JSDocRender {

    public static final File DEFAULT_FILE = new File("output/docs.json");
    public static final Gson DEFAULT_GSON = new Doc4JsGson().build();
    private final File outputFile;
    private final String jsonP;
    private final Gson gson;

    public JSONRender(File outputFile, String jsonP, Gson gson) {
        this.outputFile = outputFile;
        this.jsonP = jsonP;
        this.gson = gson;
    }

    public JSONRender() {
        this(DEFAULT_FILE, null, DEFAULT_GSON);
    }

    @Override
    public void render(JSElement root) throws IOException {
        boolean hasJSONP = (jsonP != null) && (!jsonP.isEmpty());
        outputFile.getParentFile().mkdirs();
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile))) {
            if (hasJSONP) {
                writer.append(jsonP).append('(');
            }
            gson.toJson(root, writer);
            if (hasJSONP) {
                writer.write(')');
            }
        }
    }

    public File getOutputFile() {
        return outputFile;
    }
    
    public Gson getGson(){
        return gson;
    }
}
