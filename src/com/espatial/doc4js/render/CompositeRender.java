package com.espatial.doc4js.render;

import com.espatial.doc4js.model.JSElement;
import java.io.IOException;
import java.util.Collection;

/**
 * Render that aggregates updates and passes them to multiple other renders.
 * 
 * @author tofarrell
 */
public class CompositeRender implements JSDocRender {

    /** Delegate renders */
    private final JSDocRender[] renders;

    /**
     * Create a new instance of CompositeRender
     * @param renders delegate renders
     * @throws NullPointerException if renders was null or one of the elements within it was null
     * @throws IllegalArgumentException if renders was an empty array
     */
    public CompositeRender(JSDocRender... renders) throws NullPointerException, IllegalArgumentException {
        for (int r = renders.length; r-- > 0;) { // Quick defensive null check
            if (renders[r] == null) {
                throw new NullPointerException();
            }
        }
        if (renders.length == 0) { // An empty array will render nothing, which is clearly wrong!
            throw new IllegalArgumentException();
        }
        this.renders = renders.clone();
    }

    /**
     * Create a new instance of CompositeRender
     * @param renders delegate renders
     * @throws NullPointerException if renders was null or one of the elements within it was null
     * @throws IllegalArgumentException if renders was an empty array
     */
    public CompositeRender(Collection<JSDocRender> renders) throws NullPointerException, IllegalArgumentException  {
        this(renders.toArray(new JSDocRender[renders.size()]));
    }

    /**
     * Get a copy of the array of delegate renders
     * @return a copy of the array of delegate renders
     */
    public JSDocRender[] getRenders() {
        return renders.clone();
    }

    @Override
    public void render(JSElement root) throws IOException {
        for(int r = 0; r < renders.length; r++){
            renders[r].render(root);
        }
    }
    
    
}
