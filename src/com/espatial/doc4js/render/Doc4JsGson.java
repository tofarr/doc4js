package com.espatial.doc4js.render;

import com.espatial.doc4js.model.JSClass;
import com.espatial.doc4js.model.JSCodeBlock;
import com.espatial.doc4js.model.JSElement;
import com.espatial.doc4js.model.JSFunction;
import com.espatial.doc4js.model.JSNamespace;
import com.espatial.doc4js.model.JSParamSet;
import com.espatial.doc4js.model.JSProperty;
import com.espatial.doc4js.model.JSReference;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import java.lang.reflect.Type;

/**
 *
 * @author tofarrell
 */
@SuppressWarnings("unchecked")
public class Doc4JsGson {

    public Gson build() {
        GsonBuilder builder = new GsonBuilder();
        for(Serializer serializer : Serializer.values()){
            builder.registerTypeAdapter(serializer.getType(), serializer);
        }
        return builder.create();
    }

    private static JsonObject serializeJsElement(JSElement element, JsonSerializationContext context) {
        JsonObject ret = new JsonObject();
        ret.addProperty("type", element.getType());
        ret.addProperty("key", element.getKey());
        ret.addProperty("description", element.getDescription());
        if (element.numChildren() > 0) {
            JsonArray children = new JsonArray();
            for (JSElement child : element.getChildren()) {
                children.add(context.serialize(child));
            }
            ret.add("children", children);
        }
        if (element.numCodeBlocks() > 0) {
            JsonArray codeBlocks = new JsonArray();
            for (JSCodeBlock codeBlock : element.getCodeBlocks()) {
                codeBlocks.add(context.serialize(codeBlock));
            }
            ret.add("codeBlocks", codeBlocks);
        }
        if (element.numReferences() > 0) {
            JsonArray references = new JsonArray();
            for (JSReference reference : element.getReferences()) {
                references.add(context.serialize(reference));
            }
            ret.add("children", references);
        }
        return ret;
    }

    private enum Serializer implements JsonSerializer {

        JS_ELEMENT {
            @Override
            public JsonElement serialize(Object obj, Type type, JsonSerializationContext context) {
                return serializeJsElement((JSElement) obj, context);
            }

            @Override
            public Class getType() {
                return JSElement.class;
            }
        },
        JS_PROPERTY {
            @Override
            public JsonElement serialize(Object obj, Type type, JsonSerializationContext context) {
                JSProperty property = (JSProperty) obj;
                JsonObject ret = serializeJsElement(property, context);
                if (property.getDefaultValue() != null) {
                    ret.addProperty("defaultValue", property.getDefaultValue());
                }
                return ret;
            }

            @Override
            public Class getType() {
                return JSProperty.class;
            }
        },
        JS_FUNCTION {
            @Override
            public JsonElement serialize(Object obj, Type type, JsonSerializationContext context) {
                JSFunction function = (JSFunction) obj;
                JsonObject ret = serializeJsElement(function, context);
                if(function.numParamSets() > 0){
                    JsonArray paramSets = new JsonArray();
                    for (JSParamSet paramSet : function.getParamSets()) {
                        paramSets.add(context.serialize(paramSet));
                    }
                    ret.add("paramSets", paramSets);
                }
                return ret;
            }

            @Override
            public Class getType() {
                return JSFunction.class;
            }
        },
        JS_NAMESPACE {
            @Override
            public JsonElement serialize(Object obj, Type type, JsonSerializationContext context) {
                JSNamespace namespace = (JSNamespace) obj;
                JsonObject ret = serializeJsElement(namespace, context);
                return ret;
            }

            @Override
            public Class getType() {
                return JSNamespace.class;
            }
        },
        JS_CLASS {
            @Override
            public JsonElement serialize(Object obj, Type type, JsonSerializationContext context) {
                JSClass clazz = (JSClass) obj;
                JsonObject ret = serializeJsElement(clazz, context);
                ret.add("function", JS_FUNCTION.serialize(clazz.getConstructor(), JSFunction.class, context));
                return ret;
            }

            @Override
            public Class getType() {
                return JSClass.class;
            }
        };

        public abstract Class getType();
    };
}
