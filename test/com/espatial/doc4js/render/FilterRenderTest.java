/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.espatial.doc4js.render;

import com.espatial.doc4js.filter.JSElementFilter;
import com.espatial.doc4js.model.JSElement;
import java.io.IOException;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.fail;
import junit.framework.TestCase;

/**
 *
 * @author tofarrell
 */
public class FilterRenderTest extends TestCase {
    
    public FilterRenderTest(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }
    
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }
    
    public void testFunctionality() throws IOException{
        MockRender mockRender = new MockRender();
        JSElement replacement = new JSElement("foo",null);
        MockFilter mockFilter = new MockFilter(replacement);
        try{
            new FilterRender(null, mockRender);
            fail("Should have thrown NullPointerException");
        }catch(NullPointerException ex){ // As planned
        }
        try{
            new FilterRender(mockFilter, null);
            fail("Should have thrown NullPointerException");
        }catch(NullPointerException ex){ // As planned
        }
        try{
            new FilterRender(null, null);
            fail("Should have thrown NullPointerException");
        }catch(NullPointerException ex){ // As planned
        }
        FilterRender render = new FilterRender(mockFilter, mockRender);
        assertEquals(mockFilter, render.getFilter());
        assertEquals(mockRender, render.getRender());
        JSElement root = new JSElement("bar",null);
        render.render(root);
        assertEquals(replacement, mockRender.getRendered());
    }
    
    public static class MockFilter implements JSElementFilter {

        private final JSElement replacement;

        public MockFilter(JSElement replacement) {
            this.replacement = replacement;
        }

        @Override
        public JSElement filter(JSElement root) {
            return replacement;
        }

        public JSElement getReplacement() {
            return replacement;
        }
    }

    public static class MockRender implements JSDocRender{

        private JSElement rendered;
        
        @Override
        public void render(JSElement root) throws IOException {
            if(rendered != null){
                throw new IllegalStateException();
            }
            rendered = root;
        }

        public JSElement getRendered() {
            return rendered;
        }
        
    }
}
