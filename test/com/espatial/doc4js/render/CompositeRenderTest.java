/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.espatial.doc4js.render;

import com.espatial.doc4js.model.JSElement;
import com.espatial.doc4js.render.FilterRenderTest.MockRender;
import java.io.IOException;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.fail;
import junit.framework.TestCase;

/**
 *
 * @author tofarrell
 */
public class CompositeRenderTest extends TestCase {
    
    public CompositeRenderTest(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }
    
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testFunctionality() throws IOException{
        try{
            new CompositeRender(null);
            fail("Should have thrown NullPointerException");
        }catch(NullPointerException ex){ // As planned
        }
        try{
            new CompositeRender(new JSDocRender[]{null,new MockRender()});
            fail("Should have thrown NullPointerException");
        }catch(NullPointerException ex){ // As planned
        }
        try{
            new CompositeRender(new JSDocRender[]{new MockRender(),null});
            fail("Should have thrown NullPointerException");
        }catch(NullPointerException ex){ // As planned
        }
        
        JSDocRender[] renders = new JSDocRender[]{
            new MockRender(),
            new MockRender()
        };
        CompositeRender compositeRender = new CompositeRender(renders);
        assertNotSame(renders, compositeRender.getRenders());
        assertEquals(2, compositeRender.getRenders().length);
        
        JSElement root = new JSElement("foo",null);
        compositeRender.render(root);
        assertEquals(root, ((MockRender)renders[0]).getRendered());
        assertEquals(root, ((MockRender)renders[1]).getRendered());
    }
}
