package com.espatial.doc4js.parser.comment;

import com.espatial.doc4js.parser.error.ErrorHandler;
import com.espatial.doc4js.parser.error.ParseError;
import com.espatial.doc4js.parser.error.ParseErrorType;
import com.espatial.doc4js.parser.tag.JSTag;
import java.io.LineNumberReader;
import java.io.StringReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import static junit.framework.Assert.assertEquals;
import junit.framework.TestCase;

/**
 *
 * @author tofarrell
 */
public class CommentExtractorTest extends TestCase {

    public CommentExtractorTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testSkipToNextJSCommentA() throws Exception {
        StringReader reader = new StringReader("/**foo");
        CommentExtractor.skipToNextJSComment(reader);
        assertEquals((int) 'f', reader.read());
    }

    public void testSkipToNextJSCommentB() throws Exception {
        StringReader reader = new StringReader("\nfoo/**bar");
        CommentExtractor.skipToNextJSComment(reader);
        assertEquals((int) 'b', reader.read());
    }

    public void testSkipToNextJSCommentC() throws Exception {
        StringReader reader = new StringReader("foobar");
        CommentExtractor.skipToNextJSComment(reader);
        assertEquals(-1, reader.read());
    }

    public void testSkipToNextJSCommentD() throws Exception {
        StringReader reader = new StringReader("foobar/");
        CommentExtractor.skipToNextJSComment(reader);
        assertEquals(-1, reader.read());
    }

    public void testSkipToNextJSCommentE() throws Exception {
        StringReader reader = new StringReader("//foo/*bar");
        CommentExtractor.skipToNextJSComment(reader);
        assertEquals(-1, reader.read());
    }

    public void testSkipToNextJSCommentF() throws Exception {
        StringReader reader = new StringReader("/ *foobar");
        CommentExtractor.skipToNextJSComment(reader);
        assertEquals(-1, reader.read());
    }

    public void testReadRestOfCommentA() throws Exception {
        StringReader reader = new StringReader("*/bar");
        assertEquals(Arrays.asList(""), CommentExtractor.readRestOfComment(reader));
        assertEquals('b', reader.read());
    }

    public void testReadRestOfCommentB() throws Exception {
        StringReader reader = new StringReader("* /bar");
        assertNull(CommentExtractor.readRestOfComment(reader));
        assertEquals(-1, reader.read());
    }

    public void testReadRestOfCommentC() throws Exception {
        StringReader reader = new StringReader("bar*");
        assertNull(CommentExtractor.readRestOfComment(reader));
        assertEquals(-1, reader.read());
    }

    public void testReadRestOfCommentD() throws Exception {
        StringReader reader = new StringReader("foo\nbar*zap*/");
        assertEquals(Arrays.asList("foo", "bar*zap"), CommentExtractor.readRestOfComment(reader));
        assertEquals(-1, reader.read());
    }

    public void testReadRestOfCommentE() throws Exception {
        StringReader reader = new StringReader("foo\n\nbar*zap*/");
        assertEquals(Arrays.asList("foo", "", "bar*zap"), CommentExtractor.readRestOfComment(reader));
        assertEquals(-1, reader.read());
    }

    public void testReadRestOfCommentF() throws Exception {
        StringReader reader = new StringReader("\n \n\nfoo\n\n \nbar*zap\n\n*/");
        assertEquals(Arrays.asList("", "", "", "foo", "", "", "bar*zap", "", ""), CommentExtractor.readRestOfComment(reader));
        assertEquals(-1, reader.read());
    }

    public void testParseJSTagsA() throws Exception {
        LineNumberReader reader = new LineNumberReader(new StringReader("Lorem Ipsum dolor sit amet\n"
                + " * @taga A first tag with input from a single line\n"
                + " * @tagb A second tag with some text \n"
                + " * spanning multiple lines\n"
                + " @tagc This should include the text of @tagd\n"
                + " */ This is outside the comment \n"
                + "and will be ignored"));
        List<JSTag> tags = CommentExtractor.parseJSTags(null, null, reader);
        assertEquals(Arrays.asList(
                new JSTag(2, "taga", "A first tag with input from a single line"),
                new JSTag(3, "tagb", "A second tag with some text\n"
                + "spanning multiple lines"),
                new JSTag(5, "tagc", "This should include the text of @tagd")), tags);
    }

    public void testParseJSTagsB() throws Exception {
        LineNumberReader reader = new LineNumberReader(new StringReader("@taga */This is outside the comment and will be ignored"));
        List<JSTag> tags = CommentExtractor.parseJSTags(null, null, reader);
        assertEquals(Arrays.asList(
                new JSTag(1, "taga", "")), tags);
    }

    /**
     * Test of extract method, of class CommentExtractor.
     */
    public void testExtract() throws Exception {

        ListingHandler handler = new ListingHandler();
        CommentExtractor extractor = new CommentExtractor(handler, handler);

        //Would be nice to use a resource bundle here, but, there seems to be a bug copying... :(

        URL urlA = new URL("http://a.com");
        extractor.extract(urlA, new StringReader("\n"
                + "/** This comment contains no tags and should result in an error */\n"
                + "\n"
                + "/** @ */ // another error - blank tag\n"
                + "\n"
                + "/** @invalid$tag-name error due to invalid tag name*/\n"
                + "\n"
                + "/**\n"
                + " * @namespace foo the foo namespace\n"
                + " */\n"
                + "window.foo = {\n"
                + "    /**\n"
                + "     * @function bar the bar function\n"
                + "     * @param a parameter a\n"
                + "     * @return a value\n"
                + "     */\n"
                + "    bar : function(a){\n"
                + "        return \"A value\";\n"
                + "    },\n"
                + "    /** @property zap the zap property*/\n"
                + "    zap : 10,\n"
                + "    /**\n"
                + "     * This text will not appear anywhere, but should throw an error\n"
                + "     * @class Tap the tap class\n"
                + "     * @param a parameter a\n"
                + "     * @param b parameter b\n"
                + "     */\n"
                + "    Tap : function(a, b){\n"
                + "\n"
                + "    }\n"
                + "}"
                + "/* This should not be interpreted as a jsdoc comment */\n"));

        URL urlB = new URL("http://a.com");
        extractor.extract(urlB, new StringReader("/**@sometag*/\n"
                + "Lorem ipsum /** Not @start */"));

        assertEquals(Arrays.asList(
                new ParseError(urlA, 2, ParseErrorType.NO_TAGS_IN_COMMENT),
                new ParseError(urlA, 4, ParseErrorType.INVALID_TAG_NAME, ""),
                new ParseError(urlA, 4, ParseErrorType.NO_TAGS_IN_COMMENT),
                new ParseError(urlA, 6, ParseErrorType.INVALID_TAG_NAME, "invalid$tag-name"),
                new ParseError(urlA, 6, ParseErrorType.NO_TAGS_IN_COMMENT),
                new ParseError(urlB, 2, ParseErrorType.NO_TAGS_IN_COMMENT)), handler.getErrors());

        List<Comment> expected = Arrays.asList(
                new Comment(urlA, 8, Arrays.asList(
                new JSTag(9, "namespace", "foo the foo namespace"))),
                new Comment(urlA, 12, Arrays.asList(
                new JSTag(13, "function", "bar the bar function"),
                new JSTag(14, "param", "a parameter a"),
                new JSTag(15, "return", "a value"))),
                new Comment(urlA, 20, Arrays.asList(
                new JSTag(20, "property", "zap the zap property"))),
                new Comment(urlA, 22, Arrays.asList(
                new JSTag(24, "class", "Tap the tap class"),
                new JSTag(25, "param", "a parameter a"),
                new JSTag(26, "param", "b parameter b"))),
                new Comment(urlB, 1, Arrays.asList(
                new JSTag(1, "sometag", ""))));

        assertEquals(expected, handler.getComments());

    }

    private static class ListingHandler implements CommentHandler, ErrorHandler {

        private final List<Comment> comments;
        private final List<ParseError> errors;

        public ListingHandler(List<Comment> comments, List<ParseError> errors) {
            this.comments = comments;
            this.errors = errors;
        }

        public ListingHandler() {
            this(new ArrayList<Comment>(), new ArrayList<ParseError>());
        }

        @Override
        public void handleComment(Comment comment) {
            comments.add(comment);
        }

        @Override
        public void handleError(ParseError error) {
            errors.add(error);
        }

        public List<Comment> getComments() {
            return comments;
        }

        public List<ParseError> getErrors() {
            return errors;
        }
    }
}
