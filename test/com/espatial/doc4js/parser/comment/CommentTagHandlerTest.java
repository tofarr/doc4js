package com.espatial.doc4js.parser.comment;

import com.espatial.doc4js.parser.error.ErrorHandler;
import com.espatial.doc4js.parser.error.ParseError;
import com.espatial.doc4js.parser.error.ParseErrorType;
import com.espatial.doc4js.model.JSElement;
import com.espatial.doc4js.model.JSNamespace;
import com.espatial.doc4js.model.JSProperty;
import com.espatial.doc4js.model.JSReference;
import com.espatial.doc4js.parser.tag.AbstractElementTagHandler.NamespaceTagHandler;
import com.espatial.doc4js.parser.tag.AbstractElementTagHandler.PropertyTagHandler;
import com.espatial.doc4js.parser.tag.JSTag;
import com.espatial.doc4js.parser.tag.SeeTagHandler;
import com.espatial.doc4js.parser.tag.TagHandlerContext;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import junit.framework.TestCase;

/**
 *
 * @author tofarrell
 */
public class CommentTagHandlerTest extends TestCase {

    public CommentTagHandlerTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testHandleComment() throws Exception {
        ListingHandler handler = new ListingHandler();
        TagHandlerContext context = new TagHandlerContext(new JSElement("root", null), handler);
        CommentTagHandler commentHandler = new CommentTagHandler(
                Arrays.asList(new PropertyTagHandler(), new SeeTagHandler(), new NamespaceTagHandler()),
                context);
        final URL urlA = new URL("http://a.com");
        commentHandler.handleComment(new Comment(urlA, 1, Arrays.asList(
                new JSTag(1, "see", "zap See zap"),
                new JSTag(2, "property", "foo The foo property"),
                new JSTag(3, "namespace", "bar The bar namespace"),
                new JSTag(4, "unknown", "An unknown tag"))));

        final URL urlB = new URL("http://b.com");
        commentHandler.handleComment(new Comment(urlB, 1, Arrays.asList(
                new JSTag(1, "property", "foo Another foo tag"),
                new JSTag(2, "property", "tap Yet another tag"))));

        //Set up expected
        JSElement expected = new JSElement("root",null);
        expected.putChild(new JSProperty("foo", "The foo property"));
        expected.putChild(new JSNamespace("bar", "The bar namespace"));
        expected.putChild(new JSProperty("tap", "Yet another tag"));
        List<JSReference> references = new ArrayList<>();
        expected.addReference(new JSReference("zap", "See zap"));

        assertEquals(expected, context.getRoot());

        assertEquals(Arrays.asList(
                new ParseError(urlA, 4, ParseErrorType.UNKNOWN_TAG, "unknown"),
                new ParseError(urlB, 1, ParseErrorType.EXISTING_ELEMENT)), handler.getErrors());
    }

    public static class ListingHandler implements ErrorHandler {

        private final List<ParseError> errors;

        public ListingHandler(List<ParseError> errors) {
            this.errors = errors;
        }

        public ListingHandler() {
            this(new ArrayList<ParseError>());
        }

        @Override
        public void handleError(ParseError error) {
            errors.add(error);
        }

        public List<ParseError> getErrors() {
            return errors;
        }
    }
}
