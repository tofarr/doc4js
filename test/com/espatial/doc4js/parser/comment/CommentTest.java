package com.espatial.doc4js.parser.comment;

import com.espatial.doc4js.parser.tag.JSTag;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import junit.framework.TestCase;

/**
 *
 * @author tofarrell
 */
public class CommentTest extends TestCase {

    public CommentTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testConstructor() throws Exception {
        URL url = new URL("http://espatial.com");
        List<JSTag> tags = new ArrayList<JSTag>(Arrays.asList(new JSTag(0, "foo", null), new JSTag(1, "bar", "foobar")));
        Comment comment = new Comment(url, 10, tags);
        assertEquals(url, comment.getUrl());
        assertEquals(tags, comment.getTags());
        assertEquals(10, comment.getLineNumber());
        tags.add(new JSTag(2, "zap", null));
        assertFalse(tags.equals(comment.getTags()));
    }

    public void testInvalidLineNumber() throws Exception {
        try {
            URL url = new URL("http://espatial.com");
            List<JSTag> tags = new ArrayList<JSTag>(Arrays.asList(new JSTag(0, "foo", null), new JSTag(1, "bar", "foobar")));
            Comment comment = new Comment(url, -1, tags);
            fail("Should have thrown illegal argument exception");
        } catch (IllegalArgumentException ex) {
            //as expected
        }
    }

    public void testNoTags() throws Exception {
        try {
            URL url = new URL("http://espatial.com");
            Comment comment = new Comment(url, -1, new ArrayList<JSTag>());
            fail("Should have thrown illegal argument exception");
        } catch (IllegalArgumentException ex) {
            //as expected
        }
    }
}
