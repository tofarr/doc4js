package com.espatial.doc4js.parser.tag;

import junit.framework.TestCase;

/**
 *
 * @author tofarrell
 */
public class JSTagTest extends TestCase {

    public JSTagTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testConstructorA() throws Exception {
        JSTag tag = new JSTag(21, "fOo", "bar");
        assertEquals(21, tag.getLineNumber());
        assertEquals("foo", tag.getTagName());
        assertEquals("bar", tag.getText());
    }

    public void testConstructorB() throws Exception {
        JSTag tag = new JSTag(0, "fOo", null);
        assertEquals(0, tag.getLineNumber());
        assertEquals("foo", tag.getTagName());
        assertEquals("", tag.getText());

    }

    public void testInvalidLineNumber() {
        try {
            JSTag jsTag = new JSTag(-1, "foo", "bar");
            fail("Should have thrown illegal argument exception");
        } catch (IllegalArgumentException ex) {
            //as expected
        }
    }

    public void testInvalidTagName() {
        try {
            JSTag jsTag = new JSTag(1, "foo bar", "baz");
            fail("Should have thrown illegal argument exception");
        } catch (IllegalArgumentException ex) {
            //as expected
        }
    }
}
