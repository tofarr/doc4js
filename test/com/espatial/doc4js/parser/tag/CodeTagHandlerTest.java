
package com.espatial.doc4js.parser.tag;

import com.espatial.doc4js.parser.comment.CommentTagHandlerTest.ListingHandler;
import com.espatial.doc4js.model.JSCodeBlock;
import com.espatial.doc4js.model.JSElement;
import java.util.Arrays;
import junit.framework.TestCase;

/**
 *
 * @author tofarrell
 */
public class CodeTagHandlerTest extends TestCase {
    
    public CodeTagHandlerTest(String testName) {
        super(testName);
    }
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }
    
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test of handleTag method, of class CodeTagHandler.
     */
    public void testHandleTag() {
        JSTag tag = new JSTag(1, "code", "ONCLICK alert(\"Alert may be used to send messages @users\");");
        CodeTagHandler handler = new CodeTagHandler();
        ListingHandler errorHandler = new ListingHandler();
        TagHandlerContext context = new TagHandlerContext(new JSElement("window",null), errorHandler);
        handler.handleTag(tag, context);
        assertTrue(
            Arrays.equals(
                new JSCodeBlock[]{new JSCodeBlock(null, "alert(\"Alert may be used to send messages @users\");", JSCodeBlock.JSCodeBlockType.ONCLICK)},
                context.getRoot().getCodeBlocks()));
    }

}
