package com.espatial.doc4js.filter;

import com.espatial.doc4js.model.JSElement;
import java.util.HashMap;
import java.util.Map;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.fail;
import junit.framework.TestCase;

/**
 *
 * @author tofar_000
 */
public class SimpleNamespaceFilterTest extends TestCase {

    public SimpleNamespaceFilterTest(String testName) {
        super(testName);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /**
     * Test of buildIncludeTree method, of class SimpleNamespaceFilter.
     */
    public void testBuildTree() {
        Map result = SimpleNamespaceFilter.buildTree(new String[]{"es.map", "es", "es.map.Map"});
        Map expResult = new HashMap();
        expResult.put("es", new HashMap());
        assertEquals(expResult, result);
    }

    /**
     * Test of exclude method, of class SimpleNamespaceFilter.
     */
    public void testExclude() {
        Map<String, Object> root = SimpleNamespaceFilter.buildTree(new String[]{"foo.bar", "baz"});
        JSElement element = new JSElement("window", null);
        JSElement foo = new JSElement("foo", null);
        JSElement bar = new JSElement("bar", null);
        bar.putChild(new JSElement("zap", null));
        foo.putChild(new JSElement("baz", null));
        foo.putChild(bar);
        element.putChild(foo);
        element.putChild(new JSElement("bar", null));
        element.putChild(new JSElement("baz", null));
        JSElement filtered = element.clone();
        SimpleNamespaceFilter.exclude(filtered, root);

        JSElement expected = element.clone();
        expected.removeChild("baz");
        expected.getChild("foo").removeChild("bar");

        assertEquals(expected, filtered);
    }

    /**
     * Test of include method, of class SimpleNamespaceFilter.
     */
    public void testInclude() {
        Map<String, Object> root = SimpleNamespaceFilter.buildTree(new String[]{"foo.bar", "baz"});
        JSElement element = new JSElement("window", null);
        JSElement foo = new JSElement("foo", null);
        JSElement bar = new JSElement("bar", null);
        bar.putChild(new JSElement("zap", null));
        foo.putChild(new JSElement("baz", null));
        foo.putChild(bar);
        element.putChild(foo);
        element.putChild(new JSElement("bar", null));
        element.putChild(new JSElement("baz", null));
        JSElement filtered = element.clone();
        SimpleNamespaceFilter.include(filtered, root);

        JSElement expected = element.clone();
        expected.removeChild("bar");
        expected.getChild("foo").removeChild("baz");

        assertEquals(expected, filtered);
    }
}
