
/**
 * @namespace messenger Namespace for all parameters, classes, and functions for the messenger sample application
 */
var messenger = {
     
    /**
     * @function sayHello function for saying hello given a name
     * @param name the name of the individual to greet
     * @code ONCLICK messenger.sayHello("World");
     */
    sayHello : function(name){
        this.implementation.print("Hello "+name+"!!!");
    },
    
    /**
     * @function setImplementation set the implementation to use for messaging
     * @param implementation the implementation to use
     * @see messenger.implementations
     */
    setImplementation : function(implementation){
        this.implementation.destroy();
        this.implementation = implementation;
    },
       
    /**
     * @function getImplementation get the implementation to use for messaging
     * @return the implementation currently in use
     * @see messenger.implementations
     */
    getImplementation : function(){
        return this.implementation;
    },
 
    /**
     * @namespace implementations namespace for messenger implementations
     */
    implementations : {
        /**
         * @class Console messenger that uses the console to send messages
         */
        Console : {
            /**
             * @function print print a message
             * @param msg the message to print
             */
            print : function(msg){
                console.log(msg);
            },
            /**
             * @function destroy destroy this implementation
             */        
            destroy : function(){
            }
        },
        /**
         * @namespace messenger.implementations.Alert messenger that uses alerts to send messages
         * @code ONCLICK messenger.implementation = messenger.implementations.Alert;
         */
        Alert : {
            /**
             * @function print print a message
             * @param msg the message to print
             */
            print : function(msg){
                alert(msg);
            },
            /**
             * @function destroy destroy this implementation
             */
            destroy : function(){
            }
        },
        /**
         * @class messenger.implementations.Dom messenger that places messages in a DOM element
         * @param parentElement the element in which to place messages
         * @code STATIC_DISPLAYED
         * //Wait until layout has finished
         * window.setTimeout(function(){
         * 
         *   // create a window using the layout engine to display messages
         *   window.messageWin = new tl.Window({
         *     title:"Messages",
         *     closable:false,
         *     outsideClickCloses:false,
         *     visible:true,
         *     x:30,
         *     y:10,
         *     xUnits:"px",
         *     yUnits:"px",
         *     xInvert:true,
         *     height:300},
         *     new tl.Pane({scrolling:true})); 
         *     
         *     //Set the default messenger implementation to use the window
         *     messenger.implementation = new messenger.implementations.Dom(
         *         messageWin.getContentPane().getElement());
         * },500);
         * @code ONCLICK 
         *     messenger.implementation = new messenger.implementations.Dom(
         *         messageWin.getContentPane().getElement());
         */
        Dom : function(parentElement){
            /**
             * @property content element containing messages
             */
            this.content = document.createElement("div");
            parentElement.appendChild(this.content);
            /**
             * @function print print a message
             * @param msg the message to print
             */
            this.print = function(msg){
                var div = document.createElement("div");
                div.innerHTML = msg;
                this.content.appendChild(div);
            };
            /**
             * @function destroy destroy this implementation
             */
            this.destroy = function(){
                parentElement.removeChild(this.content);
                parentElement = null;
            };
        }
    }
};

/**
 * @property messenger.implementation the implementation currently in use
 * @see messenger.implementations
 */
messenger.implementation = messenger.implementations.Console;